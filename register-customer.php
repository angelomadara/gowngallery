<?php 
	$css = [];
	include 'includes/header.php';
?>

<div class="container">
	<!-- <div class="well"> -->
	<div class="row" style="background: #fff;">    		
    		<div class="col-md-4 col-md-offset-4">

    			<form class="form" method="POST" action="actions/signup.php?type=customer">						
    				<?php 
    					if(Request::get('msg')){
    						echo "<p class='alert alert-info'>". Request::get('msg') ."</p>";	
    					}    					
    				?>
					<h3>Personal Information</h3>
					<div>
						<label for="fname">First Name <span class="asterisk">*</span> </label>
						<input type="text" name="fname" id="fname" class="form-control" value="<?=Request::get('f')?>">
					</div>
					<div>
						<label for="mname">Middle Name <span class="asterisk">*</span> </label>
						<input type="text" name="mname" id="mname" class="form-control" value="<?=Request::get('m')?>">
					</div>
					<div>
						<label for="lname">Last Name <span class="asterisk">*</span> </label>
						<input type="text" name="lname" id="lname" class="form-control" value="<?=Request::get('l')?>">
					</div>
					<div>
						<label for="address">Address <span class="asterisk">*</span> </label>
						<input type="text" name="address" id="address" class="form-control" value="<?=Request::get('a')?>">
					</div>
					<div>
						<label for="contact">Contact No. <span class="asterisk">*</span> </label>
						<input type="text" name="contact" id="contact" class="form-control no-spin mask-mobile" placeholder="0910 123 4567" value="<?=Request::get('c')?>">
					</div>
					<hr>
					<h3>Login Information</h3>
					<div>
						<label for="username">Email <span class="asterisk">*</span> </label>
						<input type="email" name="username" id="username" class="form-control" value="<?=Request::get('u')?>">
					</div>
					<div>
						<label for="password1">Password <span class="asterisk">*</span> </label>
						<input type="password" name="password1" id="password1" class="form-control">
					</div>
					<div>
						<label for="password2">Re-password <span class="asterisk">*</span> </label>
						<input type="password" name="password2" id="password2" class="form-control">
					</div>
					
					<div class="clearfix" style="margin-top:1em;">
						<input type="submit" class="btn btn-success pull-right" value="Register">
						<a href="register.php" class="btn btn-default pull-right" style="margin-right:1em;">Cancel</a>
					</div>
				</form>

    		</div>
    </div>
    <!-- </div> -->
</div>

<?php 
	include 'includes/footer.php';
 ?>