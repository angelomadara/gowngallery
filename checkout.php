<?php 

$css = [];
require_once 'includes/header.php';
require_once 'class/Cart.php';
?>


<div class="article">
        <div class="container"> 
        	<div class="row">    		
        		<div class="col-md-12">

                    <div id ="content_area">
                            
                        <div class="container-fluid display-categories" style="background-color: rgba(228, 169, 147, 0.25);" >
                            
                                
                                  <form class='col-lg-12'>
                                      <div class='container-fluid'>
                                          <div class='col-lg-4' style="background:white; border-color:#8a1032; border: 5px solid rgba(0, 0, 0, 0.1); border-radius: 8px; padding-top:20px;">

                                              <p style="text-align: center; "><b> Order Summary </b></p>
                                              <table class="table table-bordered table-striped">
                                                <thead>
                                              		<tr>
                            												<th>Item</th>
                            												<th>Price</th> 
                            												<th colspan="2">Quantity</th>
                       												    </tr>
                                                </thead>

                                                <tbody>

                       												    <?php 
                                                    $cookie = CartCookie::get();
                                                    $found_cart_items = Cart::get([$cookie]);
                                                    $total_price = 0;

                                                    if($found_cart_items){
                                                      foreach ($found_cart_items as $key => $value) {
                                                        echo "<tr>";
                                                          echo "<td>".$value->product_name."</td>";
                                                          echo "<td> Php ".number_format($value->price,2)."</td>";
                                                          echo "<td>".$value->qty."</td>";
                                                          echo "<td> <a class='remove-cart-item' href='actions/delete-cart-item.php?id=".$value->cart_id."'> <i class='glyphicon glyphicon-remove'></i> </a> </td>";
                                                        echo "</tr>";

                                                        // summaztion
                                                        $total_price = $total_price + ($value->price * $value->qty);
                                                      }
                                                    }
                                                  ?>

                                                </tbody>

                                                <tfoot>
                                                  <tr>
                                                    <td><b>Total</b></td>
                                                    <td colspan="3"> Php <?=number_format($total_price,2)?> </td>
                                                  </tr>
                                                </tfoot>

                                              </table>

                                                  <!-- <p><b>Total:</b></p> -->
                                             <!--  <img src="._image_url."small/".$found_product->image."> -->
                                          </div>
                                          <div class='col-lg-8' style="padding-top:20px; background-color: white; border-color:#8a1032; border: 5px solid rgba(0, 0, 0, 0.1); border-radius: 8px;">
                                          	   <table style="width:100%; ">
                                          	   		<p style="text-align: center"><b> Customer's Information </b></p>
                                              		<tr>
    												<th>Name:</th>
    												<th>Address:</th> 
    												<th>Number:</th>
 												    </tr>

 												    <tr>
    												<td>ASDF</td>
    												<td>asdfkgjhiuyetuiwrov ohowrvot</td> 
   												    <td>00000000000000</td>
  													</tr>


                                              </table>
                                             
                                             

                                            </div>
                                         	 <input type='button' value=Cancel class='btn btn-primary btn-sm' style="float:right;">
                                             <input type='button'  value=Accept class='btn btn-primary btn-sm' style="float:right;">

                                       </div>
                                    </form>
                                    
                           

                        </div>

                    </div>

        		</div>
        	</div>
        </div>
    </div>


<?php 
require_once 'includes/footer.php'; 
?>