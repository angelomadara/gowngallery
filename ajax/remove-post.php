<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$id = Request::get('id');

	$x = Query::update('feed',[
		'deleted_at' => _date_time
	],'feed_id',$id);

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text' 	=> 'Post has been successfully closed',
				'type'	=> 'success',
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text' 	=> 'Error encountered while removing post',
				'type'	=> 'error',
			]
		]);
	}
}