<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$password = Request::get('old-password');
	$password1 = Request::get('password1');
	$password2 = Request::get('password2');


	$check_old_password = Query::fetch("SELECT * FROM user WHERE user_id = ?",[$onlineUser['user_id']]);
	if($check_old_password){
		$isMatched = PasswordHash::check_password($check_old_password->password,$password);
		if($isMatched){
			if($password1 === $password2){

				Query::update('user',[
					"password" => PasswordHash::hash($password1)
				],'user_id',$onlineUser['user_id']);

				echo json_encode([
					'status' => true,
					'swal' => [
						'title' => '',
						'text' => "Password changed",
						'type' => 'success',
					]
				]);
			}else{
				echo json_encode([
					'status' => false,
					'swal' => [
						'title' => '',
						'text' => "Password didn't match",
						'type' => 'info',
					]
				]);
			}
		}else{
			echo json_encode([
				'status' => false,
				'swal' => [
					'title' => '',
					'text' => "Old password didn't match",
					'type' => 'info',
				]
			]);
		}
	}

}