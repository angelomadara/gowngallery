<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$url 	= Request::get('control');
$note 	= Request::get('compose') == "" ? null : Request::get('compose');
$id 	= Request::get('order-id');

if(!Session::isLogin('user')){
	return 0;
}else{
	$response = [];
	$smsGateway = new SmsGateway(DEVICE_USER, DEVICE_PASS);
	$number = alter_number(Request::get("number"));
	// $sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);

	if($url == 'approve'){
		$response = Query::update('product_order',[
			'own_approve_note' => $note,
			'status' => 'approved',
			'updated_at' => _date
		],'order_id',$id);
		$order = Query::fetch("SELECT size_id,qty FROM product_order WHERE order_id = ?",[$id]);
		$quantity = Query::fetch("SELECT qty FROM size_and_quantity WHERE size_qty_id = ?",[$order->size_id]);
		$new_quantity = $quantity->qty - $order->qty;
		Query::update('size_and_quantity',['qty'=>$new_quantity],'size_qty_id',$order->size_id);
		$sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
	}
	// elseif($url == 'refuse'){
	// 	$response = Query::update('product_order',[
	// 		'own_refuse_note' => $note,
	// 		'status' => 'denied',
	// 		'updated_at' => _date
	// 	],'order_id',$id);
	// }
	elseif($url == 'cancel'){
		$order = Query::fetch("SELECT size_id,qty,status FROM product_order WHERE order_id = ?",[$id]);
		if($order->status == 'approved'){
			$quantity = Query::fetch("SELECT qty FROM size_and_quantity WHERE size_qty_id = ?",[$order->size_id]);
			$new_quantity = $quantity->qty + $order->qty;
			Query::update('size_and_quantity',['qty'=>$new_quantity],'size_qty_id',$order->size_id);
		}
		$response = Query::update('product_order',[
			'own_cancel_note' => $note,
			'status' => 'canceled',
			'updated_at' => _date
		],'order_id',$id);
		$sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
	}
	elseif($url == 'return'){
		$order = Query::fetch("SELECT size_id,qty,status FROM product_order WHERE order_id = ?",[$id]);
		if($order->status == 'approved'){
			$quantity = Query::fetch("SELECT qty FROM size_and_quantity WHERE size_qty_id = ?",[$order->size_id]);
			$new_quantity = $quantity->qty + $order->qty;
			Query::update('size_and_quantity',['qty'=>$new_quantity],'size_qty_id',$order->size_id);
		}
		$response = Query::update('product_order',[
			'own_return_note' => $note,
			'status' => 'returned',
			'updated_at' => _date,
			'date_return' => _date_time
		],'order_id',$id);
		$sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
	}
	else{
		return 0;
	}


	// response 
	if($response){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Order has been updated.',
				'type'	=> 'success',
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error encountered try to reload the page.',
				'type'	=> 'error',
			]
		]);
	}

}