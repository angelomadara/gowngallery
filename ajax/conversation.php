<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$current_user = Session::get('user');
$feed_id = Request::get('feed_id');
$store_id = Request::get('store');

// die($current_user['user_id']);
// Json::print(Session::get('user'));
// return 0;

$feeds = Query::fetchAll("SELECT 
							c.*, s.store_name 
						FROM comments AS c 
						LEFT JOIN store AS s ON s.user_id = c.user_id 
						WHERE c.feed_id = ? 
						AND ((c.user_id = ? AND c.reply_to = ?) OR (c.user_id = ? AND c.reply_to = ?)) 
						ORDER BY c.comment_id DESC",
						[
							$feed_id,
							$store_id,
							$current_user['user_id'],
							$current_user['user_id'],
							$store_id,
						]);

$conversations = "";
$by = "";

if($feeds){
	foreach ($feeds as $key => $value) {
		$conversations.= "<div style='margin-bottom:10px'>";

			if($value->user_id == $current_user['user_id']){
				$by = "Me";
			}else{
				$by = $value->store_name;
			}

			$conversations.= "<div>".$by." <i style='font-size:10px;'>(Posted date ".date('M/d/Y h:i a',strtotime($value->created_at)).")</i>:</div>";
			$conversations.= "<div>".paragraph($value->comment)."</div>";
		$conversations.= "</div>";
		$conversations.= "<hr>";
	}
}

// Json::encode(['feed'=>$conversations]);
echo json_encode(['feed'=>$conversations]);