<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$urlType = Request::get('type');
	$note = Request::get('note');
	$date = Request::get('date-needed');
	$store = Request::get('store');
	$x = false;
	$custom_id = [];

	if($note == "" && $date == ""){
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Please leave a note and date',
				'type' => 'info'
			]
		]);
		exit();
	}

	if($urlType == 'female'){
		$custom_id = array_filter($_POST['f-dress-id']);
	}else{
		$custom_id = array_filter($_POST['m-cloth-id']);
	}

	if(!count($custom_id)){
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'You didn\'t select any item',
				'type' => 'info'
			]
		]);
		exit();
	}
	$x = Query::create('customized_items',[
		'custom_id' 	=> json_encode($custom_id),
		'user_id' 		=> $onlineUser['user_id'],
		'note' 			=> $note,
		'date_needed' 	=> db_date($date),
		'cookie' 		=> CartCookie::getCustom(),
		'status'		=> 'open',
		'store_id'		=> $store,
		'created_at' 	=> _date,
		'updated_at' 	=> _date,
	]);

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Request customization is sent',
				'type' => 'success'
			]
		]);
		CartCookie::refreshCustom();
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error sending request',
				'type' => 'error'
			]
		]);
	}
}