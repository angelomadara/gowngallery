<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$id = Request::Get('id');

	$x = Query::update('package_order',[
		'status' => 'canceled',
	],'pack_order_id',$id);

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Item successfully removed',
				'type' => 'success'
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error while removing item reload the page and try again',
				'type' => 'error'
			]
		]);
	}
}