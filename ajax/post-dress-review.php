<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$review 	= Request::get('review');
$product_id = Request::get('product-id');
$star 		= Request::get('star');
$review_id 	= Request::get('review-id');
$current_user = Session::get('user')['user_id'];
// print_r($current_user);
// die($current_user);

if($review_id){
	Query::update('product_comments',[
		'comment' => $review,
		'stars' => $star
	],'product_comment_id',$review_id);
}else{
	Query::create('product_comments',[
		'product_id' => $product_id,
		'user_id' => $current_user,
		'comment' => $review,
		'stars' => $star
	]);
}


echo json_encode([
	'status' => true,
	'swal' => [
		'title' => '',
		'text'	=> 'Review has been posted',
		'type'	=> 'success',
	]
]);