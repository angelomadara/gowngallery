<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$id = Request::get('id');

	$feed = Query::fetch("SELECT * FROM feed WHERE feed_id = ?",[$id]);

	

	if($feed->expiration > _date_time){
		// if feed is not expired

		if($feed){
			$feed = [
				'feed_id' => $feed->feed_id,
				'author' => $feed->author,
				'title' => $feed->title,
				'message' => $feed->message,
				'photo' => $feed->photo,
				'max_participants' => $feed->max_participants,
				'isClose' => $feed->isClose,
				'expiration' => date('m/d/Y',strtotime($feed->expiration)),
				'deleted_at' => $feed->deleted_at,
				'created_at' => $feed->created_at,
				'updated_at' => $feed->updated_at,
			];
		}
		echo json_encode(['status'=>true,'feed'=>$feed]);
	}else{
		// if feed is expired
		echo json_encode(['status'=>false]);
	}
}