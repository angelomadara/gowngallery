<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$current_user = Session::get('user');

$reply = Request::get('reply');
$feed_id = Request::get('feed-id');
$user_id = $current_user['user_id'];
$customer = Request::get('customer-id');
// $x = true;

// die($reply);

if($reply == "" || $feed_id == "" || $user_id == ""){
	echo json_encode([
		'status' => false,
		'swal' => [
			'title' => '',
			'text' => "Reply cannot be empty.",
			'type' => 'info'
		],
	]);
	return 0;
}

$feed = Query::fetch("SELECT participants FROM feed WHERE feed_id = ?",[$feed_id]);
// die($feed);
if($feed->participants == null){
	$participants = [];
}else{
	$participants = json_decode($feed->participants);
}

if(!in_array($user_id,$participants)){
	$parts = json_encode(array_merge([$user_id],$participants));
	Query::update('feed',[
		'participants' => $parts
	],'feed_id',$feed_id);
}


$x = Query::create('comments',[
	// 'comment_id' => $comment_id,
	'comment' => $reply,
	'feed_id' => $feed_id,
	'user_id' => $user_id,
	'reply_to' => $customer,
	'created_at' => _date_time,
	'updated_at' => _date_time,
]);

if($x){
	echo json_encode([
		'status' => true,
		'swal' => [
			'title' => '',
			'text' => "New reply posted.",
			'type' => 'success'
		],
	]);
}else{
	echo json_encode([
		'status' => false,
		'swal' => [
			'title' => '',
			'text' => "Error while saving reply.",
			'type' => 'info'
		],
	]);
}