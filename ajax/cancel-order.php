<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$url 	= Request::get('control');
$note 	= Request::get('compose');
$id 	= Request::get('order-id');

if(!Session::isLogin('user')){
	return 0;
}else{
	$response = false;
	$sms = false;
	if($url == 'cancel'){

		$smsGateway = new SmsGateway(DEVICE_USER, DEVICE_PASS);
		$number = '+639081525908';
		$message = 'Sample text message';
		$sms = $smsGateway->sendMessageToNumber($number, $message, DEVICE_ID, $sms_opt);
		if($sms){
			$response = Query::update('product_order',[
				'cus_cancel_note' => $note,
				'status' => 'canceled'
			],'order_id',$id);
		}

	}else{
		return 0;
	}

	if($response && $sms){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Order has been canceled.',
				'type'	=> 'success',
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error encountered try to reload the page.',
				'type'	=> 'error',
			]
		]);
	}
}