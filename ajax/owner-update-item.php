<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$product_id = 		Request::get("product_id");
    $product_title = 	Request::get("product_title");
    $product_cat = 		Request::get("product_cat");
    $product_type = 	Request::get("product_type");
    $product_price = 	Request::get("product_price");
    $product_desc = 	Request::get("product_desc");
    $product_keywords = Request::get("product_keywords");

    $x = Query::update('product',[
    	'type_id' => $product_type,
    	'cat_id' => $product_cat,
    	'product_name' => $product_title,
    	'description' => $product_desc,
    	'price' => $product_price,
    	'keywords' => $product_keywords,
    ],'product_id',$product_id);

    if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Product information changed',
				'type'	=> 'success',
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error encountered try to reload the page.',
				'type'	=> 'error',
			]
		]);
	}

}

