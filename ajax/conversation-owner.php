<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$current_user = Session::get('user');
$feed_id = Request::get('feed_id');
$customer = Request::get('customer');

// die($current_user['user_id']);
// Json::print(Session::get('user'));
// return 0;
$online_id = $current_user['user_id'];

// update isRead
Query::raw("UPDATE comments SET isRead = ? WHERE feed_id = ? AND user_id = ? AND reply_to = ? ",[1,$feed_id,$customer,$online_id]);

$feeds = Query::fetchAll("SELECT 
							c.*, up.first_name, up.last_name
						FROM comments AS c 
						LEFT JOIN user_profile AS up ON up.user_id = c.user_id 
						WHERE c.feed_id = ? 
						AND ((c.user_id = ? AND c.reply_to = ?) OR (c.user_id = ? AND c.reply_to = ?)) 
						ORDER BY c.comment_id DESC",
						[
							$feed_id,
							$customer,
							$current_user['user_id'],
							$current_user['user_id'],
							$customer,
						]);

$conversations = "";
$by = "";

if($feeds){
	foreach ($feeds as $key => $value) {
		$conversations.= "<div style='margin-bottom:10px'>";

			if($value->user_id == $current_user['user_id']){
				$by = "Me";
			}else{
				$by = title_case($value->first_name." ".$value->first_name);
			}

			$conversations.= "<div>".$by." <i style='font-size:10px;'>(Posted date ".date('M/d/Y h:i a',strtotime($value->created_at)).")</i>:</div>";
			$conversations.= "<div>".paragraph($value->comment)."</div>";
		$conversations.= "</div>";
		$conversations.= "<hr>";
	}
}

// Json::encode(['feed'=>$conversations]);
echo json_encode(['feed'=>$conversations]);