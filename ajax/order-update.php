<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$response = [];
	$smsGateway = new SmsGateway(DEVICE_USER, DEVICE_PASS);
	$number = alter_number(Request::get("num"));
	$type = Request::get('type');
	$id = Request::get('id');

	if($type == 'approve'){
		$note = "Your item/request has been approved please see our shop";
		$response = Query::update('product_order',[
			'own_approve_note' => $note,
			'status' => 'approved',
			'updated_at' => _date
		],'order_id',$id);
		$order = Query::fetch("SELECT size_id,qty FROM product_order WHERE order_id = ?",[$id]);
		$quantity = Query::fetch("SELECT qty FROM size_and_quantity WHERE size_qty_id = ?",[$order->size_id]);
		$new_quantity = $quantity->qty - $order->qty;
		Query::update('size_and_quantity',['qty'=>$new_quantity],'size_qty_id',$order->size_id);
		$sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
		echo json_encode(['okay']);
	}
	elseif($type == 'cancel'){

	}
}