
<?php 
    $css = [
        'index.css',
        'owner-dashboard.css'
    ]; 
   
include 'includes/owner_header.php';

?>
    
<div class="container" style="min-height: 400px;">
    <div class="well">
        <div class="row" id="owner-dashboard">

            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>products.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='#'>My Products</a></h4>
                    </div>
                </div>
            </div>
                        
            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>orders.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='#'>My Orders</a></h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>shop.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='#'>My Shop</a></h4>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<?php 
	include 'includes/footer.php';
 ?>