<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(!Session::isLogin('user') || Session::get('user')['user_type'] != "customer"){
    Redirect::to('index.php');
    return 0;
}
$customer = Session::get('user');
$profile = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$customer['user_id']]);

$items = Query::fetchAll("SELECT * FROM customized_items WHERE user_id =? ORDER BY item_id DESC",[$customer['user_id']]);

?>
    
<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff;">           
        <div class="col-lg-12">
            <ul class="breadcrumb">
                <li class="active">Home</li>
            </ul>
        </div>
    </div>
    <div class="row" id="owner-dashboard" style="background: #fff;min-height: 500px;">
        
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <?php if($profile): ?>
            <div class="relative clearfix">
                <?php 
                    $photo = $profile->picture ? _image_url.'profile/'.$profile->picture : _image_url.'profile/'.rand(1,20).'.svg'; 
                ?>
                <img src="<?= $photo ?>" style='width:100%;display: block;margin: auto;' alt="">
                <br>
                <button id="update-photo" class="btn btn-success btn-sm pull-left">Update Picture</button>
            </div>
            <div style="color: #3498db;font-size:1.2em;margin-top:15px;">
                <p><b><?= strtoupper($profile->first_name." ".$profile->middle_name." ".$profile->last_name) ?></b></p>
            </div>
            <div><?= $profile->contact ?></div>
            <div><?= title_case($profile->address) ?></div>
            <?php endif; ?>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="customer.php"><b>Dashboard</b></a></li>
                <li role="presentation"><a href="customer-orders.php"><b>Requests</b></a></li>
                <li role="presentation"><a href="post-gown.php"><b>Post Dress</b></a></li>
                <li role="presentation" class="active"><a href="my-customization.php"><b>My Customization</b></a></li>
                <li role="presentation"><a href="customer-account.php"><b>Account</b></a></li>
            </ul>
            
            <br>
            <div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Requests</th>
                                <th style="width:20%">Notes</th>
                                <th style="width:15%;">Dates</th>
                                <!-- <th style="width:15%">Status</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($items):foreach($items as $item): ?>
                                <tr>
                                    <td><?=$item->item_id?></td>
                                    <td>
                                        <div>
                                            <p>Status: <b><?=title_case($item->status)?></b></p>
                                        </div>
                                        <?php  $ids = json_decode($item->custom_id,true); ?>
                                        <?php foreach ($ids as $id) : ?>
                                            <div class="row" style="background: transparent !important;">
                                                <?php $part = Query::fetch("SELECT * FROM customization WHERE custom_id =?",[$id]); ?>                                        
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <img src="<?php echo _image_url.'parts/'.$part->image ?>" alt="<?php echo $part->image ?>" style="width:100%;display:block;">
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="border-bottom:1px solid #ddd">
                                                    <div>Type: <?php echo $part->type; ?> </div>
                                                    <div>Name: <?php echo title_case($part->name); ?> </div>
                                                    <div>Price: PHP <?php echo number_format($part->price,2); ?> </div>
                                                    <?php $store_name = Query::fetch("SELECT store_name FROM store WHERE store_id = ?",[$part->store_id]); ?>
                                                    <div>Store: <?php echo $store_name->store_name; ?> </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?> 
                                    </td>
                                    <td> 
                                        <div><b>My Note:</b> <br><?=paragraph($item->note)?></div> 
                                        <hr>
                                        <div><b>Store's Note:</b> <br> <?=paragraph($item->store_note)?> </div>
                                    </td>
                                    <td> 
                                        <div>
                                            <div>Date Needed :</div> <div><?=pretty_date($item->date_needed)?></div> 
                                        </div>
                                        <hr>
                                        <div>
                                            <div>Fitting Date :</div>
                                            <div>
                                                <?=pretty_date($item->fitting_date)?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

       
        
    </div>
</div>

<?php 
    $scripts = ["customer.js"];
	include 'includes/footer.php';
 ?>