<?php 
require_once 'init.php';
// Json::print(Session::get('user'));
$current_page = $_SERVER['PHP_SELF'];
?>
<!DOCTYPE html>
<html lang="en" data-cookie="<?=CartCookie::get()?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Gown Gallery</title>
    <!-- Bootstrap -->
    <link href="fonts/champagne/stylesheet.css" rel="stylesheet">
    <link href="fonts/opensans/stylesheet.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link href="css/bootstrap-dialog.css" rel="stylesheet">
    <link href="css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="css/bootstrap-select.css" rel="stylesheet">
    <link href="css/select2.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-2.2.4/dt-1.10.13/af-2.1.3/cr-1.3.2/r-2.1.1/rr-1.2.0/se-1.2.0/datatables.min.css"/> -->
    <link href="<?=_plugins?>/datatable/media/css/dataTables.bootstrap4.min.css" rel="stylesheet">
     <!-- <link href="css/index.css" rel="stylesheet"> -->
    <link href="css/sweetalert.css" rel="stylesheet">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/global.css" rel="stylesheet">
    <script src="js/sweetalert.min.js"></script>
    <?php 
        if(@$css){
            foreach ($css as $value) {
                echo "<link href='css/{$value}' rel='stylesheet' type='text/css'>\n";
            }
        }
    ?>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id='u' data-id='<?= Session::get("user")["user_id"] ?>'>

    <div class="container">
        <div class="row">       
            <!-- <div class="col-md-12"> -->
            <div class="">

                <div id="header">
                    <img src="images/banner.jpg" width="100%">
                </div>


                <nav class="navbar navbar-default" style="margin-bottom:0px; border-radius: 0;">
                    <div class="container">
                        <div class="navbar-header">
                            <!--<a class="nav navbar-nav " href="../index.php">Home</a>-->
                        </div>
                        <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                        <div id="navbar">
                            <ul class="nav navbar-nav" style="font-weight: 600;">
                                <?php if(Session::get('user')['user_type'] != 'owner'): ?>
                                    <li class="<?= $current_page == '/index.php' ? 'active' : '' ?>">
                                        <a href="index.php">Gallery</a>
                                    </li>
                                    <!-- <li><a href="#">Collection</a></li> -->
                                    <!-- <li><a href="#contact">Customize/Upload</a></li> -->
                                    <li class="<?= $current_page == '/about-us.php' ? 'active' : '' ?>"><a href="about-us.php">About</a></li>
                                    <!-- <li><a href="#contact">Contact</a></li> -->
                                <?php endif; ?>
                                
                                <?php if(Session::exist('user')): ?>

                                    <?php if(Session::get('user')['user_type'] == 'owner'): ?>
                                        <li 
                                            class="<?= $current_page == '/owner.php' ? 'active' : '' ?>"  
                                            style="padding-left: 5px;">
                                                <a href="owner.php">My Account</a>
                                        </li>
                                    <?php else: ?>
                                        <li 
                                            class="<?= $current_page == '/customer.php' ? 'active' : '' ?>"
                                            style="padding-left: 5px;">
                                                <a href="customer.php">My Account</a>
                                            </li>
                                    <?php endif ?>    
                                        <li style="padding-left: 5px;"><a href="logout.php">Log out</a></li>'
                                    
                                <?php else: ?>
                                    <li class="<?= $current_page == '/login.php' ? 'active' : '' ?>" style="padding-left: 5px;"><a href="login.php">Log In</a></li>
                                <?php endif ?>
                                
                            </ul>

                        </div>
                    </div>
                </nav>

                <!--  -->
                <nav style="min-height: 35px;">
                <!-- <div class="container"> -->

                    <div class="clearfix" style="background: rgba(52, 152, 219,1.0);color:#fff;">

                        <div class="col-lg-6" style="margin-top:7px;" >
                            <p >
                                <b> Welcome
                                    <?php 
                                        if(Session::exist('user')){
                                            echo Session::get('user')['username']; 
                                        }else{
                                            echo 'Guest!';
                                        }
                                    ?>
                                </b>
                            </p>
                        </div>
                        
                        <?php if(Session::get('user')['user_type'] != 'owner'): ?>    
                        <div class="col-lg-6" id="customer-cart" style=" padding-right: 4em; margin-top:7px;">
                            <ul class="navbar-right">
                                <!-- <a href="#" id="cart"><i class="glyphicon glyphicon-shopping-cart" style="left:7px; top:9px; color:#a70135; font-size: 17px"></i><span class="badge" id='cart_count' style="color:#c11e1e"></span></a> -->
                                <?php $badge_count = count(Query::fetchAll("SELECT * FROM cart WHERE cookie = ? AND status != ?",[CartCookie::get('_cartCookie'),1])); ?>
                                <?php $badge_count2= count(Query::fetchAll("SELECT * FROM package_order WHERE cookie = ? AND status = ?",[CartCookie::get('_cartCookie'),'open'])); ?>
                                <span class="badge"><?=$badge_count+$badge_count2?></span>
                                <img src="<?=_image_url?>bag.png" alt="" style='width:20px;'> <a href='bag.php' style="color: #fff;">My Bag</a>
                            </ul>
                            <div class="container hidden" id="cart-information">
                                <div class="shopping-cart" style="background: rgba(245, 245, 245, 0.83); width: 239px;">
                                    <div class="shopping-cart-header" style="text-align:right;">
                                    <!-- <i class="fa fa-shopping-cart cart-icon"></i><span class="badge">3</span> -->
                                        <div class="shopping-cart-total">
                                            <span class="lighter-text">Total:</span>
                                            <span class="main-color-text" id="cart-price"></span>
                                        </div>
                                    </div> <!--end shopping-cart-header -->

                                    <ul class="shopping-cart-items" style="list-style: none;padding:0;"></ul>

                                    <a href="checkout.php" class="btn btn-primary" style="float:right;">Checkout</a>
                                </div> <!--end shopping-cart -->
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </nav>  

            </div>
        </div>
    </div>