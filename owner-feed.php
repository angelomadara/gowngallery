<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
// include 'includes/owner_header.php';
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
$newFeeds = count(Query::fetchAll("SELECT feed_id FROM feed WHERE expiration > ?",[_date_time]));
$newOffers = 0;
$newOrders = count(Query::fetchAll("SELECT * FROM product_order WHERE status = ?",['pending']));

$feeds = Query::fetchAll("
    SELECT 
    f.*, up.first_name, up.last_name, up.user_id AS cus_id
    FROM feed AS f
    LEFT JOIN user_profile AS up ON up.user_id = f.author
    -- WHERE deleted_at IS NULL 
    ORDER BY feed_id DESC",[$owner['user_id']]);

?>

<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
        </div>

    </div>
    <div class="row" id="owner-dashboard" style="background: #fff">
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative active">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
                <!-- <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>

            <div>
                <ul class="breadcrumb">
                    <li class="active">Feeds</li>
                </ul>
            </div>
    
            <?php if($feeds): ?>
            <?php foreach($feeds as $feed): ?>
                <?php
                    if($feed->participants == null){
                        $participants_array = [];
                    }else{
                        $participants_array = json_decode($feed->participants);
                    }

                ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 clearfix" style="margin-bottom: 2em;">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <img src="<?= _image_url."small/".$feed->photo; ?>" style="width:100%;">
                    </div>
                        
                    <?php 
                        $badge = "";
                        $new = Query::fetch("SELECT COUNT(*) AS x FROM comments WHERE feed_id = ? AND reply_to = ? AND isRead = ?",[$feed->feed_id,$owner['user_id'],0]);
                        if($new->x > 0){
                            $badge = "<span class='badge cursor-pointer' data-tooltip data-placement='top' title='".$new->x." reply'> ".$new->x." </span>";
                        }

                        $banana = Query::fetch("SELECT * FROM `comments` WHERE feed_id = ? AND user_id = ?",[$feed->feed_id,$owner['user_id']]);
                        $btnClass = "btn-success";
                        $btnText = "See Details";

                        if(count($banana)){
                            $btnText = "Reply to conversation";
                        }
                        if($feed->expiration < _date || $feed->deleted_at != null){
                            $btnClass = "btn-default";
                            $btnText = "View history (this post has expired/closed)";
                        }
                    ?>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div><b><?= title_case($feed->title) ?></b> <?=$badge?> </div>
                        <div style="font-size:12px">
                            Posted by: <a href="view-customer.php?id=<?=$feed->cus_id?>"> <?=$feed->first_name?> <?=$feed->last_name?></a> 
                        </div>
                        <span class="italic" style="display: block;font-size:12px;"> 
                            Posted date <?=pretty_date($feed->created_at)?> at <?=pretty_time($feed->created_at)?> 
                        </span>
                        <span class="italic" style="display: block;font-size:12px;"> 
                            Expiration date <?=pretty_date($feed->expiration)?> at <?=pretty_time($feed->expiration)?> 
                        </span>
                        <span class="italic" style="display: block;font-size:12px;"> 
                            <?php echo count($participants_array); ?>/<?php echo $feed->max_participants; ?> participants 
                        </span>
                        <br>
                        <div><?= paragraph(mb_strimwidth($feed->message,0,380,'...'),'justify') ?></div>
                        <div>
                            <?php if($feed->max_participants > count($participants_array)): ?>
                                <a href="owner-feed-conversation.php?feed=<?= $feed->feed_id ?>" class='btn <?=$btnClass?> btn-sm'>
                                    <?=$btnText?>
                                </a>
                            <?php else: ?>
                                <?php if(in_array($owner['user_id'], $participants_array)): ?>
                                    <a href="owner-feed-conversation.php?feed=<?= $feed->feed_id ?>" class='btn <?=$btnClass?> btn-sm'>
                                        <?=$btnText?>
                                    </a>
                                <?php else: ?>
                                    <button class='btn btn-default btn-sm' type="button">Participants are full</button>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                </div>

            <?php endforeach; ?>
        <?php endif; ?>

        </div>

        
    </div>
</div>

<?php
    $scripts = ['my-shop.js','notification.js'];
    include 'includes/footer.php';
?>