(function(){

 	// input masking
	$(".mask-mobile").mask("9999 999 9999",{
		'placeholder': '0000 000 0000'
	}); 
	$(".mask-money").mask("Php 999, 999, 999.99",{
		'placeholder': 'Php 000, 000, 000.00'
	}); 

	$(document).find('.selectpicker').selectpicker();
	$('.select2').select2();

	
	$('.datepicker').datepicker({
		autoclose: true,
		format: "mm/dd/yyyy",
	    todayHighlight: true,
	    startDate: new Date()
	});
	$('.datepicker2').datepicker({
		autoclose: true,
		format: "mm/dd/yyyy",
	    todayHighlight: true,
	    // startDate: new Date()
	});

	$('[data-tooltip]').tooltip();

	$('.datatable').DataTable({	   
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ],
        "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5,
        "order": [[ 0, "desc" ]],
    });

    // $('.page-link').click(function(e){
    // 	console.log('clicked');
    // 	$('html,body').animate({
    //         scrollTop: 0
    //     },0);
    // });

}());

// function dataTable($table){
// 	$($table).DataTable({	   
//         "columnDefs": [
//             {
//                 "targets": [ 0 ],
//                 "visible": false
//             }
//         ],
//         "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
//         "iDisplayLength": 5,
//         "order": [[ 0, "desc" ]],
//     });
// }

function loading(elm){    
    $(elm).html("<div style='width:50px;margin:auto;'><img src='images/ring-alt.svg' width='100%'></div>");
}

function toFloat($string){
	return parseFloat($string.replace(/\,/g,''));
}

function moneyFormat($string){
	$string = $string.toString().replace(/\,/g,'');
	return $string.toString().split('').reverse().join('').replace(/(\d{3}(?!.*\.|$))/g, '$1,').split('').reverse().join('');
}

function dateNow(format){
	var d = new Date();
	var currDate = d.getDate();
	var currMonth = d.getMonth();
	var currYear = d.getFullYear();
	if(format == "mm/dd/yyyy")
		return currMonth+"/"+currDate+"/"+currYear;
	if(format == "yyyy-mm-dd")
		return currYear+"-"+currMonth+"-"+currDate;
	else
		return currMonth+"/"+currDate+"/"+currYear;
}