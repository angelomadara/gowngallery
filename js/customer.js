(function(){
	var customer_id = $('#customer').attr('data-id');
    
    $('.a-conversations').click(function(e){
        e.preventDefault();
        var feed_id = $(this).data('id');
        var store_id = $(this).data('store');
        var exp_date = $(this).data('exp');
        // alert(exp_date)

        // var exp_time = $(this).parent().parent()
        // .parent().parent()
        // .siblings('.feed-title').childen('span:last-child').text();
            
        // console.log(exp_time);    

        BootstrapDialog.show({
            title: "Conversations",
            message: function(dialog) {
                return $("<form id='form-reply'></form>").load('forms/conversation.html');
            },
            onshown: function(dialog){
                $("input[name='feed-id']").val(feed_id);
                $("input[name='user-id']").val(customer_id);
                $("input[name='store-id']").val(store_id);
                $("#con-info").html("This post will expire on "+exp_date);
                // alert(feed_id)
                $.get('ajax/check-post-expiration.php',{feed_id:feed_id,store_id:store_id},function(data){
                    if(data == 'disabled'){
                        $("#con-info").html("This post is expired");
                        swal({ title: '', text: 'This post is expired', type: 'info' });
                    }
                    $(document).find("#form-reply :input").prop(data, true);
                });

                loadConversation({
                    feed_id:feed_id,
                    store:store_id
                });
            },  
            // buttons: [{
            //     label: 'Close',
            //     action: function(dialog){
            //         dialog.close();
            //     }
            // }],
        });

    })

    $(document).on('click','#btn-dialog-reply',function(e){
        e.preventDefault()
        var form = $(document).find('#form-reply').serialize();
        // console.log(form);
        $.post('ajax/post-reply.php',form,function(data){
            var data = JSON.parse(data);
            swal(data.swal);
            loadConversation({
                feed_id:$("input[name='feed-id']").val(),
                store:$("input[name='store-id']").val()
            });
        });
    });

    $(".edit-post").click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        BootstrapDialog.show({
            title: "Conversations",
            message: function(dialog) {
                return $("<form id='form-update'></form>").load('forms/customer-update-post.php');
            },
            onshown: function(dialog){
                $.get('ajax/get-post.php',{id:id},function(data){
                    var data = JSON.parse(data);
                    if(data.status == false) {
                        swal('','This post is expired','info');
                        $('#form-update :input').prop('disabled',true);
                    }
                    else{
                        $('input[name="_title"]').val(data.feed.title);
                        $('textarea[name="_message"]').val(data.feed.message);
                        $('input[name="_participants"]').val(data.feed.max_participants);
                        $('input[name="_expiration"]').val(data.feed.expiration);
                        $('#form-update').append('<input type="hidden" name="id" value="'+id+'">');
                        $('.datepicker').datepicker();
                    }
                });
            },  
            buttons: [
                {
                    label: 'Close',
                    action: function(dialog){
                        dialog.close();
                    }
                },{
                    label: 'Update',
                    cssClass: 'btn-success',
                    action: function(dialog){
                        var form = $('#form-update').serialize();

                        $.post('actions/update-post.php',form,function(data){
                            var data = JSON.parse(data);
                            swal(data.swal,function(aaa){
                                if(aaa){
                                    if(data.status == true) location.reload();
                                }
                            });
                        })
                    }
                },
            ],
        });
    });

    $(".remove-post").click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        swal({
            title: "",
            text: "Are you sure you want to remove this post?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e74c3c",
            confirmButtonText: "Yes, close it!",
            cancelButtonText: "No!",
            closeOnConfirm: false,
        },function(a){
            if(a){
                $.get('ajax/remove-post.php',{id:id},function(data){
                    var data = JSON.parse(data);
                    swal(data.swal,function(adf){
                        if(adf){
                            location.reload();
                        }
                    });
                });
            }
        });
    });

	$('.a-reply-comment').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');

		BootstrapDialog.show({
			title: "Reply",
            message: function(dialog) {
                var $message = $('<form id="post-reply-form"></form>');
                var pageToLoad = dialog.getData('pageToLoad');
                $message.load(pageToLoad);        
                return $message;
            },
            data: {
                'pageToLoad': 'forms/reply-form.html'
            },
            onshown: function(dialog){
            	$("input[name='feed-id']").val(id);
				$("input[name='user-id']").val(customer_id);
            },	
            buttons: [{
                label: 'Cancel',
                action: function(dialog){
                	dialog.close();
                }
            }, {
                label: 'Reply',
                cssClass: 'btn-primary',
                action: function(){
                    var form = $('#post-reply-form').serialize();
                    $.get('ajax/post-reply.php',form,function(data){
                    	var data = JSON.parse(data);
                    	if(data.status == false){
                    		swal(data.swal);
                    	}else{
                            location.reload();
                        }
                    });
                }
            }],
        });

	});

    $('.cancel-order').click(function(e){
        var id = $(this).data('id');
        BootstrapDialog.show({
            title: "Cancel Order",
            message: function(dialog) {
                return $("<form id='form'></form>").load('forms/cancel-order.html');
            },
            onshown:function(){
                $('#order-id').val(id);
            },
            buttons: [
                {
                    label: 'Close',
                    action: function(dialog){
                        dialog.close();
                    }
                },
                {
                    label: 'Send',
                    cssClass: 'btn-success',
                    id: 'oki-buton',
                    action: function(dialog){
                        dialog.getButton('oki-buton').disable();
                        var form = $('#form').serialize();
                        $.post('ajax/cancel-order.php?control=cancel',form,function(data){
                            var data = JSON.parse(data);
                            swal(data.swal,function(isOKay){
                                if(isOKay){
                                    if(data.status == true) location.reload();
                                }
                            });
                        });
                    }
                }
            ],
        });
    });

    $(document).on('click','.review-dress',function(e){
        var id = $(this).data('id');
        BootstrapDialog.show({
            title: "Post Review",
            message: function(dialog) {
                return $("<form id='form'></form>").load('forms/review-form.html');
            },
            onshown:function(){ 
                $('#product-id').val(id); 
                $.get('ajax/product-comment.php',{id:id},function(data){
                    var data = JSON.parse(data);
                    var review = data.data;
                    if(data.status == true){
                        $('textarea[name="review"]').val(review.comment);
                        $('select[name="star"] option[value="'+review.stars+'"]').prop('selected',true);
                        $('#review-id').val(review.product_comment_id);
                    }
                });
            },
            buttons: [
                {
                    label: 'Close',
                    action: function(dialog){
                        dialog.close();
                    }
                },
                {
                    label: 'Post review',
                    cssClass: 'btn-success',
                    action: function(dialog){
                        var form = $('#form').serialize();
                        $.post('ajax/post-dress-review.php',form,function(data){
                            var data = JSON.parse(data);
                            swal(data.swal,function(isOKay){
                                if(isOKay){ if(data.status == true) location.reload(); }
                            });
                        });
                    }
                }
            ],
        });
    });

    $(document).on('click','.rate-store',function(e){
        var id = $(this).data('id');
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_SMALL,
            title: "Post Review",
            message: function(dialog) {
                return $("<form id='form'></form>").load('forms/store-ratings.html');
            },
            onshown:function(){ 
                $('#product-id').val(id); 
                $('#form').append("<input type='hidden' name='store' value='"+id+"'>");
                $.get('ajax/store-ratings.php',{id:id},function(data){
                    $('#form').append("<input type='hidden' name='rating-id' value='"+data+"'>");
                });
            },
            buttons: [
                {
                    label: 'Close',
                    action: function(dialog){
                        dialog.close();
                    }
                },
                {
                    label: 'Submit/Update',
                    cssClass: 'btn-success',
                    action: function(dialog){
                        var form = $('#form').serialize();
                        $.post('ajax/post-store-ratings.php',form,function(data){
                            var data = JSON.parse(data);
                            swal(data.swal,function(isOKay){
                                if(isOKay){ if(data.status == true) location.reload(); }
                            });
                        });
                    }
                }
            ],
        });
    });

    $('#update-my-profile').click(function(e){
        e.preventDefault();
        var form = $('#form-update-profile');
        $.post(form.attr('action'),form.serialize(),function(data){
            var data = JSON.parse(data);
            swal(data.swal);
        });
    });

    $("#update-photo").click(function(e){
        e.preventDefault();
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_SMALL,
            title: "Photo",
            message: function(dialog) {
                var $message = $('<div id="ksdjkien"></div>');
                var pageToLoad = dialog.getData('pageToLoad');
                $message.load(pageToLoad);        
                return $message;
            },
            data: {
                'pageToLoad': 'forms/customer-update-photo.php'
            },
            buttons: [{
                label: 'Cancel',
                action: function(dialog){
                    dialog.close();
                }
            }, {
                label: 'Update',
                cssClass: 'btn-primary',
                action: function(){
                    // var form = $('#post-reply-form').serialize();
                    var fd = new FormData();
                    fd.append('file',$("#file")[0].files[0]);
                    $.ajax({
                        type: 'post',
                        url : 'actions/customer-update-photo.php',
                        processData: false,
                        contentType: false,
                        data: fd,
                        dataType: 'json',
                        success: function(html){
                            swal(html.swal,function(x){
                                if(x){
                                    location.reload();
                                }
                            });
                        }
                    });
                }
            }],
        });
    });

    $('#btn-update-password').click(function(e){
        e.preventDefault();
        var form = $('#update-password');
        $.post(form.attr('action'),form.serialize(),function(data){
            var data = JSON.parse(data);
            swal(data.swal);
        });
    });

    $('.cancel-package-btn').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        swal({
            title: '',
            text : 'Are you sure do you want to cancel your order?',
            type : 'info',
            showCancelButton: true,
            confirmButtonColor: "#3498db",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false
        },function(isOkay){
            if(isOkay){
                $.post('ajax/cancel-package-order.php',{id:id},function(data){
                    var data = JSON.parse(data);
                    swal(data.swal,function(isOkay){
                        if(data.status == true) location.reload();
                    });
                });
            }
        });
    });

    function loadConversation(obj){        
        loading('#load-conversation');
        $.get('ajax/conversation.php',obj,function(data){
            var data = JSON.parse(data);
            $(document).find('#load-conversation').html(data.feed);
        });        
        $("textarea[name='reply']").val("");
    }

}())