(function(){

	$('#pack-file').change(function(e){
		var output = document.getElementById('preview');
	    output.src = URL.createObjectURL(e.target.files[0]);
	});

	$('#add-package').click(function(e){
		e.preventDefault();
		swal({
			title: '',
			text : 'Upload package?',
			type : 'info',
			showCancelButton: true,
			confirmButtonColor: "#3498db",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
			closeOnConfirm: false
		},function(isOkay){
			if(isOkay){
				var fd = new FormData();
                fd.append('file',$("#pack-file")[0].files[0]);
                fd.append('name',$("#pack-name").val());
                fd.append('prce',$("#pack-price").val());
                fd.append('note',$('#pack-note').val());
                fd.append('id',$('#id').val());
				$.ajax({
                    type: 'post',
                    url : $('#upload-form').attr('action'),
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType: 'json',
                    success: function(html){
                        swal(html.swal,function(x){                            	
                            if(html.status == true) location.href="my-packages.php";                            
                        });
                    }
                });
			}
		});
	});

}());