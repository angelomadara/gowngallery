$(document).ready(function(){

	$('#show-categories').click(function(){
		// event when class .category-id is clicked

		// serialize = get all form values
		var cats = $('#cats').serialize();
		var types = $('#types').serialize();

		// var data = cats+"&"+types;
		var form = cats+"&"+types;
		
		// AJAX request get type
		// read the full documentaion of jquery ajax requests :-)
		$.get(
			'actions/show-categories.php',
			form,
			function(response){
				// console.log(JSON.parse(response));
				var elm = "";
				var data = JSON.parse(response);

				elm += "<div class='row'>";
				$.each(data, function(id, value){
						elm += "<div class='col-lg-3'>";
							elm += "<img src='images/small/"+value.image+"' >";
							elm += "<div class='product-description'>";
								elm += value.description;
							elm += "</div>";
						elm += "</div>";		
				});				
				elm += "</div>";

				$('.display-categories').html(elm);
			}
		);
		

	});

});