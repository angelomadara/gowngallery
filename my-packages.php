<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
];
require_once "includes/header.php";
if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
$packages = Query::fetchAll("SELECT * FROM package WHERE user_id = ? ",[$owner['user_id']]);

?>

<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
        </div>
    </div>
	
	<div class="row">	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative active">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
                <!--<li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>

            <ul class="breadcrumb">
                <li class="active">My Package</li>
            </ul>

        </div>
	</div>

    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <ul id="product-link">
                <li><a href='my-products.php'>+ Dresses / Tuxedos</a> </li>
                <li>
                    <a href='owner-clothings.php'>+ Custom Parts</a> 
                </li>
                <li><a href='my-packages.php' class="active">+ Packages</a> 
                    <ul>
                        <li><a href='owner-upload-package.php'>- add package</a></li>
                    </ul>
                </li>
            </ul>
        </div> 
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 asdf-product-holder">
            <h4>Parts</h4>
            <table class="table table-bordered table-hovered table-striped datatable">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th style="width:20%">Image</th>
                        <th>Type </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($packages):foreach($packages as $package): ?>
                        <tr>
                            <td><?=$package->pack_id?></td>
                            <td>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <br>
                                        <img src="<?=_image_url.'package/'.$package->image?>" alt="" style='width:100%'>
                                    <br>
                                </div>
                            </td>   
                            <td>
                                <p>Package name: <?=$package->name?> </p>
                                <p>Package price: PHP <?=number_format($package->price,2)?></p>
                                <p>Package note: <?=paragraph($package->note,'justify')?></p>
                                <div>
                                    <a class="btn btn-success" href='owner-update-package.php?id=<?=$package->pack_id?>' data-id="<?=$item->custom_id?>">Update item</a>
                                    <!-- <button class="btn btn-success" data-id="<?=$item->custom_id?>">Remove</button> -->
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; endif; ?>
                </tbody>
            </table>
        </div>  
    </div>

</div>



<?php 
	$scripts = ['notification.js'];
	require_once "includes/footer.php";
?>