<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$user = Session::get("user");

$length = 50;
$new_name = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).date('Ymd');

$file = Request::file('permit');
$status = 1;
$file_name = basename($file["name"]);
$ext_name = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
$new_name = $new_name.'.'.$ext_name;
// Check if image file is a actual image or fake image		
$checker = getimagesize($file["tmp_name"]); // array

// print_r($checker);

if($checker === false) {
    $msg .= "File is not an image. ";
    $status = 0;
}

// Check file size
if ($file["size"] > 500000) {
    $msg .= "Image file is too large. ";
    $status = 0;
}

// Allow certain file formats
if($ext_name != "jpg" && $ext_name != "png" && $ext_name != "jpeg" && $ext_name != "gif" ) {
    $msg .= "Only JPG, JPEG, PNG & GIF files are allowed. ";
    $status = 0;
}

if($status == 0){
	Redirect::to('my-shop.php?msg='.$msg);
	return 0;
}

$x = Query::update('store',[
	'permit'=>$new_name
],'user_id',$user['user_id']);

move_uploaded_file($file["tmp_name"],'../images/_temp/'.$new_name);

// resize photo
$resize = new Resize('../images/_temp/'.$new_name);
$resize->resizeImage(400, 600, 'portrait');
$resize->saveImage('../images/permits/'.$new_name,100);
// delete temporary photo
unlink('../images/_temp/'.$new_name);

Redirect::to('my-shop.php');
// Json::print($file);
// print_r($file);