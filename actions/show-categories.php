<?php 
include '../functions/defines.php';
include '../class/class.Database.php';
include '../class/class.Request.php';

global $db_handler;

// shorthand of IF
$categories = isset($_GET['category']) ? $_GET['category'] : null;
$types 		= isset($_GET['types']) ? $_GET['types'] : null;
$params = [];


// ang katumbas nila ay ganito
//if(isset($_GET['category'])){
	//$categories = $_GET['category'];
//}else{
//	$category = null;
//}

//show categories

$sql_search_product = "SELECT 
							*
						FROM product
						WHERE ";

if($categories){
	$sql_search_product .= "(";
	foreach ($_GET['category'] as $key => $value) {
		$sql_search_product .= "cat_id = ? ";
		if(($key+1) != count($_GET['category'])) $sql_search_product .= "OR ";
	}
	$params = $categories;
	$sql_search_product .= ") ";
}

if($categories && $types) {$sql_search_product .= "OR";} // will appear only if $categories and $types are both present

if($types){	
	$sql_search_product .= " (";
	foreach ($_GET['types'] as $key => $value) {
		$sql_search_product .= "type_id = ? ";
		if(($key+1) != count($_GET['types'])) $sql_search_product .= "OR ";
	}
	$params = array_merge($params,$types);
	$sql_search_product .= ")";
}

// will execute only if no category or type is selected
// sql query and params will be overwritten
if(!$categories && !$types){
	$sql_search_product = "SELECT * FROM product";
	$params = [];
}


// haha comment ko sana muna 'tong block of code
// echo $sql_search_product."<br>";
// print_r($params);
// exit();

// array_merge = combine two arrays to became one
// $params = array_merge($categories,$types);

$statement = $db_handler->prepare($sql_search_product);
$statement->execute($params);
$found_products = $statement->fetchAll(PDO::FETCH_OBJ); // fetch found data and convert it into object PDO::FETCH_OBJ


echo json_encode($found_products);