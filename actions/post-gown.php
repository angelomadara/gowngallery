<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$msg = "";
$online_user = Session::get('user');
$product_id = Request::get('item-id');
$user_id = $online_user['user_id'];

$title 	=  Request::get('title');
$message =  Request::get('message');
$file 	=  Request::file('photo');
$participants =  Request::get('participants');
$expiration = Request::get('expiration');
$status = 1;

// json::print($file);
$length = 50;
$new_name = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).date('Ymd');
$file_name = basename($file["name"]);
$ext_name = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
$new_name = $new_name.'.'.$ext_name;
// Check if image file is a actual image or fake image		
$checker = getimagesize($file["tmp_name"]); // array

if($checker === false) {
    $msg = "&error=File is not an image. ";
    $status = 0;
}

// Check file size
if ($file["size"] > 500000) {
    $msg = "&error=Image file is too large. ";
    $status = 0;
}

// Allow certain file formats
if($ext_name != "jpg" && $ext_name != "png" && $ext_name != "jpeg" && $ext_name != "gif" ) {
    $msg = "&error=Only JPG, JPEG, PNG & GIF files are allowed. ";
    $status = 0;
}

$msg .= "&title={$title}&message={$message}&participants={$participants}";

if($status == 0){
	Redirect::to('post-gown.php?'.$msg);
}

// return 0;
Query::create('feed',[
	'author' => $user_id,
	'title' => $title,
	'message' => $message,
	'photo' => $new_name,
	'max_participants' => $participants,
	'expiration' => date('Y-m-d',strtotime($expiration)),
	'created_at' => date('Y-m-d H:i:s'),
	'updated_at' => date('Y-m-d H:i:s'),
]);

// move file
move_uploaded_file($file["tmp_name"],'../images/_temp/'.$new_name);

// resize photo
$resize = new Resize('../images/_temp/'.$new_name);
$resize->resizeImage(200, 300, 'crop');
$resize->saveImage('../images/small/'.$new_name,100);
// $resize = new Resize('../images/_temp/'.$new_name);
$resize->resizeImage(400, 600, 'crop');
$resize->saveImage('../images/big/'.$new_name,100);
// delete temporary photo
unlink('../images/_temp/'.$new_name);

Redirect::to('post-gown.php?success=Gown is now posted.');