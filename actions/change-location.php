<?php 
session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$user = Session::get('user');
$user_id = $user['user_id'];

$lati = Request::get('lati');
$long = Request::get('long');

// Json::print([$lati,$long]);

Query::update('store',[
	'lat' => $lati,
	'lng' => $long,
],'user_id',$user_id);