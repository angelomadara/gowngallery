<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$online_user = Session::get('user');
$product_id = Request::get('item-id');
$user_id = $online_user['user_id'];

$sql = "SELECT sq.*  FROM size_and_quantity AS sq
		LEFT JOIN product AS p ON p.product_id = sq.product_id
		WHERE p.user_id = ? AND sq.size = ? AND p.product_id = ?";


// $q  = Query::update('size_and_quantity',['qty'=>100],'size_qty_id',1);

// Json::array($q);
$new_qty = 0; 
// $product_id = 0;
if($_POST['size']){
	foreach ($_POST['size'] as $key => $value) {
		// echo $key." - ".$value.'<br>';
		$get_size = Query::fetch($sql,[$user_id,$key,$product_id]);
		$new_qty = $get_size->qty + $value;
		// $product_id = $get_size->product_id;
		// Json::print($get_size);
		Query::update('size_and_quantity',['qty'=>$new_qty],'size_qty_id',$get_size->size_qty_id);
		// Json::print($get_size->size_qty_id);
	}
}

Redirect::to('owner-add-stock.php?item='.$product_id);