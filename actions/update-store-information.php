<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$user_id = Session::get('user')['user_id'];

$shopname 	= ucwords(strtolower(Request::get('shopname')));
$fname 		= ucwords(strtolower(Request::get('fname')));
$mname 		= ucwords(strtolower(Request::get('mname')));
$lname 		= ucwords(strtolower(Request::get('lname')));
$address 	= ucwords(strtolower(Request::get('address')));
$contact 	= Request::get('contact');

$x = Query::update('store',[
	'store_name' => $shopname
],'user_id',$user_id);

$y = Query::update('user_profile',[
	'first_name' => $fname,
	'last_name' => $lname,
	'middle_name' => $mname,
	'address' => $address,
	'contact' => $contact,
],'user_id',$user_id);


if($x && $y){
	echo json_encode([
		'swal' => [
			'title' => 'Update',
			'text' => 'Account information successfully updated',
			'type' => 'success'
		]
	]);
}else{
	echo json_encode([
		'swal' => [
			'title' => 'Update',
			'text' => 'Server error',
			'type' => 'warning'
		]
	]);
}

