<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
// include 'includes/owner_header.php';
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
$newFeeds = count(Query::fetchAll("SELECT feed_id FROM feed WHERE expiration > ?",[_date_time]));
$newOffers = 0;
$newOrders = count(Query::fetchAll("SELECT * FROM product_order WHERE status = ?",['pending']));

if(Request::get('welcome')){
    echo "<script> 
        swal({title:'',text:'".Request::get('welcome')."',type:'success'},function(e){if(e){location.href='owner.php';}});
    </script>";
}

?>
    
<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>

    </div>
    <div class="row" id="owner-dashboard" style="background: #fff">
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="active relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
                <!-- <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>

            <div>
                <ul class="breadcrumb">
                    <li class="active">Home</li>
                </ul>
            </div>
    
            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>rss.svg" alt="">
                    </div>
                    <div class="menu-desc relative">
                        <h4><a href='owner-feed.php'>NEWS FEEDS</a></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>wedding-dress1.svg" alt="" style='display: block;width: 100%'>
                    </div>
                    <div class="menu-desc relative">
                        <h4><a href='owner-customization-page.php'>CUSTOMIZED OFFERS</a></h4>
                    </div>
                </div>
            </div>
                        
            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>orders.png" alt="">
                    </div>
                    <div class="menu-desc relative">
                        <h4><a href='my-orders.php'>REQUESTS</a></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>shopping-2.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='my-products.php'>GALLERY</a></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>products.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='my-packages.php'>MY PACKAGES</a></h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-2">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>shop.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='my-shop.php'>MANAGE MY ACCOUNT</a></h4>
                    </div>
                </div>
            </div>

        </div>

        

    </div>
</div>

<?php 
    $scripts = ['notification.js'];
	include 'includes/footer.php';
 ?>