<?php 
$css = ['index.css','accordion.css','customization.css'];
include_once 'includes/header.php';

if(!Session::exist('user')){
    Redirect::to('login.php');
    // echo "<script>location.href='index.php'</script>";
    // header('Location:index.php');
}

$id = Request::get('store');
$page_response = "";
$found_store = Query::fetch("SELECT 
								* 
							FROM store AS s
							LEFT JOIN user_profile AS up ON up.user_id = s.user_id
							WHERE s.user_id = ?",[$id]);
if($found_store) {
	$parts = Query::fetchAll("SELECT * FROM customization WHERE store_id = ?",[$found_store->store_id]);
}
else{
	$page_response = "<h2 style='text-align:center;'>Store not found go <a href=\"javascript:history.go(-1)\" style=\"font-weight: bold;\">back</a> </h2>";
	$page_response .= '
					<div style="width: 30%;margin: auto">
						<img src="'._image_url.'wedding-dress1.svg" alt="" style=\'display: block;width: 100%\'>
					</div>';
}

?>
<div class="article">
    <div class="container"> 
    	<div class="row" style="background: #fff">     
    		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'><?=$page_response?></div>           
            <?php if($found_store): ?>
	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1 class="champagne" style="font-size: 6em;text-align: center;"> <?=$found_store->store_name?> </h1>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<h4>Parts</h4>

					<?php if($parts): ?>
					<ul class="cd-accordion-menu animated">
						<li class="has-children">
							<input type="checkbox" name ="group-1" id="group-1">
							<label for="group-1">Female</label>
				      		<ul>
				      			<li class="has-children">
				      				<input type="checkbox" name ="" id="sub-group-f-top">
									<label for="sub-group-f-top">Top</label>
									<ul>
									<?php foreach($parts as $value): ?>
										<?php if($value->type == "female-top"): ?>
											<li> 
												<a href='#1' class="part" data-part="<?=$value->type?>" data-id="<?=$value->custom_id?>">
													<img src="<?= _image_url.'parts/'.$value->image ?>">
													<?= $value->name ?>
													<div> PHP <span><?= number_format($value->price,2) ?></span> </div>
												</a> 
											</li>
										<?php endif; ?>
									<?php endforeach; ?>
									</ul>
				      			</li>
				      			<li class="has-children">
				      				<input type="checkbox" name ="" id="sub-group-f-belt">
									<label for="sub-group-f-belt">Belts</label>
									<ul>
									<?php foreach($parts as $value): ?>
										<?php if($value->type == "female-belt"): ?>
											<li> 
												<a href='#1' class="part" data-part="<?=$value->type?>" data-id="<?=$value->custom_id?>">
													<img src="<?= _image_url.'parts/'.$value->image ?>">
													<?= $value->name ?>
													<div> PHP <span><?= number_format($value->price,2) ?></span> </div>
												</a> 
											</li>
										<?php endif; ?>
									<?php endforeach; ?>
									</ul>
				      			</li>
				      			<li class="has-children">
				      				<input type="checkbox" name ="" id="sub-group-f-bot">
									<label for="sub-group-f-bot">Bottom</label>
									<ul>
									<?php foreach($parts as $value): ?>
										<?php if($value->type == "female-bottom"): ?>
											<li> 
												<a href='#1' class="part" data-part="<?=$value->type?>" data-id="<?=$value->custom_id?>">
													<img src="<?= _image_url.'parts/'.$value->image ?>">
													<?= $value->name ?>
													<div> PHP <span><?= number_format($value->price,2) ?></span> </div>
												</a> 
											</li>
										<?php endif; ?>
									<?php endforeach; ?>
									</ul>
				      			</li>
				      		</ul>
						</li>

						<li class="has-children">
							<input type="checkbox" name ="group-male" id="group-male">
							<label for="group-male">Male</label>
				      		<ul>
				      			<li class="has-children">
				      				<input type="checkbox" name ="sub-group-m-top" id="sub-group-m-top">
									<label for="sub-group-m-top">Top</label>
									<ul>
									<?php foreach($parts as $value): ?>
										<?php if($value->type == "male-top"): ?>
											<li> 
												<a href='#1' class="part" data-part="<?=$value->type?>" data-id="<?=$value->custom_id?>">
													<img src="<?= _image_url.'parts/'.$value->image ?>">
													<?= $value->name ?>
													<div> PHP <span><?= number_format($value->price,2) ?></span> </div>
												</a> 
											</li>
										<?php endif; ?>
									<?php endforeach; ?>
									</ul>
				      			</li>
				      			<li class="has-children">
				      				<input type="checkbox" name ="sub-group-m-bot" id="sub-group-m-bot">
									<label for="sub-group-m-bot">Bottom</label>
									<ul>
									<?php foreach($parts as $value): ?>
										<?php if($value->type == "male-bottom"): ?>
											<li> 
												<a href='#1' class="part" data-part="<?=$value->type?>" data-id="<?=$value->custom_id?>">
													<img src="<?= _image_url.'parts/'.$value->image ?>">
													<?= $value->name ?>
													<div> PHP <span><?= number_format($value->price,2) ?></span> </div>
												</a> 
											</li>
										<?php endif; ?>
									<?php endforeach; ?>
									</ul>
				      			</li>
				      		</ul>
						</li>
					</ul> 
					<?php endif; ?>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					
		            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
		                <li role="presentation" id="female-tab" class="active">
		                    <a href="#female"><b>Female</b></a>
		                </li>
		                <li role="presentation" id="male-tab" class="">
		                    <a href="#male"><b>Male</b></a>
		                </li>
		            </ul>
						
					<div id="female-div" class="">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 part-holder">
							<br><br>
							<div class="female-top"><img src="" id="top" alt=""></div>
							<div class="female-belt"><img src="" id="belt" alt=""></div>
							<div class="female-bottom"><img src="" id="bottom" alt=""></div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<div>
								<p>Information on how to avail this dress goes here</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, ratione, recusandae! Voluptate eos soluta, praesentium modi animi architecto quam! Repellat nam incidunt pariatur, aspernatur, perferendis nisi. Blanditiis adipisci odio laboriosam.</p>
							</div>
							<table class="table table-bordered">
								<thead>
									<tr><th colspan='2'>Cost Estimation</th></tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:40%">Top price:</td>
										<td class='top money'>0.00</td>
									</tr>
									<tr>
										<td>Belt price:</td>
										<td class='mid money'>0.00</td>
									</tr>
									<tr>
										<td>Bottom price:</td>
										<td class='bot money'>0.00</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td>Total</td>
										<td class="total money"></td>
									</tr>
								</tfoot>
							</table>
							
							<form id="f-form">
								<div class="input-wrapper">
									<label for="">Date Needed</label>
									<input type="text" name="date-needed" class="form-control datepicker" value="">
								</div>
								<!-- <div class="input-wrapper">
									<label for="">Quantity</label>
									<input type="number" name="quantity" class="form-control no-spin" value="1" min="1">
								</div> -->
								<div class="input-wrapper">
									<label for="">Note</label>
									<textarea name="note" id="" cols="30" rows="5" class="form-control" placeholder="Leave a note here for additional information about your desired gown."></textarea>
								</div>
								<input type="hidden" name="f-dress-id[]" id="f-top-input">
								<input type="hidden" name="f-dress-id[]" id="f-mid-input">
								<input type="hidden" name="f-dress-id[]" id="f-bot-input">
								<input type="hidden" name="store" value="<?=$id?>" id="">
								<div class="input-wrapper">
									<button type="button" class="send-request-btn btn btn-success">Send request</button>
								</div>
							</form>
						</div>
					</div>

					<div id="male-div" class="hidden">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 part-holder">
							<br><br>
							<div class="male-top"><img src="" id="top" alt=""></div>
							<div class="male-belt"><img src="" id="belt" alt=""></div>
							<div class="male-bottom"><img src="" id="bottom" alt=""></div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<div>								
								<p>Information on how to avail this toxido goes here</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, libero. Sit voluptatibus voluptate minus, voluptatem aut! Quod dolorem, quis modi, sed nobis necessitatibus quam cum aspernatur, quidem iste expedita alias.</p>
							</div>
							<table class="table table-bordered">
								<thead>
									<tr><th colspan='2'>Cost Estimation</th></tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:40%">Top price:</td>
										<td class='top money'>0.00</td>
									</tr>
									<!-- <tr>
										<td>Belt price:</td>
										<td class='mid money'>0.00</td>
									</tr> -->
									<tr>
										<td>Bottom price:</td>
										<td class='bot money'>0.00</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td>Total</td>
										<td class="total money"></td>
									</tr>
								</tfoot>
							</table>
							
							<form id="m-form">
								<div class="input-wrapper">
									<label for="">Date Needed</label>
									<input type="text" name="date-needed" class="form-control datepicker" value="">
								</div>
								<!-- <div class="input-wrapper">
									<label for="">Quantity</label>
									<input type="number" name="quantity" class="form-control no-spin" value="1" min="1">
								</div> -->
								<div class="input-wrapper">
									<label for="">Note</label>
									<textarea name="note" id="" cols="30" rows="5" class="form-control" placeholder="Leave a note here for additional information about your desired gown."></textarea>
								</div>
								<input type="hidden" name="m-cloth-id[]" id="m-top-input">
								<input type="hidden" name="m-cloth-id[]" id="m-bot-input">
								<input type="hidden" name="store" value="<?=$id?>" id="">
								<div class="input-wrapper">
									<button type="button" class="send-request-btn btn btn-success">Send request</button>
								</div>
							</form>
						</div>
					</div>

				</div>

            <?php endif; ?>
        </div>
    </div>
</div>


<?php 
	$scripts = ['customization.js'];
	include_once 'includes/footer.php';
?>