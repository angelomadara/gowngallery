<?php 
$css = [
    'index.css',
    'customer.css'
]; 
include_once 'includes/header.php';

if(!Session::isLogin('user') || Session::get('user')['user_type'] != "customer"){
    Redirect::to('index.php');
    return 0;
}
$customer = Session::get('user');

$profile = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$customer['user_id']]);
	
$my_orders = Query::fetchAll("SELECT 
								*
							FROM product_order AS po
							LEFT JOIN product AS p ON p.product_id = po.product_id
							LEFT JOIN store AS s ON s.user_id = p.user_id
							WHERE po.user_id = ? 
							ORDER BY order_id DESC
							",[$customer['user_id']]);
?>

<div class="container">
        <div class="row" style="background: #fff;">
           
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li class=""><a href='customer.php'>Home</a></li>
                    <li class="active">My Orders</li>
                </ul>
            </div>
        </div>
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <?php if($profile): ?>
            <div class="relative clearfix">
                <?php 
                    $photo = $profile->picture ? _image_url.'profile/'.$profile->picture : _image_url.'profile/'.rand(1,20).'.svg'; 
                ?>
                <img src="<?= $photo ?>" style='width:100%;display: block;margin: auto;' alt="">
                <br>
                <button id="update-photo" class="btn btn-success btn-sm pull-left">Update Picture</button>
            </div>
            <div style="color: #3498db;font-size:1.2em;margin-top:15px;">
            	<p><b><?= strtoupper($profile->first_name." ".$profile->middle_name." ".$profile->last_name) ?></b></p>
            </div>
            <div><?= $profile->contact ?></div>
            <div><?= title_case($profile->address) ?></div>
            <?php endif; ?>
	
			<br><br>
            <div>
            	<div>Quick links</div>
            	<ul id="post-ul">
            		<li class="active">
            			<a href='customer-orders.php'>Gowns/Tuxedos</a>
            		</li>
            		<li>
            			<a href='customer-orders-package.php' class="">Packages</a>
            		</li>
            	</ul>
            </div>

        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="customer.php"><b>Dashboard</b></a></li>
                <li role="presentation" class="active"><a href="customer-orders.php"><b>Requests</b></a></li>
                <li role="presentation"><a href="post-gown.php"><b>Post Dress</b></a></li>
                <li role="presentation"><a href="my-customization.php"><b>My Customization</b></a></li>
                <li role="presentation"><a href="customer-account.php"><b>Account</b></a></li>
            </ul>
            	
            <br>
            <div>
            <table class="table table-bordered datatable">
            	<thead>
            		<tr>
            			<th>&nbsp;</th>
            			<th>My orders</th>
            		</tr>
            	</thead>
            	<tbody>
	            <?php if($my_orders): ?>
					<?php foreach($my_orders as $order): ?>
	            	<tr><td><?=$order->order_id?></td><td>
						<div class="">
							<br>
							<div class="my-order">
								<header>
								</header>
								<section class="clearfix customer-order-holder" style="margin-bottom: 20px;">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<img src="<?=_image_url.'big/'.$order->image?>" style='width:100%'>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
										<div><b><?= title_case(cut_str($order->product_name,20)) ?></b></div>
										<span class="block"> <?= title_case(cut_str($order->store_name,20)) ?> </span>
										<span class="block"> <?= $order->qty ?> pcs </span>
										<span class="block">Php <?= number_format($order->price,2) ?></span>
										<span class="status">Status: <i class="order-status-<?= $order->status ?>"><?= $order->status ?></i> </span>
										<span class="block">DATE:</span>
										<span class='block txt-sm' style="padding-left:.5em"><i>Ordered: <?= pretty_date($order->date_order) ?></i></span>
										<span class='block txt-sm' style="padding-left:.5em"><i>Needed: <?= pretty_date($order->date_needed) ?></i></span>
										<span class='block txt-sm' style="padding-left:.5em">
											<i>Return: <?= $order->date_return == "1970-01-01 00:00:00" || $order->date_return == null ? '--- -- ----' : pretty_date($order->date_return) ?></i>
										</span>
										<?php if($order->status == 'pending'): ?>
											<button class="btn btn-default btn-sm cancel-order" data-id="<?=$order->order_id?>">Cancel Order</button>
										<?php elseif($order->status == 'approved'): ?>
											<p>Note: <?php echo $order->own_approve_note; ?></p>
										<?php elseif($order->status == 'returned'): ?>
											<p>Note: <?php echo $order->own_return_note; ?></p>
											<div>
												<?php $review = Query::fetch("SELECT * FROM product_comments WHERE user_id = ? AND product_id = ?",[$customer['user_id'],$order->product_id]); ?>
												<?php //if(!$review): ?>
													<button class="btn btn-success btn-sm review-dress" data-id="<?= $order->product_id ?>">
														Post/Edit a review about this dress
													</button>
													<button class="btn btn-success btn-sm rate-store" data-id="<?= $order->store_id ?>">
														Rate the store (<?= title_case(cut_str($order->store_name,20)) ?>)
														<!-- <span class="glyphicon glyphicon-star"></span> -->
													</button>
												<?php //endif; ?>
											</div>
										<?php elseif($order->status == 'canceled'): ?>
		                                    <?= $order->own_cancel_note ? "<p>Owner reason: ".$order->own_cancel_note."</p>" : null ?>
		                                    <?= $order->cus_cancel_note ? "<p>My reason: ".$order->cus_cancel_note."</p>" : null ?>
										<?php elseif($order->status == 'denied'): ?>
											<p>Reason: <?php echo $order->own_refuse_note; ?></p>
										<?php else: ?>
											<!-- <button class="btn btn-default btn-sm cancel-order" id="">Cancel Order</button> -->
										<?php endif; ?>
									</div>
								</section>
							</div>
						</div>
					</td></tr>
					<?php endforeach ?>
				<?php endif ?>
				</tbody>
			</table>
			</div>
        </div>

		


	</div>
</div>

<?php 
$scripts = ['customer.js'];
include_once 'includes/footer.php';
?>