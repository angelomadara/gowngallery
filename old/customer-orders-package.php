<?php 
$css = [
    'index.css',
    'customer.css'
]; 
include_once 'includes/header.php';

if(!Session::isLogin('user') || Session::get('user')['user_type'] != "customer"){
    Redirect::to('index.php');
    return 0;
}
$customer = Session::get('user');

$profile = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$customer['user_id']]);
	
$my_orders = Query::fetchAll("SELECT 
								*
							FROM package_order AS po
							LEFT JOIN package AS p ON p.pack_id = po.pack_id
							LEFT JOIN store AS s ON s.user_id = p.user_id
							WHERE po.user_id = ?  AND po.status != ?
							",[$customer['user_id'],'canceled']);

?>

<div class="container">
        <div class="row" style="background: #fff;">
           
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li class=""><a href='customer.php'>Home</a></li>
                    <li class="active">My Orders</li>
                </ul>
            </div>
        </div>
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <?php if($profile): ?>
            <div class="relative clearfix">
                <?php 
                    $photo = $profile->picture ? _image_url.'profile/'.$profile->picture : _image_url.'profile/'.rand(1,20).'.svg'; 
                ?>
                <img src="<?= $photo ?>" style='width:100%;display: block;margin: auto;' alt="">
                <br>
                <button id="update-photo" class="btn btn-success btn-sm pull-left">Update Picture</button>
            </div>
            <div style="color: #3498db;font-size:1.2em;margin-top:15px;">
            	<p><b><?= strtoupper($profile->first_name." ".$profile->middle_name." ".$profile->last_name) ?></b></p>
            </div>
            <div><?= $profile->contact ?></div>
            <div><?= title_case($profile->address) ?></div>
            <?php endif; ?>
	
			<br><br>
            <div>
            	<div>Quick links</div>
            	<ul id="post-ul">
            		<li>
            			<a href='customer-orders.php'>Gowns/Tuxedos</a>
            		</li>
            		<li class="active">
            			<a href='customer-orders-package.php' class="">Packages</a>
            		</li>
            	</ul>
            </div>

        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="customer.php"><b>Dashboard</b></a></li>
                <li role="presentation" class="active"><a href="customer-orders.php"><b>Requests</b></a></li>
                <li role="presentation"><a href="post-gown.php"><b>Post Dress</b></a></li>
                <li role="presentation"><a href="my-customization.php"><b>My Customization</b></a></li>
                <li role="presentation"><a href="customer-account.php"><b>Account</b></a></li>
            </ul>
            	
            <br>
            <div>
            <table class="table table-bordered datatable">
            	<thead>
            		<tr>
            			<th>&nbsp;</th>
            			<th>My orders</th>
            		</tr>
            	</thead>
            	<tbody>
	            <?php if($my_orders): ?>
					<?php foreach($my_orders as $order): ?>
	            	<tr><td></td><td>
						<!-- <div class="row"> -->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <img src="<?php echo _image_url.'package/'.$order->image ?>" alt="" style='width:100%'>
                            </div>
							<div class="col-lg- col-md- col-sm- col-xs-">
                                <div><?php echo $order->name ?></div>
                                <div>PHP <?php echo number_format($order->price,2) ?></div>
                                <div>Description: <?php echo paragraph($order->note) ?></div>
                                <hr>
                                <?php $stat = $order->status ?> 
                                <?php if($stat == 'accepted'): ?>
                                    <div>Status: <?php echo ucwords($stat); ?></div>
                                    <div>Note: <?php echo $order->store_accept_note; ?> </div>
                                <?php elseif($stat == 'open'): ?>
                                    <div>Status: <?php echo ucwords($stat); ?></div>
                                    <div>
                                        <button class="btn btn-default btn-sm cancel-package-btn" data-id='<?php echo $order->pack_order_id ?>'>Cancel order</button>
                                    </div>
                                <?php elseif($stat == 'returned'): ?>
                                    <div>Status: <?php echo ucwords($stat); ?></div>
                                    <div>Returned Date: <?php echo pretty_date($order->returned_at); ?></div>
                                <?php endif; ?>
                                <hr>
                            </div>
						<!-- </div> -->
					</td></tr>
					<?php endforeach ?>
				<?php endif ?>
				</tbody>
			</table>
			</div>
        </div>

		


	</div>
</div>

<?php 
$scripts = ['customer.js'];
include_once 'includes/footer.php';
?>