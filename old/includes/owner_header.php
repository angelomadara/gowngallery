<?php   

include 'functions/defines.php';
include 'class/Session.php';
include 'class/class.categories.php';
include 'class/class.types.php';

// echo "<pre>"; 
// print_r(Session::get('user'));
// echo "</pre>";

$loggedin_user = Session::get('user');

if($loggedin_user['user_type'] != 'owner'){
    // if the user is not equal to OWNER
    // include 404 page
    include("404.php");
    // will not read any preceding block of codes below him
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Gown Gallery</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/global.css">

    <?php 
      if($css){
        foreach ($css as $value) {
          echo "<link href='css/{$value}' rel='stylesheet' type='text/css'>\n";
        }
      }
    ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>


    <div class="container">
      <div class="row">       
        <div class="col-md-12">

          <div id="header">
            <img src="images/banner.jpg" width="100%">
          </div>

          
        <nav class="navbar navbar-default" style="margin-bottom:0px;">
          <div class="container">
            <div class="navbar-header">
             
              <!--<a class="nav navbar-nav " href="../index.php">Home</a>-->

            </div>
          <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
              <ul class="nav navbar-nav">
                <li><a href='#'>Home</a></li>
                <li><a href='all_products.php'>Collection</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
                    
                        <ul class="dropdown-menu" id="cat-choices">
                        <?php 
                            if($categories){
                                echo "<li> <a href='#'>";
                                echo "<form id='cats'>";
                                foreach ($categories as $key => $category) {
                                    echo "<input type='checkbox' class='category-id' name='category[]' value='".$category['cat_id']."'> ";
                                    echo $category['cat_name'];
                                    echo "<br>";
                                }
                                echo '</form>';
                                echo "</a></li>";
                            }
                        ?>  
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header">Type</li>
                        <?php 
                            if($types){                              
                              echo "<li><a href='#'> ";
                              echo "<form id='types'>";
                              foreach ($types as $key => $type) {
                                echo "<input type='checkbox' name='types[]' value='".$type['type_id']."'> ".$type['type_name'];
                                echo "<br>";
                              }
                              echo "</form>";
                              echo "</a></li>";
                            }
                        ?>
                            <li role='separator'></li>
                            <li class="dropdown-header">Show</li>
                            <li>
                                <button type="button" id="show-categories" class="btn btn-default btn-small">Show</button>
                            </li>
                        </ul>
                    
                </li>
                <li><a href="#">Customize/Upload</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>

                   
                
              </ul>

              <form class="navbar-form navbar-left" style="padding-left: 125px;">
                <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
              </form>

              <ul class="nav navbar-nav ">

                   <li class="dropdown">
                        <a href="#" class=" dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">   <span class="glyphicon glyphicon-menu-hamburger"></span></a>
                        <ul class="dropdown-menu">
                          <li class="" ><a href="owner.php?add_item">Add Item</a></li>
                          <li class="" ><a href='#'>View Item </a></li>
                          <li class="" ><a href='#'>View Order </a></li>
                          <li class="" ><a href='#'>View Customer </a></li>
                          
                          
                        </ul>
                      </li>
                    
                   <!--  <li> <a href="#" class="btn btn-warning btn-md">
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                          </a>
                            
                    </li>-->
                    <ul class="nav navbar-nav ">
                <?php 
                  if(Session::exist('user')){
                    echo '<li style="padding-left: 5px;"><a href="logout.php">Log out</a></li>';
                  }else{
                    echo '<li style="padding-left: 5px;"><a href="login.php">Log In</a></li>';
                  }
                ?>
                             
              
              </ul>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>


          <nav class="navbar navbar-default" style="margin-bottom: 0 !important;">
            <div class="container">
              <div class="navbar-header"> 

              </div>
            </div>
          </nav>

        </div>
      </div>
    </div>