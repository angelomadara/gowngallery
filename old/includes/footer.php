
	<div class="clearfix"></div>
	
	<!-- <div class="footer-holder"> -->
		<div class="container">
			<div class="row" style="background:#fff;">
				<div class="col-lg-12" style="text-align: right">
					<hr>
					<!-- <div id="footer"> -->
				        <!-- <h2> &copy; 2016 www.OnlineGownRental.com</h2>   -->
				        <p> &copy; <?=date('Y')?> www.OnlineGownRental.com</p>  
				    <!-- </div> -->
				</div>
			</div>
		</div>
	<!-- </div> -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-dialog.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <!-- <script src="js/sweetalert.min.js"></script> -->
    <script src="js/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?&libraries=places&key=AIzaSyBdtomfYGmIwWE2IPe5uui7PlpH-DBQgtI"></script>
    <script src="js/locationpicker.jquery.js"></script>  
    
    <script src="<?=_plugins?>/datatable/media/js/jquery.dataTables.min.js"></script>
    <script src="<?=_plugins?>/datatable/media/js/dataTables.bootstrap4.min.js"></script>  
    <script src="js/global.js"></script>
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jq-2.2.4/dt-1.10.13/af-2.1.3/cr-1.3.2/r-2.1.1/rr-1.2.0/se-1.2.0/datatables.min.js"></script> -->

    <?php 
        if(@$scripts){
            foreach ($scripts as $value) {
                echo "<script src='js/".$value."'></script>\n";
            }
        }
    ?>
  </body>
</html>
