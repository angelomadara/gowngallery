<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}

$owner = Session::get('user');
$item_id = Request::get("item");
// print_r($owner);

$my_products = Query::fetchAll("SELECT 
                                    p.product_id, p.product_name, p.description, p.price, p.image,
                                    c.cat_name, t.type_name, 
                                    sq.size, sq.qty, sq.size_qty_id
                                FROM product AS p 
                                LEFT JOIN category AS c ON c.cat_id = p.cat_id
                                LEFT JOIN type AS t ON t.type_id = p.type_id
                                LEFT JOIN size_and_quantity AS sq ON sq.product_id = p.product_id
                                WHERE p.user_id = ? AND p.product_id = ?",
                                [
                                    $owner['user_id'],
                                    $item_id,
                                ]);
 // Json::print($my_products); 
?>
    
<div class="container" style="min-height: 400px;">
    <!-- <div class="well"> -->
        <div class="row" style="background: #fff">
            <div class="col-lg-12">
                <h1 class="champagne store-name">
                    <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                    <?php echo $store ? $store->store_name : null ; ?>
                </h1>
                <!-- <hr> -->
            </div>

            <!-- <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href='owner.php'>Dashboard</a></li>
                    <li><a href='my-products.php'>My Products</a></li>
                    <li class="active">Add Stocks</li>
                </ul>
            </div>
           <div class="col-lg-12">
                <h3> <i class="glyphicon glyphicon-tags"></i> Add Stocks</h3>
           </div> -->
        </div>
        <div class="row" id="owner-dashboard" style="background: #fff">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                    <li role="presentation" class="relative">
                        <a href="owner.php"><b>Dashboard</b></a>
                    </li>
                    <li role="presentation" id="feed-tab" class="relative">
                        <a href="owner-feed.php"><b>Feeds</b></a>
                    </li>
                    <li role="presentation" id="custom-tab" class="relative">
                        <a href="owner-customization-page.php"><b>Customization</b></a>
                    </li>
                    <li role="presentation" id='order-tab' class="relative">
                        <a href="my-orders.php"><b>Orders</b></a>
                    </li>
                    <li role="presentation" class="relative active">
                        <a href="my-products.php"><b>Products</b></a>
                    </li>
                    <li role="presentation" class="relative">
                        <a href="my-packages.php"><b>Packages</b></a>
                    </li>
                    <li role="presentation" class="relative">
                        <a href="my-shop.php"><b>My Shop</b></a>
                    </li>
                </ul>
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href='my-products.php'>My Products</a></li>
                        <li class="active">Edit Product</li>
                    </ul>
                </div>
                <div class="col-lg-12">
                    <h3> <i class="glyphicon glyphicon-tags"></i> My products</h3>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <form action="actions/add-stocks.php" method="post" style="padding: 2em 0;">
                    <input type="hidden" name="item-id" value="<?=$item_id?>">
                    <?php if($my_products): ?>
                        <?php foreach ($my_products as $key => $value): ?> 
                            <div class="form-group">
                                <label for=""> <?= $value->product_name ." - ". $value->size?> </label>
                                <input type="number" class="form-control no-spin" name="size[<?=$value->size?>]" id="" value="" placeholder="">
                            </div> 
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <button type="submit" class="btn btn-success">Update Product</button>
                </form>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <?php 
                    if($my_products){
                        foreach ($my_products as $key => $value) {
                            echo "<div class='col-lg-4'>";
                            echo "    <div class='my-product-holder'>";
                            echo "        <div class='my-product-img'>";
                            echo "            <img src='"._image_url."small/".$value->image."' alt=''>";
                            echo "        </div>";
                            echo "        <div class='my-product-desc'>";
                            echo "           <p>".$value->product_name." - ". $value->size."</p>";
                            echo "           <p>Quantity: ".$value->qty." </p>";
                            echo "        </div>";
                            echo "    </div>";
                            echo "</div>";
                        }
                    }
                ?>
            </div>

        </div>
    <!-- </div> -->
</div>


<?php 
    $scripts = ['notification.js'];
	include 'includes/footer.php';
 ?>