<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}

$owner = Session::get('user');
$newFeeds = count(Query::fetchAll("SELECT feed_id FROM feed WHERE expiration > ?",[_date_time]));
$newOffers = 0;
$newOrders = count(Query::fetchAll("SELECT * FROM product_order WHERE status = ?",['pending']));
// print_r($owner);

$orders = Query::fetchAll("SELECT 
        po.*, saq.size, p.product_name, p.image,p.price, up.first_name, up.last_name, up.contact
    FROM product_order AS po
    LEFT JOIN product AS p ON p.product_id = po.product_id
    LEFT JOIN user_profile AS up ON up.user_id = po.user_id
    LEFT JOIN size_and_quantity AS saq ON saq.size_qty_id = po.size_id
    WHERE p.user_id = ?
    -- GROUP BY date_order 
    ORDER BY date_order ASC, order_id ASC
    ",[$owner['user_id']]);
// json::print($orders);

?>

<div class="container">
    
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-1 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative active">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
               <!--  <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>
            <ul class="breadcrumb">
                <li class="active">My Orders</li>
            </ul>
            
        </div>
        <div class="clear"></div>
        <!-- <br><br> -->
    </div>
    
    <div class="row">      
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <ul id="product-link">
                <li>
                    <a href='my-orders.php' class="active">Cloths</a> 
                </li>
                <li>
                    <a href='my-orders-packages.php'>Packages</a> 
                </li>
            </ul>
        </div>  
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th>id</th>
                        <th style="width:30%">Orders</th>
                        <th>Information</th>
                        <th style="width:20%">Controls</th>
                    </tr>
                </thead>
                <tbody>
                <?php if($orders): ?>
                    <?php foreach($orders as $order): ?>
                        <tr>
                            <td><?=$order->order_id?></td>
                            <td>
                                <div class="order-holder col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2em;">
                                    
                                    <br>
                                    <div class="order-body clearfix" id="order-page">
                                        <div>
                                            <img src="<?= _image_url.'small/'.$order->image ?>" alt="" style="width:100%;">                            
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h4><b><?= title_case($order->product_name)." [".$order->size."]" ?></b></h4>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>Customer Name:</td>
                                                <td> <?= title_case($order->first_name." ".$order->last_name) ?> </td>
                                            </tr>
                                            <tr> 
                                                <td>Contact Number:</td>
                                                <td> <?= str_replace(' ','-',$order->contact) ?>  </td>
                                            </tr>
                                            <tr> 
                                                <td>Quantity:</td>
                                                <td> <?= $order->qty ?> pcs  </td>
                                            </tr>
                                            <tr> 
                                                <td>Price:</td>
                                                <td>Php <?= number_format($order->price,2) ?> </td>
                                            </tr>
                                            <tr> 
                                                <td>Total of:</td>
                                                <td><b>Php <?= number_format(($order->price*$order->qty),2) ?></b> </td>
                                            </tr>
                                            <tr> 
                                                <td>Ordered Date:</td>
                                                <td> <?= pretty_date($order->date_order) ?>  </td>
                                            </tr>
                                            <tr>    
                                                <td>Date Needed:</td>
                                                <td> <?= pretty_date($order->date_needed) ?>  </td>
                                            </tr>
                                            <tr>    
                                                <td>Date Returned:</td>
                                                <td> <?= pretty_date($order->date_return) ?>  </td>
                                            </tr>
                                            <tr>
                                                <td>Order Status:</td>
                                                <td> <?= title_case($order->status) ?> </td>
                                            </tr>
                                        </tbody>
                                    </table>                            
                                </div>
                            </td>
                            <td>
                                <div class="order-header clearfix">
                                        <div class="my-order-buttons col-lg-12 col-md-12 col-sm-12 col-xs-12" data-id='<?= $order->order_id ?>'>
                                            <?php if($order->status == 'pending'): ?>
                                                <button class="btn btn-success btn-sm approve-btn" data-contact='<?=$order->contact?>'>Approved</button>
                                                <button class="btn btn-default btn-sm cancel-btn" data-contact='<?=$order->contact?>'>Cancel</button>
                                                <!-- <button class="btn btn-default btn-sm refuse-btn">Refuse</button> -->
                                            <?php elseif($order->status == 'canceled'): ?>
                                                <?= $order->own_cancel_note ? "<p>My reason: ".$order->own_cancel_note."</p>" : null ?>
                                                <?= $order->cus_cancel_note ? "<p>Customer reason: ".$order->cus_cancel_note."</p>" : null ?>
                                            <?php elseif($order->status == 'returned'): ?>
                                                <p><b>This order is closed</b></p>
                                                <p> <b>Note to the customer:</b> <?= $order->own_return_note ?> </p>
                                            <?php elseif($order->status == 'denied'): ?>
                                                <p> Reason: <?= $order->own_refuse_note ?> </p>
                                            <?php else: ?>
                                                <button class="btn btn-default btn-sm returned-btn" data-contact='<?=$order->contact?>'>Returned</button>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<?php 
    $scripts = ['my-orders.js','notification.js'];
    require_once "includes/footer.php"; 
?>