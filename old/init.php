<?php
@session_start();
require_once 'functions/defines.php';
require_once 'functions/functions.php';

spl_autoload_register(function($class){
	require_once "class/".$class.".php";
	// echo $class.'<br>';
});

require_once 'global.php';
// require_once 'plugins/PHPmailer/sendMail.php';


CartCookie::create();