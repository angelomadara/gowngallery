<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});


$id = Request::get('id');

$x = Query::remove("cart",[
	'cart_id' => $id,
]);

if($x){
	echo json_encode([
		'swal' => [
			'title' => '',
			'text' => 'Item is removed',
			'type' => 'success'
		]
	]);
}