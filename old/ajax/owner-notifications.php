<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');	

	$newFeeds = count(Query::fetchAll("SELECT feed_id FROM feed WHERE expiration > ? AND deleted_at IS NULL",[_date_time]));
	$newOffers = count(Query::fetchAll("SELECT * FROM customized_items WHERE store_id = ? AND status = ?",[$onlineUser['user_id'],'open']));
	$newOrders = Query::fetch("SELECT 
		COUNT(po.order_id) AS bilang
	FROM product_order AS po
	LEFT JOIN product AS p ON p.product_id = po.product_id
	WHERE po.status = ?
	AND p.user_id = ?
	",['pending',$onlineUser['user_id']]);

	$feed = $offer = $order = "";
	if($newFeeds > 0){
		$feed = "<span class='badge absolute' data-tooltip data-placement='top' data-original-title='".$newFeeds." post available' style='top:0px;right:0px;''>".$newFeeds."</span>";
	}
	if($newOffers > 0){
		$offer = "<span class='badge absolute' data-tooltip data-placement='top' data-original-title='".$newOffers." offers' style='top:0px;right:0px;''>".$newOffers."</span>";
	}
	if($newOrders->bilang > 0){
		$order = "<span class='badge absolute' data-tooltip data-placement='top' data-original-title='".$newOrders->bilang." orders ' style='top:0px;right:0px;''>".$newOrders->bilang."</span>";
	}

	echo json_encode([
		'feed' => $feed,
		'offer' => $offer,
		'order' => $order,
	]);
}