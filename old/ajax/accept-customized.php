<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$smsGateway = new SmsGateway(DEVICE_USER, DEVICE_PASS);
	$number = alter_number(Request::get("number"));
	$note = Request::get('note');
	$id = Request::get('id');
	$number = alter_number(Request::get('number'));
	
	$x = false;
	
	if($id == "" && $note == ""){
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> "You've forgotten to add note to the customer.",
				'type' => 'info'
			]
		]);
		exit();
	}else{
		$x = Query::update('customized_items',[
			'store_note' => $note,
			'status' => 'pending',
		],'item_id',$id);
		if($number){
			$smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);	
		}		
	}

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Customized order has been approved',
				'type' => 'success'
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error sending approval try to reload the page and try again',
				'type' => 'error'
			]
		]);
	}
}