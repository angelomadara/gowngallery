<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$id = Request::get('id');

	$x = Query::update('customized_items',[
		'status' => 'deal',
	],'item_id',$id);

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Request item is closed',
				'type' => 'success'
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error closing request try to reload the page and try again',
				'type' => 'error'
			]
		]);
	}
}