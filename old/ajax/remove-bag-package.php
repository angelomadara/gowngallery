<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

// if(!Session::isLogin('user')){
// 	return 0;
// }else{
$onlineUser = Session::get('user');
$id = Request::get('id');

$x = Query::remove("package_order",[
	'pack_order_id' => $id,
]);

if($x){
	echo json_encode([
		'swal' => [
			'title' => '',
			'text' => 'Package is removed',
			'type' => 'success'
		]
	]);
}
// }