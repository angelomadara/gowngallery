<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$onlineUser['user_id']]);

	$file = Request::file('file');
	$name = Request::get('name');
	$type = Request::get('type');
	$prce = Request::get('prce');
	$id   = Request::get('id');
	// exit();
	if(!$file){
		echo json_encode([
			'status' => 0,
			'swal' => [
				'title' => '',
				'text'	=> 'Image must not be empty.',
				'type' => 'info'
			]
		]);
		exit();
	}

	$status = 1;
	$length = 50;
	$new_name = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).date('Ymd');
	$file_name = basename($file["name"]);
	$ext_name = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
	$new_name = $new_name.'.'.$ext_name;
	// Check if image file is a actual image or fake image		
	$checker = getimagesize($file["tmp_name"]); // array

	if($checker === false) {
	    $msg = "File is not an image. ";
	    $status = 0;
	}

	// Check file size
	if ($file["size"] > 500000) {
	    $msg = "Image file is too large. ";
	    $status = 0;
	}

	// Allow certain file formats
	if($ext_name != "jpg" && $ext_name != "png" && $ext_name != "jpeg" && $ext_name != "gif" ) {
	    $msg = "Only JPG, JPEG, PNG & GIF files are allowed. ";
	    $status = 0;
	}

	if($status == 0){
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> $msg,
				'type' => 'info'
			]
		]);
		exit();
	}

	if($name == "" || $type == "" || $prce == ""){
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Name, type and price must have a value.',
				'type' => 'info'
			]
		]);
		exit();
	}

	if($id){
		$x = Query::update('customization',[
			'type' => $type,
			'name' => $name,
			'image' => $new_name,
			'store_id' => $store->store_id,
			'price' => $prce,
		],'custom_id',$id);
	}else{
		$x = Query::create('customization',[
			'type' => $type,
			'name' => $name,
			'image' => $new_name,
			'store_id' => $store->store_id,
			'price' => $prce,
		]);
	}

	if($x){
		// move file
		move_uploaded_file($file["tmp_name"],'../images/_temp/'.$new_name);
		$resize = new Resize('../images/_temp/'.$new_name);
		if($type == 'female-belt'){
			$resize->resizeImage(400, 400, 'auto');
		}else{
			$resize->resizeImage(400, 200, 'auto');
		}
		$resize->saveImage('../images/parts/'.$new_name,100);
		unlink('../images/_temp/'.$new_name);

		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Part uploaded',
				'type' => 'success'
			]
		]);
	}

}