<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$store = Request::get('id');

	$ratings = Query::fetch("SELECT * FROM store_rating WHERE store_id = ? AND voter_id = ?",[$store,$onlineUser['user_id']]);
	if($ratings){
		echo $ratings->rating_id;
	}else{
		echo null;
	}
}