<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$reply = Request::get('reply');
$feed_id = Request::get('feed-id');
$user_id = Request::get('user-id');
$store_id = Request::get('store-id');

if($reply == "" || $feed_id == "" || $user_id == ""){
	Json::encode([
		'status' => false,
		'swal' => [
			'title' => '',
			'text' => "Reply cannot be empty.",
			'type' => 'info'
		],
	]);
	return 0;
}

$x = Query::create('comments',[
	// 'comment_id' => $comment_id,
	'comment' => $reply,
	'feed_id' => $feed_id,
	'user_id' => $user_id,
	'reply_to' => $store_id,
	'created_at' => _date_time,
	'updated_at' => _date_time,
]);

// $x = Query::create('replies',[
// 	'comment_id' => $comment_id,
// 	'reply' => $reply,
// 	'user_id' => $user_id,
// 	'created_at' => _date_time,
// 	'updated_at' => _date_time,
// ]);

if($x){
	Json::encode([
		'status' => true,
		'swal' => [
			'title' => '',
			'text' => "New reply posted.",
			'type' => 'success'
		],
	]);
}else{
	Json::encode([
		'status' => false,
		'swal' => [
			'title' => '',
			'text' => "Error while saving reply.",
			'type' => 'info'
		],
	]);
}