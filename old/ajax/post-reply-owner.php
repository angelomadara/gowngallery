<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$current_user = Session::get('user');

$reply = Request::get('reply');
$feed_id = Request::get('feed-id');
$user_id = $current_user['user_id'];
$customer = Request::get('customer-id');

if($reply == "" || $feed_id == "" || $user_id == ""){
	Json::encode([
		'status' => false,
		'swal' => [
			'title' => '',
			'text' => "Reply cannot be empty.",
			'type' => 'info'
		],
	]);
	return 0;
}

$x = Query::create('comments',[
	// 'comment_id' => $comment_id,
	'comment' => $reply,
	'feed_id' => $feed_id,
	'user_id' => $user_id,
	'reply_to' => $customer,
	'created_at' => _date_time,
	'updated_at' => _date_time,
]);

if($x){
	Json::encode([
		'status' => true,
		'swal' => [
			'title' => '',
			'text' => "New reply posted.",
			'type' => 'success'
		],
	]);
}else{
	Json::encode([
		'status' => false,
		'swal' => [
			'title' => '',
			'text' => "Error while saving reply.",
			'type' => 'info'
		],
	]);
}