<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$feed_id = Request::get('feed_id');
$store_id = Request::get('store_id');

Query::raw("UPDATE comments SET isRead = ? WHERE feed_id = ? AND user_id = ?",[1,$feed_id,$store_id]);

$feed = Query::fetch("SELECT expiration, deleted_at FROM feed WHERE feed_id = ?",[$feed_id]);
$expiration_date = null;

if($feed){
	$expiration_date = date("Y-m-d H:i:s",strtotime($feed->expiration));
	$deleted_at = $feed->deleted_at;
}

$date_now = date('Y-m-d H:i:s');



if($date_now > $expiration_date || $deleted_at != null){
	echo 'disabled';
}else{
	echo '';
}