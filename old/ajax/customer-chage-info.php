<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$address	= Request::get('address');
	$contact	= Request::get('contact');
	$firstname	= Request::get('firstname');
	$lastname	= Request::get('lastname');
	$middlename	= Request::get('middlename');

	$x = Query::update('user_profile',[
		'first_name' => $firstname,
		'middle_name' => $middlename,
		'last_name' => $lastname,
		'address' => $address,
		'contact' => $contact,
	],'user_id',$onlineUser['user_id']);

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text' 	=> 'Profile successfully updated',
				'type'	=> 'success',
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text' 	=> 'Error encountered while updating profile',
				'type'	=> 'error',
			]
		]);
	}

}