<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$cookie = CartCookie::get();
$id = Request::get('id');
$value = Request::get('value');
$item_price = Request::get('price');

Query::update('cart',[
	'qty' => $value
],'cart_id',$id);

// echo ;

$qty = Query::fetchAll("SELECT * FROM cart WHERE cookie = ? AND status = ?",[$cookie,0]);

$total = 0;
foreach ($qty as $key) {
	$price = Query::fetch("SELECT price FROM product WHERE product_id = ?",[$key->product_id]);
	$total = $total + ($key->qty * $price->price);
}


echo json_encode([
	'total'=>number_format($total,2)
]);