<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$star = Request::get('star');	
	$store = Request::get('store');
	$rating_id = Request::get('rating-id');

	$ratings = Query::fetch("SELECT * FROM store_rating WHERE rating_id = ?",[$rating_id]);

	if(!$ratings){
		$x = Query::create('store_rating',[
			'store_id' => $store,
			'voter_id' => $onlineUser['user_id'],
			$star => 1
		]);
	}else{
		Query::update('store_rating',[
			'one' => 0, 'two' => 0, 'three' => 0, 'four' => 0, 'five' => 0,
		],'rating_id',$rating_id);

		$x = Query::update('store_rating',[
			$star => 1
		],'rating_id',$rating_id);
	}

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Ratings has been submitted thank you',
				'type' => 'success'
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error while submitting the stars',
				'type' => 'error'
			]
		]);
	}

}