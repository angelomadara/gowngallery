<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$productId = Request::get("id");
	$review = Query::fetch("SELECT * FROM product_comments WHERE user_id = ? AND product_id = ?",[$onlineUser['user_id'],$productId]);

	if($review){
		echo json_encode([
			'status' => true,
			'data' => $review
		]);
	}else{
		echo json_encode([
			'status' => false
		]);
	}
	
}