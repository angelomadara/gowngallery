<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$url 	= Request::get('control');
$note 	= Request::get('compose') == "" ? null : Request::get('compose');
$id 	= Request::get('order-id');

if(!Session::isLogin('user')){
	return 0;
}else{
	$response = [];
	$smsGateway = new SmsGateway(DEVICE_USER, DEVICE_PASS);
	$number = alter_number(Request::get("number"));
	// $sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
	
	if($url == 'approve'){
		$response = Query::update('package_order',[
			'store_accept_note' => $note,
			'status' => 'accepted',
			'updated_at' => _date
		],'pack_order_id',$id);
		$sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
	}
	elseif($url == 'cancel'){
		$response = Query::update('package_order',[
			'stor_cancel_note' => $note,
			'status' => 'canceled',
			'updated_at' => _date
		],'pack_order_id',$id);
		$sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
	}
	elseif($url == 'return'){
		$response = Query::update('package_order',[
			// 'own_return_note' => $note,
			'status' => 'returned',
			'updated_at' => _date,
			'returned_at' => _date
		],'pack_order_id',$id);
		$sms = $smsGateway->sendMessageToNumber($number, $note, DEVICE_ID, $sms_opt);
	}
	else{
		return 0;
	}


	// response 
	if($response){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text'	=> 'Order has been updated.',
				'type'	=> 'success',
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text'	=> 'Error encountered try to reload the page.',
				'type'	=> 'error',
			]
		]);
	}

}