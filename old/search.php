<?php 
$css = [
    'index.css'
];    
include_once 'includes/header.php';

$sql_category = "SELECT * FROM category ORDER BY cat_name ASC";
$sql_type = "SELECT * FROM type ORDER BY type_name ASC";
$get_categories = Query::fetchAll($sql_category);
$get_types = Query::fetchAll($sql_type);

$item       = Request::get("item");
$sort_by    = Request::get("sort-by");
$price      = Request::get("price");
$ballgown   = Request::get("ballgown");
$weddinggown = Request::get("weddinggown");
$vneck      = Request::get("vneck");
$sweetheart = Request::get("sweetheart");
$typeCheck = Request::get('type-check',false) ? Request::get('type-check',false) : [];
$catCheck = Request::get('cat-check',false) ? Request::get('cat-check',false) : [];

$items_sql = "SELECT 
            p.product_id, p.product_name, p.description, p.price, p.image,
            s.store_name, t.type_name, c.cat_name
        FROM product AS p 
        LEFT JOIN store AS s ON s.user_id = p.user_id 
        LEFT JOIN category AS c ON c.cat_id = p.cat_id
        LEFT JOIN type AS t ON t.type_id = p.type_id
        WHERE 
        (p.keywords LIKE ? OR p.product_name LIKE ? OR s.store_name LIKE ? )
        ";
if($typeCheck):
    // types
    $items_sql .= " AND (";
    $x = 1;    
    foreach ($typeCheck as $value) {
        $items_sql .= " t.code = ? ";
        if($x < count($typeCheck)){
            $items_sql .= " OR ";
        }
        $x++;
    }
    $items_sql .= ")";
endif;
if($catCheck):
    // category
    $items_sql .= " AND (";
    $x = 1;    
    foreach ($catCheck as $value) {
        $items_sql .= " c.code = ? ";
        if($x < count($catCheck)){
            $items_sql .= " OR ";
        }
        $x++;
    }
    $items_sql .= ")";
endif;

if($sort_by == "a-z"){
    $items_sql .= "ORDER BY p.product_name ASC ";
}
elseif($sort_by == "z-a"){
    $items_sql .= "ORDER BY p.product_name DESC ";
}
elseif($price == "low-high"){
    $items_sql .= "ORDER BY p.price ASC ";
}
elseif($price == "high-low"){
    $items_sql .= "ORDER BY p.price DESC ";
}
elseif($sort_by == "a-z" && $price == "low-high"){
    $items_sql .= "ORDER BY p.price ASC, p.product_name ASC ";
}
elseif($sort_by == "a-z" && $price == "high-low"){
    $items_sql .= "ORDER BY p.price DESC, p.product_name ASC ";
}
elseif($sort_by == "z-a" && $price == "low-high"){
    $items_sql .= "ORDER BY p.price ASC, p.product_name DESC ";
}
elseif($sort_by == "z-a" && $price == "high-low"){
    $items_sql .= "ORDER BY p.price DESC, p.product_name DESC ";
}

// echo $items_sql;
// exit();
$found_request = Query::fetchAll($items_sql,array_merge([
    '%'.$item.'%',
	'%'.$item.'%',
	'%'.$item.'%'
],$typeCheck,$catCheck));
// Json::pretty($found_request);
?>
<div class="article">
    <!-- <br> -->
    <div class="container"> 
        <!-- <div class="well"> -->
        	<div class="row" style="background: #fff; padding: 2em 0;">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <form method="get" action="search.php" class="" style="font-size:12px;">

                        <div><b>Search:</b></div>
                        <div class="form-group">
                            <input type="text" name="item" value="<?=Request::get('item')?>" class="form-control input-sm" placeholder="Search here">
                        </div>

                        <div class="form-group">
                            <p><b>Sort By:</b></p>
                            <div>
                                <input type="radio" name="sort-by" value="a-z" id="asc" <?php if(Request::get('sort-by')=='a-z'){echo "checked";} ?> >
                                <label for="asc" style="font-weight: normal;">A-Z</label>
                            </div>
                            <div>
                                <input type="radio" name="sort-by" value="z-a" id="desc" <?php if(Request::get('sort-by')=='z-a'){echo "checked";} ?> >
                                <label for="desc" style="font-weight: normal;">Z-A</label>
                            </div>
                            <div>
                                <input type="radio" name="price" value="low-high" id="low" <?php if(Request::get('price')=='low-high'){echo "checked";} ?> >
                                <label for="low" style="font-weight: normal;">Lowest to Highest Price</label>
                            </div>
                            <div>
                                <input type="radio" name="price" value="high-low" id="high" <?php if(Request::get('price')=='high-low'){echo "checked";} ?> >
                                <label for="high" style="font-weight: normal;">Highest to Lowest Price</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <p><b>Categories</b></p>
                            <?php $categories = Query::fetchAll($sql_category); ?>
                            <?php if($categories): ?>
                                <?php foreach($categories as $key => $value): ?>

                                    <?php $isOn = Request::get($value->code) == 'on' ? 'checked' : null; ?>

                                    <div>
                                        <input type="checkbox" name="cat-check[]" value="<?=$value->code?>" id="<?=$value->code?>" <?=$isOn?> >
                                        <label for="<?=$value->code?>" style='font-weight: normal;'> <?= $value->cat_name ?> </label>
                                    </div>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <p><b>Types</b></p>
                            <?php $types = Query::fetchAll($sql_type); ?>
                            <?php if($types): ?>
                                <?php foreach($types as $key => $value): ?>
                                    
                                    <?php $isOn = Request::get($value->code) == 'on' ? 'checked' : null; ?>

                                    <div>
                                        <input type="checkbox" name="type-check[]" value="<?= $value->code ?>" id="<?=$value->code?>" <?=$isOn?> >
                                        <label for="<?=$value->code?>" style='font-weight: normal;'> <?= $value->type_name ?> </label>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        
                        <div>
                            <input type="submit" class="btn btn-success" value="Search">
                        </div>

                    </form>
                </div>	
        		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        Resutls found for: 
                        <?php 
                            foreach ($typeCheck as $value) {
                                echo $value.', ';
                            }
                            foreach ($catCheck as $value) {
                                echo $value.', ';
                            }
                        ?>
                        <?php echo $item; ?>
                    </div>
                    <hr>
                    <div id ="content_area">
                            
                        <div class="display-categories clearfix">
                             
                            <?php   
                                if($found_request){
                                    foreach ($found_request as $key => $value) {
                                        $sizes = Query::fetchAll("SELECT * FROM size_and_quantity WHERE product_id = ?",[$value->product_id]);
                                        echo "<div class='col-lg-3'>";
                                            echo "<div class='product-holder clearfix'> 
                                                    <a href='info.php?prod_id={$value->product_id}' class='product-link'>";
                                                    echo "<img src='"._image_url."small/".$value->image."' alt='".$value->product_name."'>";   
                                                        echo "<div class='product-name'>";                                                     
                                                            if($value->product_name != "" || $value->product_name != null ){
                                                                // if discription is available
                                                                echo "<p>". mb_strimwidth(title_case($value->product_name), 0,23, '...') ."</p>";
                                                            }else{
                                                                // if no description available
                                                                echo "<p>No description available.</p>";
                                                            }
                                                        echo "<p class='store-name'>" .$value->store_name. "</p>";
                                                        echo "<p class='product-price'><b>Php " .number_format($value->price,2). "</b></p>";
                                                        echo "<span> Sizes: ";
                                                        foreach($sizes as $x => $size){
                                                            echo $size->size; $x = $x + 1;
                                                            if($x < count($sizes)) { echo ", "; }
                                                            $x++;
                                                        }
                                                        echo "</span>";

                                                        echo "<span class='pull-right glyphicon glyphicon-comment leave-message' data-id='{$value->product_id}'></span>";
                                                    echo "</div>";
                                            echo "</a>
                                            </div>";
                                        echo "</div>";
                                    }
                                }else{
                                    echo "<div class='col-lg-3'>";
                                        echo "No ".Request::get('item')." found.";
                                    echo "</div>";
                                }
                            ?>

                        </div>

                    </div>
                </div>

       		</div>
       	<!-- </div> -->
    </div>
</div>

<?php include 'includes/footer.php'; ?>