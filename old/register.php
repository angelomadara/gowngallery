<?php 
	$css = [];
	include 'includes/header.php';
?>

<div class="container">
	<!-- <div class="well"> -->
	<div class="row" style="background: #fff;">    		
		<div class="clearfix" style="width: 600px;margin: auto;padding:3em 0;">

			<div class="col-lg-12">
				<h1 style="text-align: center;">REGISTER AS</h1>
			</div>

			<div class="col-lg-6">
				<div class="register-buttons">
    				<img src="<?=_image_url?>store.png" alt="">
    				<a href="register-shopowner.php" class="btn btn-success btn-lg" style="width:100%;">
    					Shop Owner
    				</a>
				</div>
			</div>

			<div class="col-lg-6">
    			<div class="register-buttons">
    				<img src="<?=_image_url?>bag.png" alt="">
    				<a href="register-customer.php" class="btn btn-success btn-lg" style="width:100%;">
    					Customer
    				</a>
    			</div>
			</div>

		</div>
	</div>
    <!-- </div> -->
</div>

<?php 
	include 'includes/footer.php';
 ?>