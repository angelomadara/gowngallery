<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';
if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
$id = Request::get('id');
$profile = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$id]);
$feeds = Query::fetchAll("SELECT * FROM feed WHERE author = ? AND deleted_at IS NULL ORDER BY feed_id DESC",[$id]);
// Json::pretty($profile);
?>

<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
        </div>

    </div>
	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
	            <li role="presentation" class="relative">
	                <a href="owner.php"><b>Dashboard</b></a>
	            </li>
	            <li role="presentation" id="feed-tab" class="relative active">
	                <a href="owner-feed.php"><b>Feeds</b></a>
	            </li>
	            <li role="presentation" id="custom-tab" class="relative">
	                <a href="owner-customization-page.php"><b>Customization</b></a>
	            </li>
	            <li role="presentation" id='order-tab' class="relative">
	                <a href="my-orders.php"><b>Orders</b></a>
	            </li>
	            <li role="presentation" class="relative">
	                <a href="my-products.php"><b>Products</b></a>
	            </li>
	            <li role="presentation" class="relative">
	                <a href="my-packages.php"><b>Packages</b></a>
	            </li>
	            <li role="presentation" class="relative">
	                <a href="my-shop.php"><b>My Shop</b></a>
	            </li>
	        </ul>

	        <div>
	            <ul class="breadcrumb">
	                <li class=""><a href='javascript:history.go(-1)'> << Back</a></li>
	                <li class="active">Customer Profile</li>
	            </ul>
	        </div>
		</div>
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
	            <?php if($profile): ?>
		            <div class="relative clearfix">
		                <?php 
		                    $photo = $profile->picture ? _image_url.'profile/'.$profile->picture : _image_url.'profile/'.rand(1,20).'.svg'; 
		                ?>
		                <img src="<?= $photo ?>" style='width:100%;display: block;margin: auto;' alt="">
		                <br>
		            </div>
		            <div><?= title_case($profile->first_name." ".$profile->middle_name." ".$profile->last_name) ?></div>
		            <div><?= $profile->contact ?></div>
		            <div><?= title_case($profile->address) ?></div>
	            <?php endif; ?>
	        </div>

	        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
	        	<h4>Customer posts</h4>
				<?php if($feeds): ?>
					<?php foreach($feeds as $key => $value): ?>
		
						<div class='feed' style="margin-bottom: 3em;">
							<div class="feed-title" style="margin-bottom: 10px;">
								<b><?= title_case($value->title) ?></b> 
								<br>
								<span class="post-time"> 
									Posted date <?=pretty_date($value->created_at)?>
								</span>
								<span class="post-time"> / 
									Expiration date <?=pretty_date($value->expiration)?>
								</span>
							</div>
							<div class="feed-body clearfix">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
									<img src="<?=_image_url."small/".$value->photo?>" alt="" style='width:100%'>
									<div class="clearfix" style="margin-top: 5px;">
										<!-- <button class="btn btn-primary btn-sm">Close</button> -->
									</div>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">		
									<p><?= paragraph($value->message,'justify') ?></p>
									<hr>
								</div>
							</div>
						</div>

					<?php endforeach; ?>
				<?php endif; ?>
	        </div>
		</div>

	</div>
</div>
<?php 
include 'includes/footer.php';
?>