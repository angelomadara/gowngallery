<?php
$css = [
    'index.css',
    'customer.css'
];
include 'includes/header.php';

if(!Session::isLogin('user') || Session::get('user')['user_type'] != "customer"){
    Redirect::to('index.php');
    return 0;
}
$customer = Session::get('user');
$profile = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$customer['user_id']]);
// Json::print($customer);
$feeds = Query::fetchAll("SELECT * FROM feed WHERE author = ? AND deleted_at IS NOT NULL ORDER BY feed_id DESC",[$customer['user_id']]);

$sql_get_comments = "SELECT DISTINCT 
					s.store_name, s.store_id, c.feed_id, c.user_id
					FROM comments AS c
					LEFT JOIN store AS s ON s.user_id = c.user_id 
					WHERE c.feed_id = ? AND c.user_id != ? 
					ORDER BY comment_id DESC";
// $sql_get_replies = "SELECT * FROM replies WHERE comment_id = ? ORDER BY reply_id DESC LIMIT 0,5";
// Json::print(Query::fetchAll($sql_get_comments,[2,$customer['user_id']]));
?>
	
<div class="container" id='customer' data-id='<?=$customer["user_id"]?>'>
        <div class="row" style="background: #fff;">
           
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li class=""><a href='customer.php'>Home</a></li>
                    <li class="active">Post Dress</li>
                </ul>
            </div>
        </div>
	<div class="row">

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <?php if($profile): ?>
            <div class="relative clearfix">
                <?php 
                    $photo = $profile->picture ? _image_url.'profile/'.$profile->picture : _image_url.'profile/'.rand(1,20).'.svg'; 
                ?>
                <img src="<?= $photo ?>" style='width:100%;display: block;margin: auto;' alt="">
                <br>
                <button id="update-photo" class="btn btn-success btn-sm pull-left">Update Picture</button>
            </div>
            <div style="color: #3498db;font-size:1.2em;margin-top:15px;">
            	<p><b><?= strtoupper($profile->first_name." ".$profile->middle_name." ".$profile->last_name) ?></b></p>
            </div>
            <div><?= $profile->contact ?></div>
            <div><?= title_case($profile->address) ?></div>
            <?php endif; ?>
            <br><br>
            <div>
            	<div>Quick links</div>
            	<ul id="post-ul">
            		<li>
            			<a href='post-gown.php'>View active post</a>
            		</li>
            		<li class="active">
            			<a href='closed-post-gown.php' class="">View closed post</a>
            		</li>
            	</ul>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 clearfix">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="customer.php"><b>Dashboard</b></a></li>
                <li role="presentation"><a href="customer-orders.php"><b>Requests</b></a></li>
                <li role="presentation" class="active"><a href="post-gown.php"><b>Post Dress</b></a></li>
                <li role="presentation"><a href="my-customization.php"><b>My Customization</b></a></li>
                <li role="presentation"><a href="customer-account.php"><b>Account</b></a></li>
            </ul>
	
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<br>
				<?php
					if(Request::get('error')){
						echo "<p class='alert alert-info'>".Request::get('error')."</p>";
					}
					elseif(Request::get('success')){
						echo "<p class='alert alert-success'>".Request::get('success')."</p>";
					}
				?>
				<form method="POST" enctype="multipart/form-data" action="actions/post-gown.php">
					<div class="input-wrapper">
						<label for="">Title</label>
						<input type="text" name="title" class="form-control" value="<?=Request::get('title')?>" required>
					</div>
					<div class="input-wrapper">
						<label for="">Details/Description</label>
						<textarea name="message" id="" rows="5" class="form-control" required><?=Request::get('message')?></textarea>
					</div>
					<div class="input-wrapper">
						<label for="">Photo</label>
						<input type="file" name="photo" id="" class="form-control">
					</div>
					<div class="input-wrapper">
						<label for="">Shop limit count</label>
						<input type="number" name="participants" id="" value="<?=Request::get('participants')?>" class="form-control">
					</div>
					<div class="input-wrapper">
						<label for="">Post expiration</label>
						<input type="text" name="expiration" id="" class="datepicker form-control">
						<!-- <div class="datepicker"></div> -->
					</div>
					<div class="clearfix">
						<button type="submit" class="btn btn-success">Post Gown</button>
					</div>
				</form>
			</div>

			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
				<h3>My Closed Posts</h3>
				<?php if($feeds): ?>
					<?php foreach($feeds as $key => $value): ?>
		
						<div class='feed'>
							<div class="feed-title" style="margin-bottom: 10px;">
								<b><?= title_case($value->title) ?></b> 
								<br>
								<span class="post-time"> 
									Posted date <?=pretty_date($value->created_at)?>
								</span>
								<span class="post-time"> / 
									Expiration date <?=pretty_date($value->expiration)?>
								</span>
							</div>
							<div class="feed-body clearfix">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
									<img src="<?=_image_url."small/".$value->photo?>" alt="" style='width:100%'>
									<div class="clearfix" style="margin-top: 5px;">
										<!-- <button class="btn btn-primary btn-sm">Close</button> -->
									</div>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">									
									<p><?= paragraph($value->message,'justify') ?></p>
									<!-- <hr> -->
									<div class="feed-replies">
										<!-- <div class="dropdown pull-right">
											<button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
												<li><a href="#" class="edit-post" data-id="<?=$value->feed_id?>">Edit post</a></li>
												<li><a href="#" class="remove-post" data-id="<?=$value->feed_id?>">Closed post</a></li>
											</ul>
										</div> -->
										<p><b>Replies</b></p>
										<?php foreach(Query::fetchAll($sql_get_comments,[$value->feed_id,$customer['user_id']]) as $commentValue ): ?>
										<div class='comment-holder' style="margin-bottom: 10px;">
											<div>
												<?php 
													$newComments = Query::fetch("
														SELECT COUNT(*) AS bilang 
														FROM comments 
														WHERE feed_id = ? 
														AND isRead = ?
														AND user_id = ?",
													[$value->feed_id,0,$commentValue->user_id]); 
												?>
												<a 
													href='#' 
													class="a-conversations" 
													data-id='<?= $commentValue->feed_id ?>' 
													data-store='<?= $commentValue->user_id ?>'
													data-exp='<?=pretty_date($value->expiration)?>'
												>View <?= $newComments->bilang == 0 ? "" : '<span class="badge"> '.$newComments->bilang.' </span>' ?> </a> replies by 
												<a href="<?=_domain.'store-name.php?id='.$commentValue->user_id?>">
													<?= $commentValue->store_name ?>
												</a>:
											</div>
										</div>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>

					<?php endforeach; ?>
				<?php endif; ?>

			</div>

        </div>

	</div>
</div>

<?php
	$scripts = ['customer.js'];
	include 'includes/footer.php';
?>