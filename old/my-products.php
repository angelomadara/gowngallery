<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}

$owner = Session::get('user');
$newFeeds = count(Query::fetchAll("SELECT feed_id FROM feed WHERE expiration > ?",[_date_time]));
$newOffers = 0;
$newOrders = count(Query::fetchAll("SELECT * FROM product_order WHERE status = ?",['pending']));
// print_r($owner);

$my_products = Query::fetchAll("SELECT 
                                    p.product_id, p.product_name, p.description, p.price, p.image,
                                    c.cat_name, t.type_name
                                    -- sq.size, sq.qty, sq.size_qty_id
                                FROM product AS p 
                                LEFT JOIN category AS c ON c.cat_id = p.cat_id
                                LEFT JOIN type AS t ON t.type_id = p.type_id
                                -- LEFT JOIN size_and_quantity AS sq ON sq.product_id = p.product_id
                                WHERE p.user_id = ?",
                                [
                                    $owner['user_id']
                                ]);
if(Request::get('msg')){
    echo "<script> 
            swal({
                title:'',text:'".Request::get('msg')."',type:'".Request::get('info')."'
            },function(xxx){
                if(xxx){location.href = 'my-products.php';}
            }); 
        </script>";
} 
?>
    
<div class="container" style="min-height: 400px;">

        <div class="row" style="background: #fff">
            <div class="col-lg-12">
                <h1 class="champagne store-name">
                    <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                    <?php echo $store ? $store->store_name : null ; ?>
                </h1>
            </div>
        </div>

        <div class="row" id="owner-dashboard" style="background: #fff">
    
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                    <li role="presentation" class="relative">
                        <a href="owner.php"><b>Dashboard</b></a>
                    </li>
                    <li role="presentation" id="feed-tab" class="relative">
                        <a href="owner-feed.php"><b>Feeds</b></a>
                    </li>
                    <li role="presentation" id="custom-tab" class="relative">
                        <a href="owner-customization-page.php"><b>Customization</b></a>
                    </li>
                    <li role="presentation" id='order-tab' class="relative">
                        <a href="my-orders.php"><b>Requests</b></a>
                    </li>
                    <li role="presentation" class="relative active">
                        <a href="my-products.php"><b>Gallery</b></a>
                    </li>
                    <!-- <li role="presentation" class="relative">
                        <a href="my-packages.php"><b>Packages</b></a>
                    </li> -->
                    <li role="presentation" class="relative">
                        <a href="my-shop.php"><b>My Shop</b></a>
                    </li>
                </ul>
                <ul class="breadcrumb">
                    <li><a href='owner.php'>Dashboard</a></li>
                    <li class="active">Products</li>
                </ul>
                <!-- <div class="col-lg-12">
                    <h3> <i class="glyphicon glyphicon-tags"></i> My products</h3>
                </div> -->
            </div>

        </div>
    
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <ul id="product-link">
                    <li>
                        <a href='my-products.php' class="active">+ Dresses / Tuxedos</a> 
                        <ul>
                            <li><a href='owner-add-product.php'> - add product</a></li>
                        </ul>
                    </li>
                    <li><a href='owner-clothings.php'>+ Custom Parts</a> </li>
                    <li><a href='my-packages.php'>+ Packages</a> </li>
                </ul>
            </div>

            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 asdf-product-holder">
                <h4>Dresses and Tuxedos</h4>
                <!-- <div class="col-lg-2">
                    <div class="my-product-holder">
                        <div class="my-product-img" style="padding-top:3em">
                            <img src="<?=_image_url?>shopping-2.png" alt="">
                        </div>
                        <div class="my-product-desc" style="padding-top: 2.6em">
                            <p> <a href='owner-add-product.php' class="b">+ Add Product</a> </p>
                        </div>
                    </div>
                </div> -->
                <?php 
                    if($my_products){
                        foreach ($my_products as $key => $value) {
                            $sizes = Query::fetchAll("SELECT * FROM size_and_quantity WHERE product_id = ?",[$value->product_id]);
                            echo "<div class='col-lg-2'>";
                            echo "    <div class='my-product-holder'>";
                            echo "        <div class='my-product-img'>";
                            echo "            <img src='"._image_url."small/".$value->image."' alt=''>";
                            echo "        </div>";
                            echo "        <div class='my-product-desc'>";
                            echo "           <p>".title_case(mb_strimwidth($value->product_name,0,16,'...'))."</p>";
                            echo "           <p style='height:40px;'>Size: ";
                                            foreach($sizes as $x => $size){
                                                echo "[".$size->qty."-".$size->size."]"; $x = $x + 1;
                                                if($x < count($sizes)) { echo ", "; }
                                                $x++;
                                            }
                            echo "           </p>";
                            // echo "           <p>".htmlspecialchars_decode($value->description)."</p>";
                            echo "           <div>
                                                <a href='owner-edit-product.php?item={$value->product_id}' class='btn btn-primary btn-sm'>Edit</a> |
                                                <!-- <a href='#' class=''>Hide</a> | -->
                                                <a href='owner-add-stock.php?item={$value->product_id}' class='btn btn-primary btn-sm'>Stocks</a> 
                                            </div>";
                            echo "        </div>";
                            echo "    </div>";
                            echo "</div>";
                        }
                    }
                ?>                
            </div>

        </div>

    <!-- </div> -->
</div>


<?php 
    $scripts = ['notification.js'];
	include 'includes/footer.php';
 ?>