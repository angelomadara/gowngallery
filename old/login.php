<?php 
	$css = [
		'login.css'
	];

	include_once 'includes/header.php';

    
    if(Session::exist('user')){
        Redirect::to('index.php');
        // echo "<script>location.href='index.php'</script>";
        // header('Location:index.php');
    }
    
?>
<!-- <div class="article"> -->
<div class="container">
    
    <div class="row" style="background: #fff">
    
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-lg-offset-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4">
            <form class="form-signin" method="POST" action="actions/login.php" style="min-height: 400px;">
                <h2 class="form-signin-heading">Sign In Here!</h2>

                <?php 
                    if(Request::get('message')){
                        // if get method on the url is present named 'message'
                        // this php block will appear
                        echo "<div>";
                        echo "  <p class=\"alert alert-danger\">".Request::get('message')."</p>";    
                        echo "</div>";      
                    }
                    if(Request::get('continue-msg')){
                        echo "<div>";
                        echo "  <p class=\"alert alert-info\">".Request::get('continue-msg')."</p>";    
                        echo "</div>";      
                    }
                ?>

                <div class="input-wrapper">
                    <label for="inputEmail" class="sr-only">Email address</label>
                    <input name="username" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                </div>
                <div class="input-wrapper">
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" value="remember-me"> Remember me</label>
                    <a href="register.php" style="float:right;">Sign Up</a>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
            </form>
        </div>

    
    
    </div>

</div>

<!-- </div> -->
<?php include 'includes/footer.php'; ?>