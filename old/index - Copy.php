<?php 
$css = [
    'index.css'
];    
include_once 'includes/header.php';
$products = Query::fetchAll("SELECT 
                                p.product_id, p.product_name, p.description, p.price, p.image,
                                s.store_name
                            FROM product AS p 
                            LEFT JOIN store AS s ON s.user_id = p.user_id");

$sql_category = "SELECT * FROM category";
$sql_type = "SELECT * FROM type";
$get_categories = Query::fetchAll($sql_category);
$get_types = Query::fetchAll($sql_type);

?>
    
<div class="article">
    <!-- <br> -->
    <div class="container"> 
        <!-- <div class="well"> -->
        	<div class="row" style="background: #fff;padding-top:2em;">  
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <form method="get" action="search.php" class="" style="font-size:12px;">

                        <div><b>Search:</b></div>
                        <div class="form-group">
                            <input type="text" name="item" value="<?=Request::get('item')?>" class="form-control input-sm" placeholder="Search here">
                        </div>

                        <div class="form-group">
                            <p><b>Sort By:</b></p>
                            <div>
                                <input type="radio" name="sort-by" value="a-z" id="asc" checked>
                                <label for="asc" style="font-weight: normal;">A-Z</label>
                            </div>
                            <div>
                                <input type="radio" name="sort-by" value="z-a" id="desc">
                                <label for="desc" style="font-weight: normal;">Z-A</label>
                            </div>
                            <div>
                                <input type="radio" name="price" value="low-high" id="low" checked>
                                <label for="low" style="font-weight: normal;">Lowest to Highest Price</label>
                            </div>
                            <div>
                                <input type="radio" name="price" value="high-low" id="high">
                                <label for="high" style="font-weight: normal;">Highest to Lowest Price</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <p><b>Categories</b></p>
                            <?php $categories = Query::fetchAll($sql_category); ?>
                            <?php if($categories): ?>
                                <?php foreach($categories as $key => $value): ?>

                                    <?php $isOn = Request::get($value->code) == 'on' ? 'checked' : null; ?>

                                    <div>
                                        <input type="checkbox" name="<?=$value->code?>" id="<?=$value->code?>" <?=$isOn?> >
                                        <label for="<?=$value->code?>" style='font-weight: normal;'> <?= $value->cat_name ?> </label>
                                    </div>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <p><b>Types</b></p>
                            <?php $types = Query::fetchAll($sql_type); ?>
                            <?php if($types): ?>
                                <?php foreach($types as $key => $value): ?>
                                    
                                    <?php $isOn = Request::get($value->code) == 'on' ? 'checked' : null; ?>

                                    <div>
                                        <input type="checkbox" name="<?=$value->code?>" id="<?=$value->code?>" <?=$isOn?> >
                                        <label for="<?=$value->code?>" style='font-weight: normal;'> <?= $value->type_name ?> </label>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        
                        <div>
                            <input type="submit" class="btn btn-success" value="Search">
                        </div>

                    </form>
                </div>    		
        		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                    <div id ="content_area">                            
                        <div class="display-categories">
                             
                            <?php   
                                // if products is available
                                if($products){

                                    foreach ($products as $key => $value) {

                                        $sizes = Query::fetchAll("SELECT * FROM size_and_quantity WHERE product_id = ? AND isHidden = ? AND qty != ?",[$value->product_id,0,0]);

                                        echo "<div class='col-lg-3'>";
                                            echo "<div class='product-holder clearfix'> 
                                                    <a href='info.php?prod_id={$value->product_id}' class='product-link'>";
                                                    echo "<img src='"._image_url."small/".$value->image."' alt='".$value->product_name."'>";   
                                                        echo "<div class='product-name'>";                                                     
                                                            if($value->product_name != "" || $value->product_name != null ){
                                                                // if discription is available
                                                                echo "<p>". mb_strimwidth($value->product_name, 0,23, '...') ."</p>";
                                                            }else{
                                                                // if no description available
                                                                echo "<p>No description available.</p>";
                                                            }
                                                        echo "<p class='store-name'>" .$value->store_name. "</p>";
                                                        echo "<p class='product-price'><b>Php " .number_format($value->price,2). "</b></p>";
                                                        if($sizes){
                                                            echo "<span> Sizes: ";
                                                            foreach($sizes as $x => $size){
                                                                echo $size->size; $x = $x + 1;
                                                                if($x < count($sizes)) { echo ", "; }
                                                                $x++;
                                                            }
                                                            echo "</span>";
                                                        }else {echo "<span>Out of stock.</span>";}
                                                        echo "<span class='pull-right glyphicon glyphicon-comment leave-message' data-id='{$value->product_id}'></span>";
                                                    echo "</div>";
                                            echo "</a>
                                            </div>";
                                        echo "</div>";
                                    }
                                }else{
                                    echo "<div class='col-lg-3'>";
                                        echo "No products found.";
                                    echo "</div>";
                                }
                            ?>

                        </div>

                    </div>
                </div>

       		</div>
       	<!-- </div> -->
    </div>
</div>

<?php 
    $scripts = ['cart.js'];
	include 'includes/footer.php';
 ?>
 