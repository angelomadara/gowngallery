<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(!Session::isLogin('user') || Session::get('user')['user_type'] != "customer"){
    Redirect::to('index.php');
    return 0;
}
$customer = Session::get('user');
$profile = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$customer['user_id']]);
// Json::pretty($customer)
?>
    
<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff;">           
        <div class="col-lg-12">
            <ul class="breadcrumb">
                <li class="active">Home</li>
            </ul>
        </div>
    </div>
    <div class="row" id="owner-dashboard" style="background: #fff;min-height: 500px;">
        
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <?php if($profile): ?>
            <div class="relative clearfix">
                <?php 
                    $photo = $profile->picture ? _image_url.'profile/'.$profile->picture : _image_url.'profile/'.rand(1,20).'.svg'; 
                ?>
                <img src="<?= $photo ?>" style='width:100%;display: block;margin: auto;' alt="">
                <br>
                <button id="update-photo" class="btn btn-success btn-sm pull-left">Update Picture</button>
            </div>
            <div style="color: #3498db;font-size:1.2em;margin-top:15px;">
                <p><b><?= strtoupper($profile->first_name." ".$profile->middle_name." ".$profile->last_name) ?></b></p>
            </div>
            <div><?= $profile->contact ?></div>
            <div><?= title_case($profile->address) ?></div>
            <?php endif; ?>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a href="customer.php"><b>Dashboard</b></a></li>
                <li role="presentation"><a href="customer-orders.php"><b>Requests</b></a></li>
                <li role="presentation"><a href="post-gown.php"><b>Post Dress</b></a></li>
                <li role="presentation"><a href="my-customization.php"><b>My Customization</b></a></li>
                <li role="presentation"><a href="customer-account.php"><b>Account</b></a></li>
            </ul>
            <div class="col-lg-3">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>orders.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='customer-orders.php'>Requests</a></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>wedding-dress.svg" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='post-gown.php'>Post Dress</a></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>wedding-dress1.svg" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='my-customization.php'>My Customization</a></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="owner-menu">
                    <div class="menu-icon">
                        <img src="<?=_image_url?>boss-1.png" alt="">
                    </div>
                    <div class="menu-desc">
                        <h4><a href='customer-account.php'>My Account</a></h4>
                    </div>
                </div>
            </div>
        </div>

       
        
    </div>
</div>


<?php 
    $scripts = ["customer.js"];
	include 'includes/footer.php';
 ?>