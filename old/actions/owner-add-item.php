<?php 
session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(Session::exist('user')){

	$online_user = Session::get('user');
    $msg = "Encountered errors: ";
    $status = 1;

	$product_title 		= Request::get('product_title');
	// $product_quantity 	= Request::get('product_quantity');
	$product_size 		= Request::get('product_size',false);
	$product_cat 		= Request::get('product_cat');
	$product_type 		= Request::get('product_type');
	$product_image 		= Request::file('product_image');
	$product_price 		= Request::get('product_price');
	$product_desc 		= Request::get('product_desc');
	$product_keywords 	= Request::get('product_keywords');
	
	$length = 50;
    $new_name = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).date('Ymd');

	if($product_title=="" || $product_cat=="" || $product_type=="" || $product_price=="" || $product_keywords==""){
		$msg .= "Fill all important fields. ";
		$status = 0;
	}

	if(count($product_size) <= 0) { 
		$msg.= "Select a size. ";
		$status = 0;
	}

	// Json::print($product_image);

	// $target_dir = "uploads/";
	$file_name = basename($product_image["name"]);
	$ext_name = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
	$new_name = $new_name.'.'.$ext_name;
	// Check if image file is a actual image or fake image		
    $checker = getimagesize($product_image["tmp_name"]); // array

    if($checker === false) {
        $msg .= "File is not an image. ";
	    $status = 0;
    }

	// Check file size
	if ($product_image["size"] > 500000) {
	    $msg .= "Image file is too large. ";
	    $status = 0;
	}

	// Allow certain file formats
	if($ext_name != "jpg" && $ext_name != "png" && $ext_name != "jpeg" && $ext_name != "gif" ) {
	    $msg .= "Only JPG, JPEG, PNG and GIF files are allowed. ";
	    $status = 0;
	}

	$msg .= "&prod={$product_title}&price={$product_price}&desc={$product_desc}&key={$product_keywords}&info=error";

	if($status == 0){
		Redirect::to('owner-add-product.php?msg='.$msg);
	}else{
		$item_id = Query::create('product',[
			'user_id' 		=> $online_user['user_id'],
			'type_id' 		=> $product_type,
			'cat_id' 		=> $product_cat,
			'product_name' 	=> $product_title,
			'description' 	=> $product_desc,
			'price' 		=> $product_price,
			// 'image' 		=> $product_image['name'],
			'image' 		=> $new_name,
			'keywords' 		=> $product_keywords,
		]);

		$id = $item_id->lastInsertId();
		// save sizes on the other table
		foreach ($product_size as $key => $value) {		
			Query::create('size_and_quantity',[
				'product_id' => $id,
				'size' => $value
			]);
		}
		// move file
		// move_uploaded_file($product_image["tmp_name"],'../images/small/'.$new_name);
		move_uploaded_file($product_image["tmp_name"],'../images/_temp/'.$new_name);

		// resize photo
		$resize = new Resize('../images/_temp/'.$new_name);
		$resize->resizeImage(200, 300, 'crop');
		$resize->saveImage('../images/small/'.$new_name,100);
		// $resize = new Resize('../images/_temp/'.$new_name);
		$resize->resizeImage(400, 600, 'crop');
		$resize->saveImage('../images/big/'.$new_name,100);
		// delete temporary photo
		unlink('../images/_temp/'.$new_name);

		Redirect::to('my-products.php?msg=New product added.&info=success');
	}

}


exit();
