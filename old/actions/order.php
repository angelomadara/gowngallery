<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	@require_once "../class/".$class.".php";
});

$isLogin = Session::get('user');
$cookie = CartCookie::get('_cartCookie');
$date_needed = Request::get('date-needed',false);
$pack_date_needed = Request::get('pack-date-needed',false);
$msg = "?";

// print_r($pack_date_needed);
// exit();

// if(count($date_needed) < 1){
// 	Redirect::to("bag.php?error=Cannot find any items on your bag to continue this action");
// 	exit();
// }

if(!$isLogin){
	Redirect::to('login.php?continue-msg=Login your first account to continue shopping');
	return 0;
}else{
	$items = Query::fetchAll('SELECT * FROM cart WHERE cookie = ? AND status = ?',[$cookie,0]);
	$packs = Query::fetchAll('SELECT * FROM package_order WHERE cookie = ? AND status = ?',[$cookie,'open']);

	
	if(count($items) > 0){
		foreach ($items as $key => $value) {
			Query::create('product_order',[
				'product_id' => $value->product_id,
				'qty' => $value->qty,
				'user_id' => $isLogin['user_id'],
				// 'date_needed' => db_date_time(Request::get("date-needed")),
				'date_needed' => db_date_time($date_needed[$key]),
				'date_order' => _date_time,
				'size_id' => $value->size_id,
				// 'date_return' => db_date_time($return_date),
				'status' => 'pending',
				'created_at'=>_date,
				'updated_at'=>_date,
			]);		

			Query::update('cart',[
				'status' => 1
			],'cart_id',$value->cart_id);
		}

		CartCookie::refresh();
	}

	if(count($packs) > 0){
		foreach ($packs as $key => $pack) {
			Query::update('package_order',[
				'status' => 'pending',
				// 'status' => 'open',
				'user_id' => $isLogin['user_id'],
				'created_at' => _date,
				'updated_at' => _date,
				'date_needed' => db_date_time($pack_date_needed[$key]),
			],'pack_order_id',$pack->pack_order_id);
		}
	}
	Redirect::to('bag.php?success=Your request dresse(s) are now on process please wait for the shop owners confirmation. Thank you.');

}



