<?php 

session_start();
include '../class/Session.php';
include '../class/Request.php';
include '../class/Cart.php';
include '../class/CartCookie.php';


$cookie = CartCookie::get();

$found_cart_items = Cart::get([$cookie]);

$cart_count = count($found_cart_items);

$total_price = 0;
if($found_cart_items){
	foreach ($found_cart_items as $key => $value) {
		$total_price = $total_price + ($value->price * $value->qty);
	}
}

// convert array to json object
echo json_encode([
	'cart' => $found_cart_items,
	'count' => $cart_count,
	'total_price' => number_format($total_price,2)
]);