<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(Session::isLogin('user')){
	Redirect::to('index.php');
}

// print_r(Session::get('user'));

$username = Request::get('username');
$password = Request::get('password');
$err_message = "Incorrect credentials";

$found_user = Query::fetch("SELECT 
	 							a.user_id, a.username, a.password, a.user_type,
	 							b.first_name, b.last_name, b.middle_name, b.address, b.contact,
                                s.store_name, s.description, a.status
	 						FROM 
	 							user AS a
	 						LEFT JOIN
	 							user_profile AS b ON b.user_id = a.user_id
                            LEFT JOIN
                            	store AS s ON s.user_id = a.user_id
	 						WHERE 
	 							username = ?",[$username]);


// Json::pretty($found_user);
// exit();

// if username is found 
if($found_user){
	// check if the password is correct
	// Json::pretty($found_user);

	if($found_user->status == 0){
		$err_message = "Account has not been activated yet";
		header("Location: ../login.php?continue-msg=".$err_message);
		exit();
	}

	$isSimilar = PasswordHash::check_password($found_user->password, $password);
	if($isSimilar){

		$profile_array = [
			'user_id' 		=> $found_user->user_id,
			'username' 		=> $found_user->username,
			'user_type' 	=> $found_user->user_type,
			'firstname' 	=> $found_user->first_name,
			'lastname' 		=> $found_user->last_name,
			'middlename' 	=> $found_user->middle_name,
			'address' 		=> $found_user->address,
			'contact' 		=> $found_user->contact,
			'store'			=> $found_user->store,
			'store_desc'	=> $found_user->description
		];

		Session::put('user',$profile_array);

		if($found_user->user_type == 'owner'){
			header("Location: ../owner.php");
		}else{
			header("Location: ../index.php");
		}

	}else{
		// if password is not similar 
		header("Location: ../login.php?message=".$err_message);
	}

}
// if username not found
else{
	// user not found
	// Json::pretty([
	// 	'message' => 'Username and password is incorrect'
	// ]);
	header("Location: ../login.php?message=".$err_message);
}

exit();
// include '../functions/defines.php';
// include '../class/Database.php';
// include '../class/Request.php';
// include '../class/PasswordHash.php';
// include '../class/Session.php';

// global $db_handler;

// $username_to_find = Request::get('username');
// $password_to_compare = Request::get('password');

// // echo PasswordHash::hash('123456');

// // search username first if available

// $sql_search_username = "SELECT 
// 							a.user_id, a.username, a.password, a.user_type,
// 							b.first_name, b.last_name, b.middle_name, b.address, b.contact 
// 						FROM 
// 							user AS a
// 						LEFT JOIN
// 							user_profile AS b ON b.user_id = a.user_id
// 						WHERE 
// 							username = ?";

// $statement = $db_handler->prepare($sql_search_username);
// $statement->execute([$username_to_find]);
// $found_user = $statement->fetch(PDO::FETCH_OBJ); // fetch found data and convert it into object PDO::FETCH_OBJ

// // print_r($found_user);

// $err_message = "Incorrect credentials";

// if($found_user){
// 	// if username is available
// 	$found_password = $found_user->password;
// 	$isPasswordValid = PasswordHash::check_password($found_password, $password_to_compare);
// 	if($isPasswordValid){

// 		$user_array_value = [
// 			'user_id' 		=> $found_user->user_id,
// 			'username' 		=> $found_user->username,
// 			'user_type' 	=> $found_user->user_type,
// 			'firstname' 	=> $found_user->first_name,
// 			'lastname' 		=> $found_user->last_name,
// 			'middlename' 	=> $found_user->middle_name,
// 			'address' 		=> $found_user->address,
// 			'contact' 		=> $found_user->contact
// 		];

// 		Session::put('user',$user_array_value);
		
// 		if($found_user->user_type == 'owner'){
// 			// if the user type is owner redirect to owner page

			

// 			header("Location: ../owner.php");
// 		}else{
// 			// else redirect to index page
// 			header("Location: ../index.php");
// 		}
		
// 	}else{
// 		header("Location: ../login.php?message=".$err_message);
// 	}
// }
// else{
// 	// if username is not available
// 	header("Location: ../login.php?message=".$err_message);
// }
