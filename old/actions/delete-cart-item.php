<?php 


spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});


$id = Request::get("id");


if($id == false){
		
	$message = "msg=Error encountered";
	Redirect::to('checkout',$message);

}else{
	
	$x = Cart::delete($id);
	Redirect::to('checkout');

}