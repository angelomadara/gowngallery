<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');
	$title = Request::get('_title');
	$message = Request::get('_message');
	$participants = Request::get('_participants');
	$expiration = Request::get('_expiration');
	$feed_id = Request::get('id');

	$x = Query::update('feed',[
		'title' => $title,
		'message' => $message,
		'max_participants' => $participants,
		'expiration' => $expiration,
	],'feed_id',$feed_id);

	if($x){
		echo json_encode([
			'status' => true,
			'swal' => [
				'title' => '',
				'text' 	=> 'Post successfully updated',
				'type'	=> 'success',
			]
		]);
	}else{
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text' 	=> 'Error encountered while updating post',
				'type'	=> 'error',
			]
		]);
	}

}