<?php 

session_start();
require_once '../functions/defines.php';
require_once '../functions/functions.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$file = Request::file('file');
	$prod_id = Request::get('prod_id');

	$status = 1;
	$length = 50;
    $new_name = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).date('Ymd');

	$file_name = basename($file["name"]);
	$ext_name = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
	$new_name = $new_name.'.'.$ext_name;
	// Check if image file is a actual image or fake image		
    $checker = getimagesize($file["tmp_name"]); // array
    $msg = "";

    if($checker === false) {
        $msg .= "File is not an image. ";
	    $status = 0;
    }

	// Check file size
	if ($file["size"] > 500000) {
	    $msg .= "Image file is too large. ";
	    $status = 0;
	}

	// Allow certain file formats
	if($ext_name != "jpg" && $ext_name != "png" && $ext_name != "jpeg" && $ext_name != "gif" ) {
	    $msg .= "Only JPG, JPEG, PNG & GIF files are allowed. ";
	    $status = 0;
	}

	if($status == 0){
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text' 	=> $msg,
				'type'	=> 'error',
			]
		]);
	}else{

		if(move_uploaded_file($file["tmp_name"],'../images/_temp/'.$new_name)){
		// if(move_uploaded_file($file["tmp_name"],'../class/'.$new_name)){

			$x = Query::update('product',[
				'image' => $new_name,
			],'product_id',$prod_id);
			// $x = smart_resize_image('../images/_temp/'.$new_name, 200, 300, true);
			// $file = '../images/_temp/'.$new_name;
			// smart_resize_image($file , null, 400 , 600 , false , '../images/big/'.$new_name , false , false ,100 );
			// smart_resize_image($file , null, 200 , 300 , false , '../images/small/'.$new_name , false , false ,100 );
			
			// resize photo
			// include_once '../images/_temp/asdf.php';
			$resize = new Resize('../images/_temp/'.$new_name);
			// Json::pretty($resize);
			// exit();
			$resize->resizeImage(200, 300, 'crop');
			$resize->saveImage('../images/small/'.$new_name,100);
			// $resize = new Resize('../images/_temp/'.$new_name);
			$resize->resizeImage(400, 600, 'crop');
			$resize->saveImage('../images/big/'.$new_name,100);
			// delete temporary photo
			unlink('../images/_temp/'.$new_name);

			echo json_encode([
				'status' => true,
				'swal' => [
					'title' => '',
					'text' 	=> 'Product picture changed',
					'type'	=> 'success',
				]
			]);
		}else{
			echo json_encode([
				'status' => false,
				'swal' => [
					'title' => '',
					'text' 	=> 'Error encountered while uploading the photo',
					'type'	=> 'error',
				]
			]);
		}

	}

	// Json::pretty($_FILES);

}