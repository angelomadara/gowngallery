<?php 
@session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	@require_once "../class/".$class.".php";
});
require_once '../plugins/PHPmailer/sendMail.php';
// echo 'register.php';
// exit();

$type 		= Request::get("type");
$shopname 	= Request::get('shopname');
$fname 		= Request::get('fname');
$mname 		= Request::get('mname');
$lname 		= Request::get('lname');
$address 	= Request::get('address');
$contact 	= Request::get('contact');
$username 	= Request::get('username');
$password1 	= Request::get('password1');
$password2 	= Request::get('password2');
$lat		= Request::get('lat') ? Request::get('lat') : 14.676842590385638;
$lng		= Request::get('lng') ? Request::get('lng') : 120.54445457458496;
$permit 	= Request::file('permit'); // permit

$length = 50;
$new_name = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).date('Ymd');
$status = 1;
// Json::print($checker);
// exit();
// VALIDATIONS
$msg = "&shop=".$shopname."&f=".$fname."&l=".$lname."&m=".$mname."&a=".$address."&c=".$contact."&u=".$username;
if($fname == "" || $lname == "" || $mname == "" || $address == "" || $username == ""){
	$msg = "All fields are important. ".$msg;
}
// check if the username already exist 
$found_user = Register::check_username($username);
if($found_user){
	$msg = "Sorry username already existed. ".$msg;
	if($type == 'customer'){  		header('Location:../register-customer.php?msg='.$msg); }
	elseif($type == 'shop-owner'){  header('Location:../register-shopowner.php?msg='.$msg); }
	else{							header('Location:../register.php');}
	exit();
}
// check if the password is eight character long
if(strlen($password1) < 8){
	$msg = "Password must be 8 characters long and an Alpha-Numeric. ".$msg;
	if($type == 'customer'){  		header('Location:../register-customer.php?msg='.$msg); }
	elseif($type == 'shop-owner'){  header('Location:../register-shopowner.php?msg='.$msg); }
	else{							header('Location:../register.php');}
	exit();
}
// check the email is valid
if(!filter_var($username, FILTER_VALIDATE_EMAIL)){
	$msg = "Invalid email addess. ".$msg;
	if($type == 'customer'){  		header('Location:../register-customer.php?msg='.$msg); }
	elseif($type == 'shop-owner'){  header('Location:../register-shopowner.php?msg='.$msg); }
	else{							header('Location:../register.php');}
	exit();
}
// check if the password is similar 
if($password1 !== $password2){
	$msg = "Password didn't matched. ".$msg;
	// echo $type;
	if($type == 'customer'){  		
		header("Location:../register-customer.php?msg=".$msg);
		// Redirect::to('register-customer.php?msg='.$msg); 
	}
	elseif($type == 'shop-owner'){  
		// Redirect::to('register-shopowner.php?msg='.$msg); }
		header("Location:../register-shopowner.php?msg=".$msg);
	}
	else{							
		header("Location:../register.php?msg");
		// Redirect::to('register.php');
	}
// echo 'a';
// exit();
	
}else{// SAVE
	$user_type = "";
	if($type == 'customer'){
		$user_type = "customer";
		// registration for customer
		$last_id = Query::create('user',[
			'username' => $username,
			'password' => PasswordHash::hash(Request::get('password1')),
			'user_type' => 'customer',
			'created_at' => date('Y-m-d H:i:s'),
			'code' => $new_name
		]);
		$id = $last_id->lastInsertId();
		Query::create('user_profile',[
			'user_id' => $id,
			'first_name' => $fname,
			'last_name' => $lname,
			'middle_name' => $mname,
			'address' => $address,
			'contact' => $contact,
		]);
	}
	elseif($type == 'shop-owner'){		
		$user_type = "owner";

		// permit settings
		// $file_name = basename($permit["name"]);
		// $ext_name = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
		// $new_name = $new_name.'.'.$ext_name;

		// if ($permit["size"] > 500000) {
		//     $msg = "Image file is too large. ".$msg;
		//     $status = 0;
		// }
		// if($ext_name != "jpg" && $ext_name != "png" && $ext_name != "jpeg" && $ext_name != "gif" && $ext_name != 'pdf') {
		//     $msg = "Only JPG, JPEG, PNG, GIF, and PDF files are allowed. ".$msg;
		//     $status = 0;
		// }
		// if($status == 0){
		// 	Redirect::to('register-shopowner.php?msg='.$msg);
		// 	return 0;
		// }

		// registration for shop owner
		$last_id = Query::create('user',[
			'username' => $username,
			'password' => PasswordHash::hash(Request::get('password1')),
			'user_type' => 'owner',
			'created_at' => date('Y-m-d H:i:s'),
			'code' => $new_name
		]);

		$id = $last_id->lastInsertId();
		Query::create('user_profile',[
			'user_id' => $id,
			'first_name' => $fname,
			'last_name' => $lname,
			'middle_name' => $mname,
			'address' => $address,
			'contact' => $contact,
		]);
		Query::create('store',[
			'store_name' => $shopname,
			'user_id' => $id,
			// 'permit' => $new_name,
			'lat' => $lat,
			'lng' => $lng
		]);
		// move_uploaded_file($permit["tmp_name"],'../images/permits/'.$new_name);
		// Redirect::to('owner.php?welcome=You successfully registered! Welcome to Balanga Gown Gallery.');
	}

	$e = "<table
		border='0' 
		cellpadding='0' 
		cellspacing='0'
		style='font-family:Arial,Helvetica,sans-serif;border-collapse:collapse;margin:0;margin-left:auto;margin-right:auto;padding:0;width:100%!important;max-width:720px!important;line-height:100%!important'>"
		."<tbody>"
			."<tr>"
				."<td>"
					."<img src='http://balangagowngallery.com/images/banner.jpg' style='width:100%;'>"
				."</td>"
			."</tr>"
			."<tr>"
				."<td style='padding-top:1em;'>"
					."<h2>Dear ".ucwords($fname)."</h2>"
				."</td>"
			."</tr>"
			."<tr>"
				."<td>
					<p>Welcome to BALANGA GOWN GALLERY the first ecommerce website that showcase all beautiful gowns and tuxedos made by first class designers.</p>
					<p>To continue and activate your account click this link <a href='http://balangagowngallery.com/activate.php?code=".$new_name."'>ACTIVATE</a>. </p>
				 </td>"
			."</tr>"
			."<tr>"
				."<td style='padding-top:3em;'>
					<p>Thank you,</p>
					<p>Bataan gown gallery team</p>
				 </td>"
			."</tr>"
		."</tbody>"
	."</table>";

	sendMail($username,$e);

	// if the user successfully registered 
	// login him automatically
	// Session::put('user',[
	// 	'user_id' 		=> $id,
	// 	'username' 		=> $username,
	// 	'user_type' 	=> $user_type,
	// 	'firstname' 	=> $fname,
	// 	'lastname' 		=> $lname,
	// 	'middlename' 	=> $mname,
	// 	'address' 		=> $address,
	// 	'contact' 		=> $contact
	// ]);

	// Redirect::to('index.php?welcome=You successfully registered! Welcome to Balanga Gown Gallery.');
	header('Location:../index.php?welcome=Account successfully created. Check your email to confirm your registration.');

}