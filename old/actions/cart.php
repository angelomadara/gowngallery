<?php 
session_start();
include '../class/Session.php';
include '../class/Request.php';
include '../class/Cart.php';
include '../class/CartCookie.php';
include '../class/Product.php';
// spl_autoload(function($class){
// 	require_once '../class/'.$class.'.php';
// 	echo $class;
// });



$login_user = Session::get('user');
$cookie = CartCookie::get();
$prod_id = Request::get('prod_id');
$qty = 1;
$product_qty = 0;
$log_id = null;

if($login_user){
	$log_id = $login_user['user_id'];
}

// filterings
$Product = new Product();
$found_product = $Product->get_product_by_id($prod_id);
// if($found_product){
	// $product_qty = $found_product->qty;
// }

$foundItemOnCart = Cart::get_by_cookie_and_prod_id([
	$cookie,
	$prod_id
]);

if($found_product->qty < $foundItemOnCart->qty){
	echo json_encode([
		'status'=>false,
		'message' => "Item not avialable for now."
	]);
	return 0;
}

// saving and updating cart
if($foundItemOnCart == false){

	$a = Cart::add([
		$cookie,
		$prod_id,
		$log_id,
		$qty
	]);

	echo json_encode([
		'status' => $a // will return true | false
	]);

}else{

	// echo ++$foundItemOnCart->qty;

	$b = Cart::update_qty([
		++$foundItemOnCart->qty,
		$cookie,
		$prod_id
	]);

	echo json_encode([
		'status' => $b // will return true | false
	]);

}







// print_r(Cart::get([ CartCookie::get() ]));