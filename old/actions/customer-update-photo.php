<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

if(!Session::isLogin('user')){
	return 0;
}else{
	$onlineUser = Session::get('user');

	$file = Request::file('file');
	$status = 1;
	$length = 50;
    $new_name = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).date('Ymd');

	$file_name = basename($file["name"]);
	$ext_name = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
	$new_name = $new_name.'.'.$ext_name;
	// Check if image file is a actual image or fake image		
    $checker = getimagesize($file["tmp_name"]); // array
    $msg = "";

    if($checker === false) {
        $msg .= "File is not an image. ";
	    $status = 0;
    }

	// Check file size
	if ($file["size"] > 500000) {
	    $msg .= "Image file is too large. ";
	    $status = 0;
	}

	// Allow certain file formats
	if($ext_name != "jpg" && $ext_name != "png" && $ext_name != "jpeg" && $ext_name != "gif" ) {
	    $msg .= "Only JPG, JPEG, PNG & GIF files are allowed. ";
	    $status = 0;
	}

	if($status == 0){
		echo json_encode([
			'status' => false,
			'swal' => [
				'title' => '',
				'text' 	=> $msg,
				'type'	=> 'error',
			]
		]);
	}else{

		if(move_uploaded_file($file["tmp_name"],'../images/_temp/'.$new_name)){

			$x = Query::update('user_profile',[
				'picture' => $new_name,
			],'user_id',$onlineUser['user_id']);

			$resize = new Resize('../images/_temp/'.$new_name);
			$resize->resizeImage(400, 400, 'auto');
			$resize->saveImage('../images/profile/'.$new_name,100);
			// delete temporary photo
			unlink('../images/_temp/'.$new_name);
			echo json_encode([
				'status' => true,
				'swal' => [
					'title' => '',
					'text' 	=> 'Profile picture changed',
					'type'	=> 'success',
				]
			]);
		}else{
			echo json_encode([
				'status' => false,
				'swal' => [
					'title' => '',
					'text' 	=> 'Error encountered while uploading the photo',
					'type'	=> 'error',
				]
			]);
		}

	}
}