<?php 

session_start();
require_once '../functions/defines.php';
spl_autoload_register(function($class){
	require_once "../class/".$class.".php";
});

$user = Session::get('user');
// Json::print($user);
$user_id = $user['user_id'];
$item_id = Request::get('item_id');
$size_id = Request::get('size');

$pack_id = Request::get('pack_id');

$cookie  = CartCookie::get('_cartCookie');

if($item_id){
	$x = Query::create("cart",[
		'cookie' => $cookie,
		'product_id' => $item_id,
		'size_id' => $size_id,
		'user_id' => $user_id,
		'qty' => 1,
		"created_at" => _date_time,
	    "updated_at" => _date_time,
	]);
	Redirect::to('info.php?prod_id='.$item_id);
}else{
	Query::create("package_order",[
		'pack_id' => $pack_id,
		'user_id' => $user_id,
		'cookie' => $cookie,
		'status' => 'open'
	]);
	Redirect::to('store-package.php?id='.$pack_id);
}


// Json::print($x);
