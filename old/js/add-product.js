$('#product_image').change(function(e){

	var output = document.getElementById('preview');
    output.src = URL.createObjectURL(e.target.files[0]);

	// if (this.files && this.files[0]) {
	//     var reader = new FileReader();	    
	//     reader.onload = function (e) {
	//         $('#preview').attr('src', e.target.result);
	//     }	    
	//     reader.readAsDataURL(this.files[0]);
	// }
});


$('#update-item').click(function(e){
	e.preventDefault();
	var form = $('#update-item-form');

	$.post(form.attr('action'),form.serialize(),function(data){
		var data = JSON.parse(data);
		swal(data.swal);
	});
});

$('#update-item-photo').click(function(e){
	var id = $(this).data('id');
	BootstrapDialog.show({
        size: BootstrapDialog.SIZE_SMALL,
        title: "Photo",
        message: function(dialog) {
            var $message = $('<div id="ksdjkien"></div>');
            var pageToLoad = dialog.getData('pageToLoad');
            $message.load(pageToLoad);        
            return $message;
        },
        data: {
            'pageToLoad': 'forms/owner-change-item-photo.php'
        },
        buttons: [{
            label: 'Cancel',
            action: function(dialog){
                dialog.close();
            }
        }, {
            label: 'Update',
            cssClass: 'btn-primary',
            action: function(){
                // var form = $('#post-reply-form').serialize();
                var fd = new FormData();
                fd.append('file',$("#file-update")[0].files[0]);
                fd.append('prod_id',id);
                $.ajax({
                    type: 'post',
                    url : 'actions/owner-update-product-img.php',
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType: 'json',
                    success: function(html){
                        swal(html.swal,function(x){
                            if(x){
                                location.reload();
                            }
                        });
                    }
                });
            }
        }],
    });

});

$(document).on('change','#file-update',function(e){
	var output = document.getElementById('img-preview');
    output.src = URL.createObjectURL(e.target.files[0]);
});