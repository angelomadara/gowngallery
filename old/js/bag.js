(function(){
	
	$('.prod_qty').click(function(){
		qty(this);
	}).keyup(function(){
		qty(this);
	});

	$('.remove-item').click(function(e){
		e.preventDefault();
		var id = $(this).data('id');
		$.post('ajax/remove-item.php',{id:id},function(data){
			var data = JSON.parse(data);
			swal(data.swal,function(isOkay){if(isOkay) location.reload(); });
		});
	});

	$('.remove-package').click(function(e){
		e.preventDefault();
		var id = $(this).data('id');
		$.post('ajax/remove-bag-package.php',{id:id},function(data){
			var data = JSON.parse(data);
			swal(data.swal,function(isOkay){if(isOkay) location.reload(); });
		});
	});

	function qty(e){
		var value = $(e).val()
		var maxVal = $(e).attr('max');
		var id = $(e).attr('data-id');
		var price = $(e).attr('data-price');
		console.log(maxVal	);
		if(value <= 0){
			$(e).siblings('.err').html("<i style='color:red'>Quantity exceed or lower than zero</i>");
		}else{
			$(e).siblings('.err').html("");
			$.get("ajax/update-bag.php",{id:id,value:value,price:price},function(data){
				// console.log(data);
				// $(e).parent('td').find('.item_tot > span').text(data.total);
				var data = JSON.parse(data);
				$('#sub-total > span').html(data.total);
			});
		}
	}

}());