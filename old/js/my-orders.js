(function(){

	$(".approve-btn").click(function(){
		var id = $(this).parent('div').data('id');
		var contact = $(this).data('contact');
		note({
			label: "Create your approval note to your customer.",
			id: id,
			mobile: contact,
			type: "approve",
		});
	});

	// $(".refuse-btn").click(function(){
	// 	var id = $(this).parent('div').data('id');
	// 	var contact = $(this).data('contact');
	// 	note({
	// 		label: "Create your refusal note to your customer.",
	// 		id: id,
	// 		type: "refuse",
	// 	});
	// });

	$(".cancel-btn").click(function(){
		var id = $(this).parent('div').data('id');
		var contact = $(this).data('contact');
		note({
			label: "Create your returned note to your customer.",
			id: id,
			mobile: contact,
			type: "cancel",
		});
	});


	$(".returned-btn").click(function(){
		var id = $(this).parent('div').data('id');
		var contact = $(this).data('contact');
		note({
			label: "Create your returned note to your customer.",
			id: id,
			mobile: contact,
			type: "return",
		});
	});

	function note(obj){
		var textConfirm = "Your item/request has been approved please see our shop";
		var textCancel = "Your item/request has been forfeit. Thank you.";
		var textReturn = "Thank you for trusting, please rate our service. Thank you.";
		BootstrapDialog.show({
			title: "Note",
	        data: {'pageToLoad': 'forms/order-note.html'},
	        message: function(dialog) {
	            return $('<form id="form"></form>').load(dialog.getData('pageToLoad'));
	        },
	        onshown: function(dialog){
	        	if(obj.type == 'approve') $('textarea#compose').text(textConfirm);
	        	if(obj.type == 'cancel') $('textarea#compose').text(textCancel);
	        	if(obj.type == 'return') $('textarea#compose').text(textReturn);
	        	// console.log('text');
	        	$('#div-note').text(obj.label);
	        	$('#order-id').val(obj.id);
	        	$('#mobile').val(obj.mobile);
	        },	
	        buttons: [{
	            label: 'Cancel',
	            action: function(dialog){
	            	dialog.close();
	            }
	        }, {
	            label: 'Send',
	            cssClass: 'btn-primary',
	            id: 'eto',
	            action: function(dialog){
	            	var $button = this;
                    $button.disable();
                    $button.spin();
                    dialog.setClosable(false);
	                var form = $('#form').serialize();
	                $.post('ajax/post-order.php?control='+obj.type,form,function(data){
	                	var data = JSON.parse(data);
                		swal(data.swal,function(isOKay){
                			if(isOKay){
                				if(data.status == true) location.reload();
                			}
                			// $button.enable();
			                // $button.spin();
			                // dialog.setClosable(true);
                		});
	                });
	            }
	        }],
	    });
	}

}());