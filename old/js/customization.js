var accordionsMenu = $('.cd-accordion-menu');
var miliSec = 400;
if( accordionsMenu.length > 0 ) {
	
	accordionsMenu.each(function(){
		var accordion = $(this);
		//detect change in the input[type="checkbox"] value
		accordion.on('change', 'input[type="checkbox"]', function(){
			var checkbox = $(this);
			// console.log(checkbox.prop('checked'));
			( checkbox.prop('checked') ) ? checkbox.siblings('ul').attr('style', 'display:none;').slideDown(miliSec) : checkbox.siblings('ul').attr('style', 'display:block;').slideUp(miliSec);
		});
	});
}

$('#female-tab').click(function(e){
	femaleTab();
});

$('#male-tab').click(function(e){
	maleTab();
});

var fTopPrice = $('#female-div td.top');
var fMidPrice = $('#female-div td.mid');
var fBotPrice = $('#female-div td.bot');
var mTopPrice = $('#male-div td.top');
// var mMidPrice = $('#male-div td.mid');
var mBotPrice = $('#male-div td.bot');
var partLocator = '';

$('.part').click(function(e){
	e.preventDefault();
	var part = $(this).data('part'); // button
	var removePart = "<span clas='glyphicon glyphicon-remove'></span>";
	var id = $(this).data('id');	
	var img = $(this).children('img'); // image to clone
	var fTotal=0,mTotal=0;
	// var fTopPriceValue=0;
	// var fMidPriceValue=0;
	// var fBotPriceValue=0;
	var itemPrice = $(this).find('span').text();
	switch(part) {
	    case 'female-top':
	    	femaleTab();// clear element
	    	// cost estimation
			fTopPrice.html(itemPrice);
			fTotal = toFloat(fTopPrice.text()) + toFloat(fMidPrice.text()) + toFloat(fBotPrice.text());
			$('#f-top-input').val(id);
			$("#female-div td.total").text(moneyFormat(fTotal.toFixed(2)));
			// new element to append
	        $(".female-top").html($(img).clone().css({
	        	'display':'block',
	        	'margin':'auto',
	        	'width':'100%'
	        })).append('<span class="glyphicon glyphicon-remove remove-item" data-loc="f-top">');
	    break;
	    case 'female-belt':
	    	femaleTab();// clear element
	    	// cost estimation
			fMidPrice.html(itemPrice);
			fTotal = toFloat(fTopPrice.text()) + toFloat(fMidPrice.text()) + toFloat(fBotPrice.text());
			$('#f-mid-input').val(id);
			$("#female-div td.total").text(moneyFormat(fTotal.toFixed(2)));
			// new element to append
	        $(".female-belt").html($(img).clone().css({
	        	'display':'block',
	        	'margin':'auto',
	        	'width':'100%'
	        })).append('<span class="glyphicon glyphicon-remove remove-item" data-loc="f-mid">');
	    break;
	    case 'female-bottom':
	    	femaleTab(); // clear element
	    	// cost estimation
			fBotPrice.html(itemPrice);
			fTotal = toFloat(fTopPrice.text()) + toFloat(fMidPrice.text()) + toFloat(fBotPrice.text());
			$('#f-bot-input').val(id);
			$("#female-div td.total").text(moneyFormat(fTotal.toFixed(2)));
			// new element to append
			$("#f-price").text(fTotal);	    	
	        $(".female-bottom").html($(img).clone().css({
	        	'display':'block',
	        	'margin':'auto',
	        	'width':'100%'
	        })).append('<span class="glyphicon glyphicon-remove remove-item" data-loc="f-bot">');
	    break;
	    case 'male-top':
	    	maleTab();
			mTopPrice.html(itemPrice);
			mTotal = toFloat(mTopPrice.text()) + toFloat(mBotPrice.text());
			$('#m-top-input').val(id);
			$("#male-div td.total").text(moneyFormat(mTotal.toFixed(2)));
			// 
			$("#m-price").text(mTotal);
	        $(".male-top").html($(img).clone().css({
	        	'display':'block',
	        	'margin':'auto',
	        	'width':'100%'
	        })).append('<span class="glyphicon glyphicon-remove remove-item" data-loc="m-top">');
	    break;
	    case 'male-bottom':
	    	maleTab();
			mBotPrice.html(itemPrice);
			mTotal = toFloat(mTopPrice.text()) + toFloat(mBotPrice.text());
			$('#m-bot-input').val(id);
			$("#male-div td.total").text(moneyFormat(mTotal.toFixed(2)));
			// mTotal=mTopPrice+mBeltPrice+mBotPrice;
			$("#m-price").text(mTotal);	    	
	        $(".male-bottom").html($(img).clone().css({
	        	'display':'block',
	        	'margin':'auto',
	        	'width':'100%'
	        })).append('<i class="glyphicon glyphicon-remove remove-item" data-loc="m-bot">');
	    break;
	}

	$(document).find('.remove-item').css({
		'position':'absolute',
		'top':'0px','right':'0px',
		'font-size':'1.5em',
		'color': '#e74c3c',
		'cursor':'pointer'
	});

});

$(document).on('click','.send-request-btn',function(e){
	e.preventDefault();
	var form = $(this).parent('div').parent('form');
	var url = 'ajax/request-customization.php?type=';
	switch(form.attr('id')){
		case 'f-form':
			swal({
				title: '',
				text : 'Send customization request?',
				type : 'info',
				showCancelButton: true,
				confirmButtonColor: "#3498db",
				confirmButtonText: "Yes, send my request",
				closeOnConfirm: false
			},function(isOkay){
				if(isOkay){
					$.post(url+'female',form.serialize(),function(data){
						var data = JSON.parse(data);
						swal(data.swal,function(isOkay){
							if(isOkay){
								if(data.status == true){
									location.href='my-customization.php';
								}
							}
						});
					});
				}
			});
		break;
		default:
			swal({
				title: '',
				text : 'Send customization request?',
				type : 'info',
				showCancelButton: true,
				confirmButtonColor: "#3498db",
				confirmButtonText: "Yes, send my request",
				closeOnConfirm: false
			},function(isOkay){
				if(isOkay){
					$.post(url+'male',form.serialize(),function(data){
						var data = JSON.parse(data);
						swal(data.swal,function(isOkay){
							if(isOkay){
								if(data.status == true){
									location.href='my-customization.php';
								}
							}
						});
					});
				}
			});
	}
});

$(document).on('click','.remove-item',function(){
	$(this).siblings('img').remove();
	$(this).remove();
	var location = $(this).data('loc');
	switch(location){
		case "f-top":
			fTopPrice.html('0.00');
			fTopPriceValue = toFloat(fTopPrice.text()) + toFloat(fMidPrice.text()) + toFloat(fBotPrice.text());
			$("#female-div td.total").text(moneyFormat(fTopPriceValue.toFixed(2)));
			$('#f-top-input').val("");
		break;
		case "f-mid":
			fMidPrice.html('0.00');
			fTopPriceValue = toFloat(fTopPrice.text()) + toFloat(fMidPrice.text()) + toFloat(fBotPrice.text());
			$("#female-div td.total").text(moneyFormat(fTopPriceValue.toFixed(2)));
			$('#f-mid-input').val("");
		break;
		case "f-bot":
			fBotPrice.html('0.00');
			fTopPriceValue = toFloat(fTopPrice.text()) + toFloat(fMidPrice.text()) + toFloat(fBotPrice.text());
			$("#female-div td.total").text(moneyFormat(fTopPriceValue.toFixed(2)));
			$('#f-bot-input').val("");
		break;
		case "m-top":
			mTopPrice.html('0.00');
			mTotal = toFloat(mTopPrice.text()) + toFloat(mBotPrice.text());
			$("#male-div td.total").text(moneyFormat(mTotal.toFixed(2)));
			$('#m-top-input').val("");
		break;
		case "m-bot":
			mBotPrice.html('0.00');
			mTotal = toFloat(mTopPrice.text()) + toFloat(mBotPrice.text());
			$("#male-div td.total").text(moneyFormat(mTotal.toFixed(2)));
			$('#m-bot-input').val("");
		break;
	}
});


function maleTab(){	
	$('#male-tab').addClass('active');
	$('#female-tab').removeClass('active');
	$('#female-div').addClass('hidden');
	$('#male-div').removeClass('hidden');
}

function femaleTab(){
	$('#female-tab').addClass('active');
	$('#male-tab').removeClass('active');
	$('#female-div').removeClass('hidden');
	$('#male-div').addClass('hidden');
}