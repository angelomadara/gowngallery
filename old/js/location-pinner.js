$('#us2').locationpicker({
	location: {latitude: 14.679416506896382, longitude: 120.54106426239014},
	radius: 300,
	inputBinding: {
		latitudeInput: $('#us2-lat'),
		longitudeInput: $('#us2-lon'),
		radiusInput: $('#us2-radius'),
		locationNameInput: $('#us2-address')
	}
});