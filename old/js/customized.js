(function(){

	$('#part-file').change(function(e){
		var output = document.getElementById('preview');
	    output.src = URL.createObjectURL(e.target.files[0]);
	});

	$('#add-part').click(function(e){
		e.preventDefault();
		swal({
			title: '',
			text : 'Upload cloth part?',
			type : 'info',
			showCancelButton: true,
			confirmButtonColor: "#3498db",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
			closeOnConfirm: false
		},function(isOkay){
			if(isOkay){
				var fd = new FormData();
                fd.append('file',$("#part-file")[0].files[0]);
                fd.append('name',$("#part-name").val());
                fd.append('type',$("#part-type").val());
                fd.append('prce',$("#part-price").val());
                fd.append('id',$('#id').val());
				$.ajax({
                    type: 'post',
                    url : $('#upload-part').attr('action'),
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType: 'json',
                    success: function(html){
                        swal(html.swal,function(x){
                            	
                            if(html.status == true) location.href="owner-clothings.php";
                            
                        });
                    }
                });
			}
		});
	});

	// $('.update-part').click(function(e){
	// 	e.preventDefault();
	// 	var id = $(this).data('id');

	// })

	$('#confirm-request').click(function(e){
		var id = $(this).data('id');
		var number = $(this).parent('div').data('number');
		
		BootstrapDialog.show({
			title: "Dress part",
	        data: {'pageToLoad': 'forms/custom-request-confirm.html'},
	        message: function(dialog) {
	            return $('<form id="form"></form>').load(dialog.getData('pageToLoad'));
	        },
	        onshown: function(dialog){
	        	$('#form').append("<input type='hidden' name='id' value='"+id+"'>");
        		$('#form').append("<input type='hidden' name='number' value='"+number+"'>");
	        },	
	        buttons: [{
	            label: 'Cancel',
	            action: function(dialog){
	            	dialog.close();
	            }
	        }, {
	            label: 'Send',
	            cssClass: 'btn-primary',
	            action: function(){
	                var form = $('#form').serialize();
	                $.post('ajax/accept-customized.php',form,function(data){
	                	var data = JSON.parse(data);
                		swal(data.swal,function(isOKay){
                			if(isOKay){
                				if(data.status == true) location.reload();
                			}
                		});
	                });
	            }
	        }],
	    });
	});

	$('#close-request').click(function(e){
		var id = $(this).data('id');
		var number = $(this).parent('div').data('number');
		swal({
			title: '',
			text : 'Close the deal?',
			type : 'info',
			showCancelButton: true,
			confirmButtonColor: "#3498db",
			confirmButtonText: "Yes, close this deal",
			closeOnConfirm: false
		},function(isOkay){
			if(isOkay){
				$.post('ajax/close-customized.php',{id:id},function(data){
					var data = JSON.parse(data);
            		swal(data.swal);
				});
			}
		});
	});

	$('#cancel-request').click(function(e){
		var id = $(this).data('id');
		var number = $(this).parent('div').data('number');
		BootstrapDialog.show({
			title: "Dress part",
	        data: {'pageToLoad': 'forms/custom-request-cancel.html'},
	        message: function(dialog) {
	            return $('<form id="form"></form>').load(dialog.getData('pageToLoad'));
	        },
	        onshown: function(dialog){
	        	$('#form').append("<input type='hidden' name='id' value='"+id+"'>");
        		$('#form').append("<input type='hidden' name='number' value='"+number+"'>");
	        },	
	        buttons: [{
	            label: 'Cancel',
	            action: function(dialog){
	            	dialog.close();
	            }
	        }, {
	            label: 'Send',
	            cssClass: 'btn-primary',
	            action: function(){
	                var form = $('#form').serialize();
	                $.post('ajax/cancel-customized.php',form,function(data){
	                	var data = JSON.parse(data);
                		swal(data.swal,function(isOKay){
                			if(isOKay){
                				if(data.status == true) location.reload();
                			}
                		});
	                });
	            }
	        }],
	    });
	});

}());