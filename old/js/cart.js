
(function(){
	return 0;
	$("#cart").on("click", function(event) {
		event.preventDefault();
		// $(".shopping-cart").fadeToggle( "fast");
		$('#cart-information').toggleClass('hidden','show');
	});

 	$('.leave-message').click(function(e){
 		e.preventDefault();
 		var id = $(this).attr('data-id');
 		alert('not yet working')
 	});	

  	$('#rent-item').click(function(){

	  	var id = $(this).attr('data-id');
	  	var elm = "";	  
	  	elm += "<p>Add this item to cart ?</p>";
	  	// console.log(id)
	  	// return 0;
	  	BootstrapDialog.show({
	        title: 'Confirmation',
	        message: elm,
	        buttons: [
	        	{
	        		label: 'Cancel',
	        		action: function(closeDialog){
	        			closeDialog.close();
	        		}
	        	},
	        	{
	        		label: 'Add item',
	        		cssClass: 'btn-warning',
	        		action: function(closeDialog){
	        			// ajax request GET
	        			$.get('actions/cart.php',{'prod_id':id},function(response){
	        				// console.log(response);
	        				var response = JSON.parse(response);
	        				// console.log(response)
	        				if(response.status == true){	        					
			        			update_cart();
	        				}else{
	        					alert("Error updating your item:\n"+response.message);
	        				}
	        			});

	        			closeDialog.close();
	        		}
	        	}
	        ]
	    });
  
  	});

  	$('.remove-cart-item').click(function(e){
  		e.preventDefault();
  		var url = $(this).attr('href');

  		BootstrapDialog.show({
	        title: 'Confirmation',
	        message: "Are you sure do want to remove this item.",
	        buttons: [
	        	{
	        		label: 'Cancel',
	        		action: function(closeDialog){
	        			closeDialog.close();
	        		}
	        	},
	        	{
	        		label: 'Remove',
	        		cssClass: 'btn-warning',
	        		action: function(closeDialog){
	        			location.href = url;
	        			closeDialog.close();
	        		}
	        	}
	        ]
	    });
  	});

  	// get cart items
  	update_cart();
	function update_cart(){
		var cookie = $('html').attr('data-cookie');
		// alert(cookie);
		$.get('actions/get-cart-items.php',{'ref':cookie},function(response){
			// JSON.parse = convert string to json object
			var cart_obj = JSON.parse(response);
			var element = "";
			// console.log(cart_obj);
			
			$.each(cart_obj.cart, function(id,value){
				element += "<li>";
					element += "<img src='images/small/"+value.image+"' style='width:20px;'>";
					element += "<span class='item-name'> "+value.product_name+" </span>";
					element += "<span class='item-price'> "+value.price+" </span>";
					element += "<span class='item-quantity'> (x"+value.qty+") </span>";
				element += "</li>";
			});

			$(".shopping-cart-items").html(element);
			$("#cart_count").html(cart_obj.count);
			$('#cart-price').html("Php "+cart_obj.total_price);
		});
	}
	// end of cart items
  
})();