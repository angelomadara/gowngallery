var lati = $('#us2-lat');
var long = $('#us2-lon');
var radius = $('#us2-radius');
var address = $('#us2-address');

// alert(lati.val())

$('#us2').locationpicker({
	location: {latitude: lati.val(), longitude: long.val()},
	radius: 300,
	inputBinding: {
		latitudeInput: lati,
		longitudeInput: long,
		radiusInput: radius,
		locationNameInput: address
	},
	onchanged: function(currentLocation, radius, isMarkerDropped) {
	    // alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
	    $.get('actions/change-location.php',{
	    	'lati' : currentLocation.latitude,
	    	'long' : currentLocation.longitude
	    },function(data){
	    	// console.log(data);
	    	swal({
	    		title : "Location changed",
	    		text : "",
	    		type : "success"
	    	});
	    });
	}
});

$('.reply-comment').click(function(){
	var id = $(this).attr('data-id');
	var user = $('#u').attr('data-id');
    // alert(user);
    // return 0;
	BootstrapDialog.show({
		title: "Reply",
        message: function(dialog) {
            var $message = $('<form id="post-reply-form"></form>');
            var pageToLoad = dialog.getData('pageToLoad');
            $message.load(pageToLoad);        
            return $message;
        },
        data: {
            'pageToLoad': 'forms/reply-form.html'
        },
        onshown: function(dialog){
        	$("input[name='feed-id']").val(id);
			$("input[name='user-id']").val(user);
        },	
        buttons: [{
            label: 'Cancel',
            action: function(dialog){
            	dialog.close();
            }
        }, {
            label: 'Reply',
            cssClass: 'btn-primary',
            action: function(){
                var form = $('#post-reply-form').serialize();
                $.get('ajax/post-reply.php',form,function(data){
                	var data = JSON.parse(data);
                	if(data.status == false){
                		swal(data.swal);
                	}else{
                		location.reload();
                	}
                });
            }
        }],
    });
});

$('#btn-reply').click(function(e){
    e.preventDefault();
    var form = $('#form-post-reply').serialize();
    $.post('ajax/post-reply-owner.php',form,function(data){
        var data = JSON.parse(data);
        // console.log(data);
        swal(data.swal);
        loadConversation({
            feed_id:$("input[name='feed-id']").val(),
            customer:$("input[name='customer-id']").val()
        });
    });
});

$('#btn-refesh').on('click',function(e){
    e.preventDefault();
    loadConversation({
        feed_id:$("input[name='feed-id']").val(),
        customer:$("input[name='customer-id']").val()
    });
});

// SHOP MANAGEMENT
$('#btn-update-store').click(function(e){
    e.preventDefault();
    var form = $('#update-my-store');
    var action = form.attr('action');
    $.post(action,form.serialize(),function(data){
        var data = JSON.parse(data);
        swal(data.swal);
    });
});

$('#btn-update-password').click(function(e){
    e.preventDefault();
    $.post('ajax/update-password.php',
        $("#update-passwod-form").serialize()
    ,function(data){
        var data = JSON.parse(data);
        if(data.status == true){
            $("#update-passwod-form :input").val('');
        }
        swal(data.swal);
    });
});

loadConversation({
    feed_id:$("input[name='feed-id']").val(),
    customer:$("input[name='customer-id']").val()
});
function loadConversation(obj){    
    loading('#load-conversation');    
    $.get('ajax/conversation-owner.php',obj,function(data){
        var data = JSON.parse(data);
        $(document).find('#load-conversation').html(data.feed);
    });
    $("textarea[name='reply']").val("");
}

