<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
];
require_once "includes/header.php";
if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
// Json::pretty($owner);
// $newFeeds = count(Query::fetchAll("SELECT feed_id FROM feed WHERE expiration > ?",[_date_time]));
// $newOffers = 0;
// $newOrders = count(Query::fetchAll("SELECT * FROM product_order WHERE status = ?",['pending']));
$parts = Query::fetchAll("SELECT * FROM dress_parts");
$store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]);
// Json::pretty($store);
$items = Query::fetchAll("SELECT * FROM customization WHERE store_id = ? ",[$store->store_id]);
// Json::pretty($items);
// exit();
?>

<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>

    </div>
	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative active">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
                <!-- <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>
            <ul class="breadcrumb">
                <li><a href='owner.php'>Dashboard</a></li>
                <li class="active">Customized Offers</li>
            </ul>
            <div class="clearfix">
                <!-- <a href="owner-customization-page.php">Customization</a> -->
            </div>
		</div>
	</div>

    <div class="row">       
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <ul id="product-link">
                <li><a href='my-products.php'>+ Dresses / Tuxedos</a> </li>
                <li>
                    <a href='owner-clothings.php' class="active">+ Custom Parts</a> 
                    <ul>
                        <li><a href='owner-upload-clothings.php'>- add part</a></li>
                    </ul>
                </li>
                <li><a href='my-packages.php'>+Packages</a> </li>
            </ul>
        </div> 
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 asdf-product-holder">
            <h4>Parts</h4>
            <table class="table table-bordered table-hovered table-striped datatable">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th style="width:20%">Image</th>
                        <th>Type </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($items):foreach($items as $item): ?>
                        <tr>
                            <td><?=$item->custom_id?></td>
                            <td>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <br>
                                        <img src="<?=_image_url.'parts/'.$item->image?>" alt="" style='width:100%'>
                                    <br>
                                </div>
                            </td>   
                            <td>
                                <p>Part title: <?=title_case(str_replace('-',' ',$item->type))?> </p>
                                <p>Part name: <?=$item->name?> </p>
                                <p>Part estimated price: PHP <?=number_format($item->price,2)?></p>
                                <div>
                                    <a class="btn btn-success" href='owner-update-clothings.php?id=<?=$item->custom_id?>' data-id="<?=$item->custom_id?>">Update item</a>
                                    <!-- <button class="btn btn-success" data-id="<?=$item->custom_id?>">Remove</button> -->
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; endif; ?>
                </tbody>
            </table>
        </div>  
    </div>

</div>

<?php 
	$scripts = ['customized.js','notification.js'];
	require_once "includes/footer.php";
?>