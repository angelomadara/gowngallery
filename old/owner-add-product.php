<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}

$owner = Session::get('user');
if(Request::get('msg')){
    echo "<script> 
            swal({
                title:'',text:'".Request::get('msg')."',type:'".Request::get('info')."'
            },function(xxx){
                if(xxx){location.href = 'owner-add-product.php';}
            }); 
        </script>";
} 
?>
    
<div class="container" style="min-height: 400px;">
    <!-- <div class="well"> -->

        <div class="row" style="background: #fff">
            <div class="col-lg-12">
                <h1 class="champagne store-name">
                    <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                    <?php echo $store ? $store->store_name : null ; ?>
                </h1>
                <!-- <hr> -->
            </div>
        </div>

        <div class="row" id="owner-dashboard" style="background: #fff">                       
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                    <li role="presentation" class="relative">
                        <a href="owner.php"><b>Dashboard</b></a>
                    </li>
                    <li role="presentation" id="feed-tab" class="relative">
                        <a href="owner-feed.php"><b>Feeds</b></a>
                    </li>
                    <li role="presentation" id="custom-tab" class="relative">
                        <a href="owner-customization-page.php"><b>Customization</b></a>
                    </li>
                    <li role="presentation" id='order-tab' class="relative">
                        <a href="my-orders.php"><b>Requests</b></a>
                    </li>
                    <li role="presentation" class="relative active">
                        <a href="my-products.php"><b>Gallery</b></a>
                    </li>
                    <!-- <li role="presentation" class="relative">
                        <a href="my-packages.php"><b>Packages</b></a>
                    </li> -->
                    <li role="presentation" class="relative">
                        <a href="my-shop.php"><b>My Shop</b></a>
                    </li>
                </ul>
                <ul class="breadcrumb">
                    <li><a href='my-products.php'>My Products</a></li>
                    <li class="active">Add Product</li>
                </ul>
                <!-- <div class="col-lg-12">
                    <h3> <i class="glyphicon glyphicon-tags"></i> My products</h3>
                </div> -->
            </div>
        </div>
    
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <ul id="product-link">
                    <li><a href='my-products.php' class="active">+ Dresses / Tuxedos</a>
                        <ul>
                            <li><a href='owner-upload-clothings.php'>- add product</a></li>
                        </ul> 
                    </li>
                    <li>
                        <a href='owner-clothings.php'>+ Custom Parts</a> 
                    </li>
                    <li><a href='my-packages.php'>+ Packages</a> </li>
                </ul>
            </div> 
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 asdf-product-holder">
                <div class="col-lg-6">
                    <form class="form" method="POST" action="actions/owner-add-item.php" enctype="multipart/form-data">

                        <!-- <input type="hidden" name="insert_post" value="test"/> -->
                        <div class="">  
                            <table class="table" id="add-product-table">

                                <tr>
                                    <td align=""><b>Product Title:</b></td>
                                    <td><input type="text" name="product_title" size="80" class="form-control" required value="<?=Request::get('prod')?>" ></td>
                                </tr>
                                
                                <!-- <tr>
                                    <td align=""><b>Quantity:</b></td>
                                    <td><input type="text" name="product_quantity" size="20" class="form-control" required ></td>
                                </tr> -->
                                
                                <tr >
                                    <td align=""><b>Size:</b></td>
                                    <td>
                                        <select name="product_size[]" class="form-control select2" required multiple>
                                            <option disabled="" value=""></option>
                                            <?php 
                                                $sizes = Query::fetchAll("SELECT * FROM size");
                                                if($sizes){
                                                    foreach ($sizes as $key => $value) {
                                                        echo "<option value='".$value->size."'>".$value->size."</option>";
                                                    }
                                                }
                                            ?>                  
                                        </select>           
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align=""><b>Category:</b></td>
                                    <td>
                                        <select name="product_cat"  class="form-control selectpicker" required >
                                            <option value="" disabled=""> Select a category </option>
                                            <?php 
                                                $categories = Query::fetchAll("SELECT * FROM category");
                                                if($categories){
                                                    foreach ($categories as $key => $category) {
                                                        echo "<option value='".$category->cat_id."'>".$category->cat_name."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>           
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align=""><b>Type:</b></td>
                                    <td><select name="product_type" class="form-control selectpicker" required >
                                        <option disabled="" value=""> Select a type </option>
                                        <?php 
                                            $types = Query::fetchAll("SELECT * FROM type");
                                            if($types){
                                                foreach ($types as $key => $type) {
                                                    echo "<option value='".$type->type_id."'>".$type->type_name."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align=""><b>Image:</b></td>
                                    <td><input type="file" name="product_image" id="product_image" class="form-control" required ></td>
                                </tr>
                                
                                <tr>
                                    <td align=""><b>Price:</b></td>
                                    <td><input type="number" name="product_price" class="form-control no-spin" required  value="<?=Request::get('price')?>"></td>
                                </tr>
                                
                                <tr >
                                    <td align=""><b>Description:</b></td>
                                    <td><textarea class="form-control" name="product_desc" value="<?=Request::get('desc')?>" style="max-width: 100%; min-width: 100%;width: 100%;height: 100px"></textarea></td>
                                </tr>
                                
                                <tr >
                                    <td align=""><b>Keywords:</b></td>
                                    <td><input type="text" name="product_keywords" class="form-control" required  value="<?=Request::get('key')?>"></td>
                                </tr>
                                    
                                
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" class="btn btn-success" value="Add Item" style="float:right ;"> 
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </form>
                </div>
                
                <div class="col-md-6 col-md-6 col-sm-6 col-xs-6">
                    <img id="preview" src="images/fashion.png" alt="Item image" style="width:100%;">
                </div>
            </div>
        </div>

    <!-- </div> -->
</div>


<?php 
    $scripts = ['add-product.js','notification.js'];
	include 'includes/footer.php';
 ?>