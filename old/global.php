<?php 

// store ratings
function storeRatings($params){
    $ratings = Query::fetchAll("SELECT * FROM store_rating WHERE store_id = ?",$params);
    $onestar = $twostar = $threestar = $fourstar = $fivestar = 0;
    if($ratings):
        $total = count($ratings);
        foreach ($ratings as $rating) {
            $onestar = $onestar + $rating->one;
            $twostar = $twostar + $rating->two;
            $threestar = $threestar + $rating->three;
            $fourstar = $fourstar + $rating->four;
            $fivestar = $fivestar + $rating->five;
        }
        $onestar    = 100 - ((($total - $onestar)/$total)*100);
        $twostar    = 100 - ((($total - $twostar)/$total)*100);
        $threestar  = 100 - ((($total - $threestar)/$total)*100);
        $fourstar   = 100 - ((($total - $fourstar)/$total)*100);
        $fivestar   = 100 - ((($total - $fivestar)/$total)*100);
    endif;
    echo '<div style=""> ';
        echo '<div>Store ratings</div>';
        echo '<div class="clearfix">';
            echo '<div class="pull-left" style="width:10%;">5 <i class="glyphicon glyphicon-star-empty" style="color: #2980b9;"></i></div>';
            echo '<div class="pull-left" style="width:90%;"><span class="store-rating" style="width:'.$fivestar.'%"></span></div>';
        echo '</div>';
        echo '<div>';
            echo '<div class="pull-left" style="width:10%;">4 <i class="glyphicon glyphicon-star-empty" style="color: #2980b9;"></i></div>';
            echo '<div class="pull-left" style="width:90%;"><span class="store-rating" style="width:'.$fourstar.'%"></span></div>';
        echo '</div>';
        echo '<div>';
            echo '<div class="pull-left" style="width:10%;">3 <i class="glyphicon glyphicon-star-empty" style="color: #2980b9;"></i></div>';
            echo '<div class="pull-left" style="width:90%;"><span class="store-rating" style="width:'.$threestar.'%"></span></div>';
        echo '</div>';
        echo '<div>';
            echo '<div class="pull-left" style="width:10%;">2 <i class="glyphicon glyphicon-star-empty" style="color: #2980b9;"></i></div>';
            echo '<div class="pull-left" style="width:90%;"><span class="store-rating" style="width:'.$twostar.'%"></span></div>';
        echo '</div>';
        echo '<div>';
            echo '<div class="pull-left" style="width:10%;">1 <i class="glyphicon glyphicon-star-empty" style="color: #2980b9;"></i></div>';
            echo '<div class="pull-left" style="width:90%;"><span class="store-rating" style="width:'.$onestar.'%"></span></div>';
        echo '</div>';
    echo '</div>';
}

function storeRating($params){
    $ratings = Query::fetchAll("SELECT * FROM store_rating WHERE store_id = ?",$params);
    $onestar = $twostar = $threestar = $fourstar = $fivestar = 0;
    if($ratings):
        $total = count($ratings);
        foreach ($ratings as $rating) {
            $onestar = $onestar + $rating->one;
            $twostar = $twostar + $rating->two;
            $threestar = $threestar + $rating->three;
            $fourstar = $fourstar + $rating->four;
            $fivestar = $fivestar + $rating->five;
        }
        $onestar    = ($onestar/$total)*100;
        $twostar    = ($twostar/$total)*100;
        $threestar  = ($threestar/$total)*100;
        $fourstar   = ($fourstar/$total)*100;
        $fivestar   = ($fivestar/$total)*100;

        $average = round(($onestar + $twostar + $threestar + $fourstar + $fivestar)/$total,2);

        // $str = "<span>";
        //     $str .= "".$average."%";
        // $str .= "</span>";
        // echo "<div>";
        //     echo "<span class='pull-left' style='width:".."%;background:red;height:16px;'></span>";
        // echo "</div>";
        return $average;
    endif;
}
