<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
];
require_once "includes/header.php";
if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
$id = Request::get('id');
// $parts = Query::fetchAll("SELECT * FROM dress_parts");
$store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]);

$part = Query::fetch("SELECT * FROM customization WHERE custom_id = ?",[$id]);

?>

<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>

    </div>
	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Orders</b></a>
                </li>
                <li role="presentation" class="relative active">
                    <a href="my-products.php"><b>Products</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>
            <ul class="breadcrumb">
                <li><a href='owner.php'>Dashboard</a></li>
                <li class="active">Customized Offers</li>
            </ul>
            <div class="clearfix">
                <!-- <a href="owner-customization-page.php">Customization</a> -->
            </div>
		</div>
	</div>

    <div class="row">       
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <ul id="product-link">
                <li><a href='my-products.php'>Dresses / Tuxedos</a> </li>
                <li>
                    <a href='owner-clothings.php' class="active">Customization Parts</a> 
                    <ul>
                        <li><a href='owner-upload-clothings.php'>+ add part</a></li>
                    </ul>
                </li>
                <li><a href='#'>Packages</a> </li>
            </ul>
        </div> 
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 asdf-product-holder">
            <h4>Update Part</h4>
            <?php if($part): ?>
            <form id="upload-part" action="ajax/upload-cloth-part.php" class="clearfix">
                <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <div class="input-wrapper">
                        <label for="">Part Name</label>
                        <input type="text" name="part-name" id="part-name" class="form-control" value="<?php echo $part->name ?>">
                    </div>
                    <div class="input-wrapper">
                        <label for="">Part Type</label>
                        <select name="part-type" id="part-type" class="form-control selectpicker">
                            <option id="" selected disabled></option>
                            <option value="female-top">Female Top</option>
                            <option value="female-belt">Female Belt</option>
                            <option value="female-bottom">Female Bottom</option>
                            <option value="male-top">Male Top</option>
                            <option value="male-bottom">Male Bottom</option>
                        </select>
                    </div>
                    <div class="input-wrapper">
                        <label for="">Part Price</label>
                        <input type="number" name="part-price" id="part-price" class="form-control no-spin" value="<?php echo $part->price ?>">
                    </div>
                    <div class="input-wrapper">
                        <label for="">Part Photo</label>
                        <input type="file" name="part-file" id="part-file" class="form-control no-spin">
                    </div>
                    <div class="input-wrapper">
                        <button type="submit" class="btn btn-success" id="add-part">Add Part</button>
                    </div>
                </div>
                <div class="col-md-7 col-md-7 col-sm-7 col-xs-7">
                    <img id="preview" src="images/parts/<?php echo $part->image ?>" alt="Item image" style="width:100%;">
                </div>
            </form>
            <?php endif; ?>
        </div>  
    </div>

</div>

<?php 
	$scripts = ['customized.js','notification.js'];
	require_once "includes/footer.php";
?>