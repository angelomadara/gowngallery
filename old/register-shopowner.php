<?php 
	$css = [];
	include 'includes/header.php';
	
?>

<div class="container">
	<!-- <div class="well"> -->
	<form method="POST" action="actions/signup.php?type=shop-owner" enctype="multipart/form-data" class="row" style="background: #fff;">    		
		<div class="col-md-4 col-md-offset-1">
			<br>
			<!-- <form class="form" method="POST" action="actions/signup.php?type=shop-owner"> -->
				
				<?php 
					if(Request::get('msg')){
						// echo "<p class='alert alert-info'>". Request::get('msg') ."</p>";	
						echo "<script> swal('','".Request::get('msg')."','info') </script>";
					}    					
				?>
				<!-- <h3>Shop Information</h3> -->
				<!-- <div id=""> -->
					<!-- <label for="fname">Shop Name <span class="asterisk">*</span> </label> -->
					<!-- <input type="hidden" name="shopname" id="shopname" class="form-control" value="<?=Request::get('shop')?>"> -->
				<!-- </div> -->
				<!-- <hr> -->
				<h3>Personal Information</h3>
				<div>
					<label for="fname">First Name <span class="asterisk">*</span> </label>
					<input type="text" name="fname" id="fname" class="form-control" value="<?=Request::get('f')?>">
				</div>
				<div>
					<label for="mname">Middle Name <span class="asterisk">*</span> </label>
					<input type="text" name="mname" id="mname" class="form-control" value="<?=Request::get('m')?>">
				</div>
				<div>
					<label for="lname">Last Name <span class="asterisk">*</span> </label>
					<input type="text" name="lname" id="lname" class="form-control" value="<?=Request::get('l')?>">
				</div>
				<div>
					<label for="address">Address <span class="asterisk">*</span> </label>
					<input type="text" name="address" id="address" class="form-control" value="<?=Request::get('a')?>">
				</div>
				<div>
					<label for="contact">Contact No. <span class="asterisk">*</span> </label>
					<input type="text" name="contact" id="contact" class="form-control no-spin mask-mobile" placeholder="0910 123 4567" value="<?=Request::get('c')?>">
				</div>
				<hr>
				<h3>Login Information</h3>
				<div>
					<label for="username">Email <span class="asterisk">*</span> </label>
					<input type="email" name="username" id="username" class="form-control" value="<?=Request::get('u')?>">
				</div>
				<div>
					<label for="password1">Password <span class="asterisk">*</span> </label>
					<input type="password" name="password1" id="password1" class="form-control">
				</div>
				<div>
					<label for="password2">Re-password <span class="asterisk">*</span> </label>
					<input type="password" name="password2" id="password2" class="form-control">
				</div><hr>
				<!-- <h3>Business Permit</h3> -->
				<!-- <div> -->
					<!-- <label for="permit">Permit <span class="asterisk">*</span> </label> -->
					<!-- <input type="file" name="permit" id="permit" class="form-control" value=""> -->
				<!-- </div> -->
				
				<div class="clearfix" style="margin-top:1em;">
					<input type="submit" class="btn btn-success pull-right" value="Register">
					<a href="register.php" class="btn btn-default pull-right" style="margin-right:1em;">Cancel</a>
				</div>
			<!-- </form> -->

		</div>
		<!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<h3>Pin location</h3>
			<div>
				<label for="">Location: </label>
				<input type="text" id="us2-address" class="form-control"/>
			</div>			
			<div id="us2" style="width: 100%; height: 400px;"></div>
			<input type="hidden" name="lat" id="us2-lat" />
			<input type="hidden" name="lng" id="us2-lon"/>
			<input type="hidden" id="us2-radius"/>
			
		</div> -->
    </form>
    <!-- </div> -->
</div>

<?php 
	$scripts = ['location-pinner.js'];
	include 'includes/footer.php';
 ?>