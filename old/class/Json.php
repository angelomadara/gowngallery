<?php 

/**
* 
*/
class Json
{
	public static function encode($params = []){
		if(count($params)){
			echo json_encode($params);
		}
		return false;
	}

	public static function decode($params = []){
		if(count($params)){
			echo json_decode($params);
		}
		return false;
	}

	public static function pretty($params=[]){
		if(count($params)){
			echo "<pre>";
			echo json_encode($params,JSON_PRETTY_PRINT);
			echo "</pre>";
		}
		return false;
	}

	public static function order($params = []){
		if(count($params)){
			echo "<pre>";
			echo print_r($params);
			echo "</pre>";
		}
		return false;
	}
}