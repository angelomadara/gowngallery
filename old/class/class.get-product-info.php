<?php 


    $id = Request::get('prod_id');

	require_once 'Database.php';

	global $db_handler;


	$sql = "SELECT 
				p.product_name, p.description AS p_desc, p.size, p.price, p.image, p.qty AS p_qty,
				s.store_name, s.description AS s_desc,
				up.first_name, up.last_name, up.address, up.contact,
				c.cat_name, t.type_name
			FROM product AS p
			LEFT JOIN store AS s ON s.user_id = p.user_id
			LEFT JOIN user_profile AS up ON up.user_id = p.user_id
			LEFT JOIN type AS t ON t.type_id = p.type_id
			LEFT JOIN category AS c ON c.cat_id = p.cat_id
			WHERE p.product_id = ?";

	$params = [$id];

	$statement = $db_handler->prepare($sql);
	$statement->execute($params);
	$found_product = $statement->fetch(PDO::FETCH_OBJ);

	// echo "<pre>";
	// print_r($found_product);