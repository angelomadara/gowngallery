<?php 

/**
* 
*/
class CartCookie
{

	public static $alphabets = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

	public static function create(){
		$cookie_reference = self::$alphabets[rand(0,51)].
							self::$alphabets[rand(0,51)].
							self::$alphabets[rand(0,51)].
							self::$alphabets[rand(0,51)].
							self::$alphabets[rand(0,51)].
							self::$alphabets[rand(0,51)].
							self::$alphabets[rand(0,51)].
							self::$alphabets[rand(0,51)].
							date('ymdhis');
		if(!isset($_COOKIE['_cartCookie']) || $_COOKIE['_cartCookie'] == '' ){
			setcookie('_cartCookie',$cookie_reference,time()+86400,'/');
		}
		if(!isset($_COOKIE['_customization']) || $_COOKIE['_customization'] == '' ){
			setcookie('_customization',$cookie_reference,time()+86400,'/');
		}
	}

	public static function refresh()
	{
		setcookie('_cartCookie','',time()+86400,'/');
	}

	public static function get(){
		if(isset($_COOKIE['_cartCookie'])){
			return $_COOKIE['_cartCookie'];
		}
		return false;
	}

	public static function refreshCustom()
	{
		setcookie('_customization','',time()+86400,'/');
	}

	public static function getCustom(){
		if(isset($_COOKIE['_customization'])){
			return $_COOKIE['_customization'];
		}
		return false;
	}
}

// new CartCookie;