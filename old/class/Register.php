<?php 

/**
* 
*/
class Register extends DB
{

	public static function check_username($username){

		$handler = DB::ready()->_pdo;

		$sql_cart = "SELECT * FROM user WHERE username = ?";

		$statement = $handler->prepare($sql_cart);
		$statement->execute([
			$username
		]); 
		return $statement->fetch(PDO::FETCH_OBJ);
	}

}