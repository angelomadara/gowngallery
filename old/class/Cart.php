<?php 
require_once 'Database.php';
/**
* 
*/
class Cart
{
	public static function add($params){
		global $db_handler;

		$sql_cart = "INSERT INTO cart(cookie,product_id,user_id,qty) VALUES(?,?,?,?)";

		$statement = $db_handler->prepare($sql_cart);
		$statement->execute($params); 
		if($statement->rowCount() > 0) return true;
		else return false;
	}

	public static function update_qty($params){
		global $db_handler;

		$sql_cart = "UPDATE cart SET qty = ? WHERE cookie = ? AND product_id = ?";

		$statement = $db_handler->prepare($sql_cart);
		$statement->execute($params); 
		if($statement->rowCount() > 0) return true;
		else return false;
	}

	public static function get($params = []){
		global $db_handler;

		$sql_cart = "
			SELECT
				c.cookie, c.qty, c.cart_id,
				p.product_name, p.price, p.image, p.size,
				up.first_name, up.last_name, up.middle_name, up.contact, up.address
			FROM cart AS c 
			LEFT JOIN product AS p ON p.product_id = c.product_id
			LEFT JOIN user_profile AS up ON up.user_id = c.user_id
			WHERE cookie = ? AND deleted_at IS NULL
		";
		// IS NOT NULL = record sa database na hindi NULL
		// kung gusto mo kunin lahat ng NULL value sa table gamitin mo lang IS NULL
		$statement = $db_handler->prepare($sql_cart);
		$statement->execute($params);

		return $statement->fetchAll(PDO::FETCH_OBJ);

	}

	public static function delete($id){
		global $db_handler;
		$date = date('Y-m-d H:i:s');
		$sql_cart = "UPDATE cart SET deleted_at = ? WHERE cart_id = ?";
		$statement = $db_handler->prepare($sql_cart);
		return $statement->execute([$date,$id]);
		if($statement->rowCount() > 0) return true;
		else return false;
	}

	public static function get_by_cookie_and_prod_id($params){
		global $db_handler;
		$sql_cart = "SELECT * FROM cart WHERE cookie = ? AND product_id = ?";
		$statement = $db_handler->prepare($sql_cart);
		$statement->execute($params);
		return $statement->fetch(PDO::FETCH_OBJ);
	}
}
