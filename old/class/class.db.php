<?php 
try {
	$handler = new PDO('mysql:host=127.0.0.1;dbname=rentals','root','');
	$handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// session_start();
} catch(PDOException $e){
	echo $e->getMessage();
	die();
}
