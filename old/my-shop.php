<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}

$owner = Session::get('user');
$newFeeds = count(Query::fetchAll("SELECT feed_id FROM feed WHERE expiration > ?",[_date_time]));
$newOffers = 0;
$newOrders = count(Query::fetchAll("SELECT * FROM product_order WHERE status = ?",['pending']));
// print_r($owner);
$store = Query::fetch("SELECT 
                            s.store_name, s.description, s.permit, s.lat, s.lng,
                            up.first_name, up.last_name, up.middle_name,
                            up.address, up.contact, u.username
                        FROM store AS s
                        LEFT JOIN user_profile AS up ON up.user_id = s.user_id
                        LEFT JOIN user AS u ON u.user_id = s.user_id
                        WHERE up.user_id = ?",[$owner['user_id']]);
 // Json::print($store); 
?>
    
<div class="container" style="min-height: 400px;">

    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>

        
    </div>
    <div class="row" id="owner-dashboard" style="background: #fff">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
                <!-- <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative active">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li class="active">Manage my account</li>
                </ul>
            </div>
           <div class="col-lg-12">
                <h3> <i class="glyphicon glyphicon-home"></i> My Account</h3>
           </div>
        </div>
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form method="post" action='actions/update-store-information.php' id="update-my-store" class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                <h3>Shop Information</h3>
                <div id="">
                    <label for="fname">Shop Name <span class="asterisk">*</span> </label>
                    <input type="text" name="shopname" id="shopname" class="form-control" value="<?=$store?$store->store_name:null?>">
                </div>
                <hr>
                <h3>Personal Information</h3>
                <div>
                    <label for="fname">First Name <span class="asterisk">*</span> </label>
                    <input type="text" name="fname" id="fname" class="form-control" value="<?=$store?$store->first_name:null?>">
                </div>
                <div>
                    <label for="mname">Middle Name <span class="asterisk">*</span> </label>
                    <input type="text" name="mname" id="mname" class="form-control" value="<?=$store?$store->middle_name:null?>">
                </div>
                <div>
                    <label for="lname">Last Name <span class="asterisk">*</span> </label>
                    <input type="text" name="lname" id="lname" class="form-control" value="<?=$store?$store->last_name:null?>">
                </div>
                <div>
                    <label for="address">Address <span class="asterisk">*</span> </label>
                    <input type="text" name="address" id="address" class="form-control" value="<?=$store?$store->address:null?>">
                </div>
                <div>
                    <label for="contact">Contact No. <span class="asterisk">*</span> </label>
                    <input type="text" name="contact" id="contact" class="form-control no-spin mask-mobile" placeholder="0910 123 4567" value="<?=$store?$store->contact:null?>">
                </div>
                <div class="clearfix" style="margin-top:1em;">
                    <input type="submit" id="btn-update-store" class="btn btn-success pull-right" value="Update">
                    <!-- <a href="javascript:history.go(-1)" class="btn btn-default pull-right" style="margin-right:1em;">Cancel</a> -->
                </div>
            </form>

            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                <div>
                    <label for="">Update Location: </label>
                    <input type="text" id="us2-address" class="form-control"/>
                </div>          
                <div id="us2" style="width: 100%; height: 400px; margin-top: 50px;"></div>
                <?php 
                    $defLat = 14.676842590385638;
                    $defLng = 120.54445457458496;
                ?>
                <input type="hidden" value="<?=$store->lat?$store->lat:$defLat?>" name="lat" id="us2-lat" />
                <input type="hidden" value="<?=$store->lng?$store->lng:$defLng?>" name="lng" id="us2-lon"/>
                <input type="hidden" id="us2-radius"/>
            </div>
        </div>      
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3em">            
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                <form action="ajax/update-password.php" id="update-passwod-form">
                    <h3>Login Information</h3>
                    <div>
                        <label for="username">Old password <span class="asterisk">*</span> </label>
                        <input type="password" name="old-password" id="old-password" class="form-control">
                    </div>
                    <div>
                        <label for="password1">Password <span class="asterisk">*</span> </label>
                        <input type="password" name="password1" id="password1" class="form-control">
                    </div>
                    <div>
                        <label for="password2">Re-password <span class="asterisk">*</span> </label>
                        <input type="password" name="password2" id="password2" class="form-control">
                    </div>
                </form>
                <div class="clearfix" style="margin-top:1em;">
                    <input type="submit" id="btn-update-password" class="btn btn-success pull-right" value="Update">
                </div>
            </div>
            
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                <form action="actions/update-permit.php" method="post" enctype="multipart/form-data" class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <h3>Update Business Permit</h3>
                    <div>
                        <label for="permit">Permit <span class="asterisk">*</span> </label>
                        <input type="file" name="permit" id="permit" class="form-control" value="">
                    </div>
                    <div class="clearfix" style="margin-top:1em;">
                        <input type="submit" id="btn-update-store" class="btn btn-success pull-right" value="Update">
                    </div>
                </form>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    <img src="<?=$store?_image_url.'permits/'.$store->permit:null?>" alt="Permit" style='width:100%'>
                </div>
            </div>
        </div>

    </div>

</div>


<?php 
    $scripts = ['my-shop.js','notification.js'];
	include 'includes/footer.php';
?>