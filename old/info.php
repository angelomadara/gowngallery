<?php 
    $css = [
        'index.css'
    ]; 
   
include 'includes/header.php';

$id = Request::get('prod_id');
$found_product = Query::fetch("SELECT 
                    p.product_name, p.description AS p_desc, p.price, p.image,
                    s.store_name, s.description AS s_desc, s.store_id, 
                    up.first_name, up.last_name, up.address, up.contact,
                    c.cat_name, t.type_name, u.username, u.user_id
                FROM product AS p
                LEFT JOIN store AS s ON s.user_id = p.user_id
                LEFT JOIN user AS u ON u.user_id = p.user_id
                LEFT JOIN user_profile AS up ON up.user_id = p.user_id
                LEFT JOIN type AS t ON t.type_id = p.type_id
                LEFT JOIN category AS c ON c.cat_id = p.cat_id
                WHERE p.product_id = ?",[$id]);

$sizes = Query::fetchAll("SELECT * FROM size_and_quantity WHERE qty > ? AND isHidden != ? AND product_id = ?",[0,1,$id]);

// Json::print($found_product);
?>
    
<div class="article">
    <div class="container">             
    <?php if($found_product): ?>
    	<div class="row" style="background: #fff">    
            
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id ="content_area">                        
                    <div class="display-categories">
                        <form class='' action='actions/bag.php' method='POST' style="padding-top: 2em;">
                            <!-- <div class=''> -->
                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>
                                    <img src="<?=_image_url."big/".$found_product->image?>">
                                </div>

                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>                                
                                    <!-- <p style="font-size:2em;font-weight: 700;"> <b><?= $found_product->product_name ?></b> </p><br> -->
                                    <h3><?= title_case($found_product->product_name) ?></h3><br>
                                    <p>Category: <a href='search.php?ballgown=on'><?=$found_product->cat_name?></a></p>
                                    <p>Type: <a href='search.php?sweetheart=on'><?=$found_product->type_name?></a></p>
                                    <p> Price: Php <?=number_format($found_product->price,2)?> </p>
                                    <p style="text-align: justify;"><?=htmlspecialchars_decode($found_product->p_desc)?> </p> 
                                    <!-- <p>
                                        DELIVERED IN: <br>
                                        Greater NCR: 0-0 days, Major cities: 0-0 days, Provincial: 0-0 days. All in working days
                                    </p> -->
                                    <p>Sold by: <a href='store-name.php?id=<?=$found_product->user_id?>'><?=$found_product->store_name?></a></p>
                                    <!-- store ratings -->
                                    <?php storeRatings([$found_product->store_id]); ?>
                                    <p>Contact us at: <a href="#"> <?=$found_product->username?> </a> </p>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                                    <!-- <p> Item on stock </p> -->
                                    <div class="input-wrapper clearfix" style="">
                                        <!-- <label for="" style="font-weight: normal;float:left;margin-right: 10px;"> -->
                                        <label for="" style="font-weight: normal;">
                                            <?php if(count($sizes) > 0 ): ?>
                                                <p> Item on stock </p>
                                                <p>Only <?=count($sizes)?> size<?php if(count($sizes)!=1){echo "s";}?> available</p>
                                            <?php else: ?>
                                                <p>Item not available</p>
                                            <?php endif; ?>
                                        </label>
                                        <select name="size" id="" class="form-control input-sm" required style="width:70px;">
                                            <option value=""></option>
                                            <?php foreach($sizes as $size): ?>
                                                <option value="<?=$size->size_qty_id?>"> <?=$size->size?></option>
                                            <?php endforeach; ?>
                                        </select>

                                    </div>
                                    <input type='hidden' name='item_id' required value='<?=Request::get('prod_id')?>'>
                                    <button type='submit' id='rent-item' data-id='<?=Request::get('prod_id')?>' value='' class='' style='outline:none;'>
                                        <img src="<?=_image_url?>bag.png" alt="" style='display: inline-block; width: 50px;'>
                                        &nbsp;&nbsp;Add to bag
                                    </button>

                                    <div style="margin-top: 2em;">
                                        <h4>Body measurement details</h4>
                                        <i>Click to view full image</i>
                                        <a href="<?= _image_url.'sizechart.jpg' ?>" target="_blank">
                                            <img src="<?= _image_url.'sizechart.jpg' ?>" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3em;">
                                    <h4>Body Guide</h4>
                                    <img src="<?= _image_url.'sizechartguide.jpg' ?>" alt="">
                                </div>
                            <!-- </div> -->
                        </form>
                    </div>
                </div>
    		</div>
    	</div>
        
        <div class="row">            
            <br><br><br>
            <!-- comments -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php 
                    $comments = Query::fetchAll('SELECT 
                        pc.*,CONCAT(up.first_name," ",up.last_name) AS name 
                    FROM product_comments AS pc
                    LEFT JOIN user_profile AS up ON up.user_id = pc.user_id
                    WHERE pc.product_id = ? ORDER BY product_comment_id DESC',[$id]);
                ?>
                <h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Comments (<?=count($comments)?>) </h4><br>
                <?php if($comments): ?>
                    <?php foreach($comments as $comment): ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:1em;">
                            <div> 
                                <b><?= title_case($comment->name) ?> </b>
                                <?php 
                                    $x = 1;
                                    do {   
                                        echo "<i class='glyphicon glyphicon-star'  style='color: #9b59b6;'></i> ";
                                        $x++;
                                    } while ($x <= $comment->stars);
                                ?>
                            </div>
                            <div>
                                <?= paragraph($comment->comment) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:1em;">
                        <div>No comments yet for this product</div>    
                    </div>
                <?php endif; ?>
            </div>
        </div>
    
    <?php endif; ?>
    </div>
</div>

<?php 
	include 'includes/footer.php';
 ?>