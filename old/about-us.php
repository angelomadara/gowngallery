<?php  
include_once 'includes/header.php';
?>

<div class="article">
    <div class="container"> 
    	<div class="row" style="background: #fff;padding-top:2em;">  
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h3>About Us</h3>
				<p>The Balanga Gown Gallery is a web-based system designed to be a gateway for customers 
				looking for a gown to rent and different registered rental shops around Balanga City 
				to do transactions online.</p>

				<p>The Balanga Gown Gallery and all the Registered Rental Shop in the portal follow
				the Universal Sizing Code to generalize the process of picking of size.</p>

				<p>This website offers Online Rental of Gowns and Gown Customization with Cost
				Estimation.</p>
			</div>
		</div>
	</div>
</div>

<?php include_once 'includes/footer.php'; ?>