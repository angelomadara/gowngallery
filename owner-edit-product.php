<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}

$owner = Session::get('user');
$prod_id = Request::get('item');

$product = Query::fetch("SELECT * FROM product WHERE product_id = ?",[$prod_id]);
// Json::pretty($product);
?>
    
<div class="container" style="min-height: 400px;">

        <div class="row" style="background: #fff">
            <div class="col-lg-12">
                <h1 class="champagne store-name">
                    <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                    <?php echo $store ? $store->store_name : null ; ?>
                </h1>
                <hr>
            </div>
        </div>

        <div class="row" id="owner-dashboard" style="background: #fff">       
                
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                    <li role="presentation" class="relative">
                        <a href="owner.php"><b>Dashboard</b></a>
                    </li>
                    <li role="presentation" id="feed-tab" class="relative">
                        <a href="owner-feed.php"><b>Feeds</b></a>
                    </li>
                    <li role="presentation" id="custom-tab" class="relative">
                        <a href="owner-customization-page.php"><b>Customization</b></a>
                    </li>
                    <li role="presentation" id='order-tab' class="relative">
                        <a href="my-orders.php"><b>Orders</b></a>
                    </li>
                    <li role="presentation" class="relative active">
                        <a href="my-products.php"><b>Products</b></a>
                    </li>
                    <li role="presentation" class="relative">
                        <a href="my-packages.php"><b>Packages</b></a>
                    </li>
                    <li role="presentation" class="relative">
                        <a href="my-shop.php"><b>My Shop</b></a>
                    </li>
                </ul>
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href='my-products.php'>My Products</a></li>
                        <li class="active">Edit Product</li>
                    </ul>
                </div>
                <div class="col-lg-12">
                    <h3> <i class="glyphicon glyphicon-tags"></i> My products</h3>
                </div>
            </div>

            <div class="col-lg-6">
                <?php 
                    // if(Request::get('msg')){
                    //     echo "<div>";
                    //     echo "  <p class=\"alert alert-danger\">".Request::get('msg')."</p>";    
                    //     echo "</div>";      
                    // }                   
                ?>
                <?php if($product): ?>
                <form id="update-item-form" class="form" method="POST" action="ajax/owner-update-item.php" enctype="multipart/form-data">
                    <input type="hidden" name="product_id" value="<?=Request::get('item')?>">
                    <div class="">  
                        <table class="table" id="add-product-table">

                            <tr>
                                <td align=""><b>Product Title:</b></td>
                                <td>
                                    <input type="text" name="product_title" size="80" class="form-control" required value="<?=$product->product_name?>" >
                                </td>
                            </tr>
                            
                            <!-- <tr >
                                <td align=""><b>Size:</b></td>
                                <td>
                                    <select name="product_size[]" class="form-control select2" required multiple>
                                        <option disabled="" value=""></option>
                                        <?php 
                                            $sizes = Query::fetchAll("SELECT * FROM size");
                                            if($sizes){
                                                foreach ($sizes as $key => $value) {
                                                    echo "<option value='".$value->size."'>".$value->size."</option>";
                                                }
                                            }
                                        ?>                  
                                    </select>           
                                </td>
                            </tr> -->
                            
                            <tr>
                                <td align=""><b>Category:</b></td>
                                <td>
                                    <select name="product_cat"  class="form-control selectpicker" required >
                                        <option value="" disabled=""> Select a category </option>
                                        <?php $categories = Query::fetchAll("SELECT * FROM category"); ?>
                                        <?php if($categories): ?>
                                            <?php foreach ($categories as $key => $category): ?>
                                                <?php $selected = $category->cat_id == $product->cat_id ? "selected" : ""; ?>
                                                <option value='<?=$category->cat_id?>' <?=$selected?> > <?=$category->cat_name?> </option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>           
                                </td>
                            </tr>
                            
                            <tr>
                                <td align=""><b>Type:</b></td>
                                <td><select name="product_type" class="form-control selectpicker" required >
                                    <option disabled="" value=""> Select a type </option>
                                    <?php $types = Query::fetchAll("SELECT * FROM type"); ?>
                                    <?php if($types): ?>
                                        <?php foreach ($types as $key => $type): ?>
                                            <?php $selected = $type->type_id == $product->type_id ? "selected" : ""; ?>
                                            <option value='<?=$type->type_id?>' <?=$selected?> > <?=$type->type_name?> <?=$selected?> </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    ?>
                                </select>
                                </td>
                            </tr>
                            
                            <!-- <tr>
                                <td align=""><b>Image:</b></td>
                                <td><input type="file" name="product_image" id="product_image" class="form-control" required ></td>
                            </tr> -->
                            
                            <tr>
                                <td align=""><b>Price:</b></td>
                                <td><input type="number" name="product_price" class="form-control no-spin" required  value="<?=$product->price?>"></td>
                            </tr>
                            
                            <tr >
                                <td align=""><b>Description:</b></td>
                                <td><textarea class="form-control" name="product_desc" value="" style="max-width: 100%; min-width: 100%;width: 100%;height: 100px"><?=$product->description?></textarea></td>
                            </tr>
                            
                            <tr >
                                <td align=""><b>Keywords:</b></td>
                                <td><input type="text" name="product_keywords" class="form-control" required  value="<?=$product->keywords?>"></td>
                            </tr>
                                
                            
                            <tr>
                                <td colspan="2">
                                    <input type="submit" id="update-item" class="btn btn-success" value="Update Item" style="float:right ;"> 
                                </td>
                            </tr>

                        </table>
                    </div>
                </form>
                <?php endif; ?>
            </div>
            
            <div class="col-md-6 col-md-6 col-sm-6 col-xs-6 relative">
                <div class="relative"> 
                    <span 
                        class="glyphicon glyphicon-camera" 
                        id='update-item-photo'
                        data-id='<?=Request::get("item")?>'
                        style="position: absolute; top:10px; left:10px;font-size: 2em;cursor: pointer;"
                    ></span> 
                </div>
                <img id="preview" src="images/big/<?=$product->image?>" alt="Item image" style="width:100%;">
            </div>

        </div>
    <!-- </div> -->
</div>


<?php 
    $scripts = ['add-product.js','notification.js'];
	include 'includes/footer.php';
 ?>