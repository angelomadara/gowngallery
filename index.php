<?php 
$css = [
    'index.css'
];    
include_once 'includes/header.php';
$products = Query::fetchAll("SELECT 
                                p.product_id, p.product_name, p.description, p.price, p.image,
                                s.store_name, s.user_id AS sid, s.store_id
                            FROM product AS p 
                            LEFT JOIN store AS s ON s.user_id = p.user_id
                            ORDER BY p.price ASC");

$sql_category = "SELECT * FROM category ORDER BY cat_name ASC";
$sql_type = "SELECT * FROM type ORDER BY type_name ASC";
$get_categories = Query::fetchAll($sql_category);
$get_types = Query::fetchAll($sql_type);

// json::print(Session::get('user'));
if(Request::get('welcome')){
    echo "<script> 
        swal({title:'',text:'".Request::get('welcome')."',type:'success'},function(e){if(e){location.href='index.php';}});
    </script>";
}           
?>

<div class="article">
    <div class="container"> 
    	<div class="row" style="background: #fff;padding-top:2em;">  
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <form method="get" action="search.php" class="" style="font-size:12px;">

                    <div><b>Search:</b></div>
                    <div class="form-group">
                        <input type="text" name="item" value="<?=Request::get('item')?>" class="form-control input-sm" placeholder="Search here">
                    </div>

                    <div class="form-group">
                        <p><b>Sort By:</b></p>
                        <div>
                            <input type="radio" name="sort-by" value="a-z" id="asc">
                            <label for="asc" style="font-weight: normal;">A-Z</label>
                        </div>
                        <div>
                            <input type="radio" name="sort-by" value="z-a" id="desc">
                            <label for="desc" style="font-weight: normal;">Z-A</label>
                        </div>
                        <div>
                            <input type="radio" name="price" value="low-high" id="low">
                            <label for="low" style="font-weight: normal;">Lowest to Highest Price</label>
                        </div>
                        <div>
                            <input type="radio" name="price" value="high-low" id="high">
                            <label for="high" style="font-weight: normal;">Highest to Lowest Price</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <p><b>Categories</b></p>
                        <?php $categories = Query::fetchAll($sql_category); ?>
                        <?php if($categories): ?>
                            <?php foreach($categories as $key => $value): ?>

                                <div>
                                    <input type="checkbox" name="cat-check[]" value="<?=$value->code?>" id="<?=$value->code?>">
                                    <label for="<?=$value->code?>" style='font-weight: normal;'> <?= $value->cat_name ?> </label>
                                </div>

                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <p><b>Types</b></p>
                        <?php $types = Query::fetchAll($sql_type); ?>
                        <?php if($types): ?>
                            <?php foreach($types as $key => $value): ?>

                                <div>
                                    <input type="checkbox" name="type-check[]" value="<?= $value->code ?>" id="<?=$value->code?>"  >
                                    <label for="<?=$value->code?>" style='font-weight: normal;'> <?= $value->type_name ?> </label>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    
                    <div>
                        <input type="submit" class="btn btn-success" value="Search">
                    </div>

                </form>
            </div>    	
            	
    		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <div id ="content_area">                            
                    <div class="display-categories clearfix">
                         
                    <?php if($products): ?>
                    <?php foreach ($products as $key => $value): ?>
                            <?php $sizes = Query::fetchAll("SELECT * FROM size_and_quantity WHERE product_id = ? AND isHidden = ? AND qty != ?",[$value->product_id,0,0]); ?>

                            <div class='grid col-lg-3'>
                                <div class='grid-item product-holder'> 
                                    <a href='info.php?prod_id=<?=$value->product_id?>' class='product-link'>
                                        <img src='<?= _image_url."small/".$value->image ?>' alt='<?= $value->product_name ?>'>  
                                        <div class='product-name'>                                                    
                                                <?php if($value->product_name != "" || $value->product_name != null ): ?>
                                                    <!-- if discription is available -->
                                                    <p> <?= mb_strimwidth(title_case($value->product_name), 0,23, '...') ?> </p>
                                                <?php else: ?>
                                                    <!-- if no description available -->
                                                    <p>No description available.</p>
                                                <?php endif; ?>
                                            <div class='store-name'> <a href='store-name.php?id=<?= $value->sid ?>'><?= $value->store_name ?></a> </div>
                                            <?php echo "Store rating: ".storeRating([$value->store_id])."%"; ?>
                                            <p class='product-price'><b>Php <?= number_format($value->price,2) ?> </b></p>
                                                <?php if($sizes): ?>
                                                    <span> Sizes:
                                                    <?php foreach($sizes as $x => $size): ?>
                                                        <?php echo $size->size; $x = $x + 1; ?>
                                                        <?php if($x < count($sizes)) { echo ", "; } ?>
                                                        <?php $x++; ?>
                                                    <?php endforeach; ?>
                                                    </span>
                                                <?php else: ?>
                                                    <span>Out of stock.</span>
                                                <?php endif; ?>
                                                <!-- <span class='pull-right glyphicon glyphicon-comment leave-message' data-id='{$value->product_id}'></span> -->
                                        </div>
                                    </a>
                                </div>
                            </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <div class='col-lg-3'>
                            No products found.
                        </div>
                    <?php endif; ?>    
                        
                    </div>
                </div>
            </div>

   		</div>
    </div>
</div>

<?php 
    $scripts = ['cart.js'];
	include 'includes/footer.php';
 ?>
 