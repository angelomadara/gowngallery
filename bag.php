<?php 
require_once "includes/header.php";

$user_id = Session::get('user');

$cookie = CartCookie::get('_cartCookie');

$my_bag = Query::fetchAll("
	SELECT 
		c.cart_id,
		p.product_name, p.image, p.price, c.qty AS order_qty,
		s.size, s.qty AS stock_qty, p.product_id
	FROM cart AS c
	LEFT JOIN product AS p ON p.product_id = c.product_id
	LEFT JOIN size_and_quantity AS s ON s.size_qty_id = c.size_id
	WHERE cookie = ? AND status =?
	",[$cookie,0]);

$packages = Query::fetchAll("
	SELECT *
	FROM package_order AS po
	LEFT JOIN package AS p ON p.pack_id = po.pack_id
	WHERE po.cookie = ? AND status = ?
	",[$cookie,'open']);

// json::pretty($packages);
$total = $packageTotal = 0;
// echo CartCookie::refresh();
?>

<div class="container">
	<div class="row">
			
		<form action='actions/order.php' method="post" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<br><br>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2">Items</th>
						<th style="width:250px;">Quantity / Date Needed</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					<?php if($my_bag): ?>
					<?php foreach($my_bag as $bag): ?>
						<tr> 
							<td>
								<span class="btn btn-danger btn-sm remove-item" data-id='<?= $bag->cart_id ?>'> 
									<i class="glyphicon glyphicon-remove"></i> 
								</span>
							</td>
							<td>
								<img src="<?= _image_url.'small/'.$bag->image ?>" alt="" style="width:50px;">
								<p><?=title_case($bag->product_name) . " - ". $bag->size ?></p>
							</td> 
							<td>
								<div>
									<span>Only <?= $bag->stock_qty ?> item(s) available </span><br>
									<input 
										type="number" 
										max="<?= $bag->stock_qty ?>" 
										min="1" 
										class="prod_qty form-control input-sm" 
										data-id="<?= $bag->cart_id ?>" 
										data-price="<?= $bag->price ?>" 
										value="<?= $bag->order_qty ?>" 
										style='width:70px;'>
									<span class='err'></span>
								</div>
								<br>
								<div>
									<div>Date</div>
									<input type="text" name="date-needed[]" class="form-control input-sm datepicker">
								</div>
							</td>
							<td class='item_tot'>
								Php <span><?=number_format($bag->price,2)?></span>
							</td> 
						</tr>
						<?php $total = $total + ($bag->price * $bag->order_qty); ?>
					<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan='3'>Total</td>
						<td id='sub-total'>Php <span><?= number_format($total,2) ?></span></td>
					</tr>
				</tfoot>
			</table>

			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2">Packages</th>
						<th style="width:250px;">Date Needed</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					<?php if($packages): foreach($packages as $package): ?>
						<tr>
							<td>
								<span class="btn btn-danger btn-sm remove-package" data-id='<?= $package->pack_order_id ?>'> 
									<i class="glyphicon glyphicon-remove"></i> 
								</span>
							</td>
							<td> 
								<div><img src="<?php echo _image_url.'package/'.$package->image; ?>" style='width:60px;'></div>
								<div><?php echo $package->name ?></div> 
							</td>
							<td>
								<div class="input-wrapper">
									<label for="">Date</label>
									<input type="text" name="pack-date-needed[]" class="form-control datepicker input-sm">
								</div>
							</td>
							<td>
								<div>PHP <?php 
									echo number_format($package->price,2); 
									$packageTotal = $packageTotal + $package->price;
								?></div>
							</td>
						</tr>
					<?php endforeach;endif; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan='3'>Total</td>
						<td id='sub-total'>Php <span><?= number_format($packageTotal,2) ?></span></td>
					</tr>
				</tfoot>
			</table>
			<!-- <div class="input-wrapper">
				<label for="">Date needed</label>
				<input type="text" name="date-needed" id="" class="form-control datepicker">
			</div> -->
			<?php 
				if(Request::get('msg')){
					echo "<div class='alert alert-info'>".Request::get('msg')."</div>";
				}
				if(Request::get('success')){
					echo "<div class='alert alert-success'>".Request::get('success')."</div>";
				}
				if(Request::get('error')){
					echo "<div class='alert alert-danger'>".Request::get('error')."</div>";
				}
			?>
			<input type="submit" class="btn btn-success pull-right" value="Confirm">
		</form>
		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<br><br>
			<div id="ordering-information">
				<h4>How to rent?</h4>
				<ol>
					<li>Choose a Gown.</li>
					<li>Pick your size base from the Universal Sizing posted below.</li>
					<li>Add your chosen gown to your "Bag".</li>
					<li>To rent, send a request your chosen gown to the rental shop.</li>
					<li>Wait a confirmation message if your rented item is granted or not through SMS.</li>
					<li>ATTENTION: Please allocate one to two days before the event for your fitting.</li>
				</ol>
			</div>
		</div>

	</div>
</div>

<?php 
$scripts = ['bag.js'];
require_once "includes/footer.php";
?>