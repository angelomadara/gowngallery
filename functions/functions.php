<?php 

function title_case($str){
	$str = strtolower($str);
	return ucwords($str);
}

function paragraph($str,$style='left'){
    $str = strtolower($str);
    $css = "style='text-align:";
    if($style=='left'){
    	$css .= "left'";
    }
    elseif($style=='right'){
    	$css .= "right'";
    }
    elseif($style=='justify'){
    	$css .= "justify'";
    }else{
    	$css .= "left'";
    }
    return "<div ".$css.">".preg_replace_callback('/[.!?] .*?\w/', create_function('$matches', 'return strtoupper($matches[0]);'), ucfirst($str))."</div>";
}

function cut_str($string,$count){
    return mb_strimwidth($string, 0, $count,'...');
}

// DATE AND TIME
function db_date_time($date){
    return date('Y-m-d H:i:s',strtotime($date));
}

function db_date($date){
    if($date)
      return date('Y-m-d',strtotime($date));
    else
      return '';
}

function pretty_date($date){
    if($date)
        return date("M d, Y",strtotime($date));
    else
        return false;
}

function pretty_time($date){
    return date("h:i A",strtotime($date));
}


function alter_number($mobile_number){
    $the_number = 0;
    if(strpos($mobile_number, '-') || strpos($mobile_number, ' ')){
        // clean number
        $remove_dash_space = str_replace(['-',' '], '', $mobile_number);
        $prefix = substr($remove_dash_space,0,1);
        if(strlen($remove_dash_space) < 10 || strlen($remove_dash_space) > 15){
            // return if number is less than ten characters or more than 15 char
            $the_number = false;
        }else{  
            // format number 
            switch($prefix){
                case "+":$the_number = $remove_dash_space;break;
                case "0":$the_number = is_numeric($remove_dash_space) ? "+63".substr($remove_dash_space,1,strlen($remove_dash_space)) : false;break;
                case '9':$the_number = "+63".$remove_dash_space;break;
                default:$the_number = false;
            }
        }
    }else{
        $prefix = substr($mobile_number,0,1);
        if(strlen($mobile_number) < 10 || strlen($mobile_number) > 15){
            // return if number is less than ten characters or more than 15 char
            $the_number = false;
        }else{  
            // format number 
            switch($prefix){
                case "+":$the_number = $mobile_number;break;
                case "0":$the_number = is_numeric($mobile_number) ? "+63".substr($mobile_number,1,strlen($mobile_number)) : false;break;
                case '9':$the_number = "+63".$mobile_number;break;
                default:$the_number = false;
            }
        }
    }
    // if(strlen($the_number) != 13){return false;}else{return $the_number;}       
    if(strlen($the_number) != 13){return false;}else{return $the_number;}       
}


// function resize_image($file, $w, $h, $crop=FALSE) {
//     list($width, $height) = getimagesize($file);
//     $r = $width / $height;
//     if ($crop) {
//         if ($width > $height) {
//             $width = ceil($width-($width*abs($r-$w/$h)));
//         } else {
//             $height = ceil($height-($height*abs($r-$w/$h)));
//         }
//         $newwidth = $w;
//         $newheight = $h;
//     } else {
//         if ($w/$h > $r) {
//             $newwidth = $h*$r;
//             $newheight = $h;
//         } else {
//             $newheight = $w/$r;
//             $newwidth = $w;
//         }
//     }
//     $src = imagecreatefromjpeg($file);
//     $dst = imagecreatetruecolor($newwidth, $newheight);
//     return imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

//     // return $dst;
// }

// function smart_resize_image($file,
//                               $string             = null,
//                               $width              = 0, 
//                               $height             = 0, 
//                               $proportional       = false, 
//                               $output             = 'file', 
//                               $delete_original    = true, 
//                               $use_linux_commands = false,
//                               $quality            = 100,
//                               $grayscale          = false
//          ) {
      
//     if ( $height <= 0 && $width <= 0 ) return false;
//     if ( $file === null && $string === null ) return false;
//     # Setting defaults and meta
//     $info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
//     $image                        = '';
//     $final_width                  = 0;
//     $final_height                 = 0;
//     list($width_old, $height_old) = $info;
//     $cropHeight = $cropWidth = 0;
//     # Calculating proportionality
//     if ($proportional) {
//       if      ($width  == 0)  $factor = $height/$height_old;
//       elseif  ($height == 0)  $factor = $width/$width_old;
//       else                    $factor = min( $width / $width_old, $height / $height_old );
//       $final_width  = round( $width_old * $factor );
//       $final_height = round( $height_old * $factor );
//     }
//     else {
//       $final_width = ( $width <= 0 ) ? $width_old : $width;
//       $final_height = ( $height <= 0 ) ? $height_old : $height;
//       $widthX = $width_old / $width;
//       $heightX = $height_old / $height;
      
//       $x = min($widthX, $heightX);
//       $cropWidth = ($width_old - $width * $x) / 2;
//       $cropHeight = ($height_old - $height * $x) / 2;
//     }
//     # Loading image to memory according to type
//     switch ( $info[2] ) {
//       case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
//       case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
//       case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
//       default: return false;
//     }
    
//     # Making the image grayscale, if needed
//     if ($grayscale) {
//       imagefilter($image, IMG_FILTER_GRAYSCALE);
//     }    
    
//     # This is the resizing/resampling/transparency-preserving magic
//     $image_resized = imagecreatetruecolor( $final_width, $final_height );
//     if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
//       $transparency = imagecolortransparent($image);
//       $palletsize = imagecolorstotal($image);
//       if ($transparency >= 0 && $transparency < $palletsize) {
//         $transparent_color  = imagecolorsforindex($image, $transparency);
//         $transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
//         imagefill($image_resized, 0, 0, $transparency);
//         imagecolortransparent($image_resized, $transparency);
//       }
//       elseif ($info[2] == IMAGETYPE_PNG) {
//         imagealphablending($image_resized, false);
//         $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
//         imagefill($image_resized, 0, 0, $color);
//         imagesavealpha($image_resized, true);
//       }
//     }
//     imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);
    
    
//     # Taking care of original, if needed
//     if ( $delete_original ) {
//       if ( $use_linux_commands ) exec('rm '.$file);
//       else @unlink($file);
//     }
//     # Preparing a method of providing result
//     switch ( strtolower($output) ) {
//       case 'browser':
//         $mime = image_type_to_mime_type($info[2]);
//         header("Content-type: $mime");
//         $output = NULL;
//       break;
//       case 'file':
//         $output = $file;
//       break;
//       case 'return':
//         return $image_resized;
//       break;
//       default:
//       break;
//     }
    
//     # Writing image according to type to the output destination and image quality
//     switch ( $info[2] ) {
//       case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
//       case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
//       case IMAGETYPE_PNG:
//         $quality = 9 - (int)((0.9*$quality)/10.0);
//         imagepng($image_resized, $output, $quality);
//         break;
//       default: return false;
//     }
//     return true;
// }