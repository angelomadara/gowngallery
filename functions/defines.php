<?php 

// set timezone to asia/manila +8
date_default_timezone_set('Asia/Manila');

// here lies all defines
// some global usable everywhere variables


// start the session on all pages
// just include this defines.php
// @session_start();


// DEFINES

// kukunin nya ang domain name
// this should return http://localhost
// can be use in every anchor tags or even on form actions
define('_domain','http://'.$_SERVER['HTTP_HOST'].'/'); 

define('_plugins',_domain.'plugins/');

define('_image_url',_domain.'images/');

define('_date',date("Y-m-d"));

define('_time',date("H:i:s"));

define('_date_time',_date.' '._time);


// SMS CREDENTIALS
define('DEVICE_ID',45274);
define('DEVICE_USER','jauvymii@gmail.com');
define('DEVICE_PASS','thesisit');
$sms_opt = [
	'send_at' => strtotime('+10 seconds'), // Send the message in 10 minutes
	'expires_at' => strtotime('+1 hour') // Cancel the message in 1 hour if the message is not yet sent
];
