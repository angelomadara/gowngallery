<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
// include 'includes/owner_header.php';
include 'includes/header.php';

if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');

$get_feed_id = Request::get('feed');

// $feeds = Query::fetchAll("SELECT f.* FROM feed AS f
// 						LEFT JOIN comments AS c ON c.feed_id = f.feed_id
// 						WHERE c.user_id = ?",[$owner['user_id']]);
$feed = Query::fetch("SELECT * FROM feed WHERE feed_id = ?",[$get_feed_id]);

// json::print($feed);

$sql_get_comments = "SELECT * 
                    FROM comments AS c 
                    LEFT JOIN store AS s ON s.user_id = c.user_id 
                    WHERE c.feed_id = ? AND c.user_id = ?
                    ORDER BY comment_id DESC";
// $sql_get_comments = "SELECT DISTINCT 
//                     s.store_name, s.store_id, c.feed_id, c.user_id
//                     FROM comments AS c
//                     LEFT JOIN store AS s ON s.user_id = c.user_id 
//                     WHERE c.feed_id = ? AND c.user_id != ? 
//                     ORDER BY comment_id DESC";
$comments = Query::fetchAll($sql_get_comments,[$get_feed_id,$owner['user_id']]);
?>

	
<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php $store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]); ?>
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>

        <div class="col-lg-12">
        </div>
    </div>
    <div class="row" id="owner-dashboard" style="background: #fff">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative active">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
               <!--  <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>

            <div>
                <ul class="breadcrumb">
                    <li class=""><a href='owner-feed.php'>Feeds</a></li>
                    <li class="active">Offer</li>
                </ul>
            </div>
        </div>
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php if($feed): ?>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <img src="<?= _image_url."small/".$feed->photo; ?>" style="width:100%;">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div><b><?= title_case($feed->title) ?></b></div>
                    <span class="italic" style="display: block;"> 
                        Posted date <?=pretty_date($feed->created_at)?> at <?=pretty_time($feed->created_at)?> 
                    </span>
                    <span class="italic" style="display: block;"> 
                        Expiration date <?=pretty_date($feed->expiration)?> at <?=pretty_time($feed->expiration)?> 
                    </span>
                    <div><?= paragraph($feed->message,'left') ?></div>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
        
                    <form action="" id="form-post-reply">
                        <div id="con-info"></div>
                        <div class="input-wrapper clearfix">
                            <label for="">Post reply</label>
                            <?php 
                                $date = Query::fetch("SELECT expiration,deleted_at FROM feed WHERE feed_id = ?",[$get_feed_id]);
                                $disabled = "";
                                $text = "";
                                // echo $date->expiration;
                                if($date->expiration < date('Y-m-d H:i:s') || $date->deleted_at != null){
                                    $disabled = "disabled";
                                    $text = "This post was expired or closed by the customer";
                                }
                            ?>
                            <textarea name="reply" id="" rows="2" class="form-control" <?= $disabled ?> placeholder='<?= $text ?>'>  </textarea>
                            <button type="button" id="btn-reply" class="btn btn-primary btn-sm pull-right" <?= $disabled ?> style="margin-top: 5px;">Reply</button>
                            <button type="button" id="btn-refesh" class="btn btn-primary btn-sm pull-right" style="margin-top: 5px;margin-right: 5px;"> 
                                <span class="glyphicon glyphicon-refresh"></span> 
                            </button>
                        </div>
                        <div class="input-wrapper">
                            <input type="hidden" name="feed-id" value="<?= $get_feed_id ?>">
                            <input type="hidden" name="customer-id" value="<?= $feed->author ?>">
                            <input type="hidden" name="store-id" value="<?= $owner['user_id'] ?>">
                        </div>
                    </form>

                    <div><b>Replies</b></div>
                    <div class="clearfix" id="load-conversation"></div> 
                    <br>

                    <?php foreach($comments as $comment ): ?>
                        <!-- <div style="padding-bottom:1em;">
                            <div><?= paragraph($comment->comment,'left') ?></div>
                            <div class='post-time'><?= pretty_date($comment->created_at)." at ".pretty_time($comment->created_at) ?></div>
                        </div> -->
                    <?php endforeach; ?>
             
                </div>
            
            <?php endif; ?> 
        </div>
        
    </div>
</div>


<?php
    $scripts = ['my-shop.js','notification.js'];
	include 'includes/footer.php';
?>