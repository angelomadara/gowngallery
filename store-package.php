<?php 
    $css = [
        'index.css'
    ]; 
   
include 'includes/header.php';

$id = Request::get('id');
$package = Query::fetch("SELECT 
                            p.* , s.store_name, s.store_id
                        FROM package AS p
                        LEFT JOIN store AS s ON s.user_id = p.user_id
                        WHERE p.pack_id = ?",[$id]);


// Json::pretty($package);
?>
    
<div class="article">
    <div class="container">             
    <?php if($package): ?>
    	<div class="row" style="background: #fff">    
            
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id ="content_area">                        
                    <div class="display-categories">
                        <form class='' action='actions/bag.php' method='POST' style="padding-top: 2em;">
                            <!-- <div class=''> -->
                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>
                                    <img src="<?=_image_url."package/".$package->image?>">
                                </div>

                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>
                                    <h3><?= title_case($package->name) ?></h3><br>
                                    <p> Price: Php <?=number_format($package->price,2)?> </p>
                                    <p style="text-align: justify;"><?=htmlspecialchars_decode($package->note)?> </p> 
                                    <p>Sold by: <a href='store-name.php?id=<?=$package->user_id?>'><?=$package->store_name?></a></p>
                                    <!-- store ratings -->
                                    <?php storeRatings([$package->store_id]); ?>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                                    <input type='hidden' name='pack_id' required value='<?=$id?>'>
                                    <button type='submit' id='rent-item' data-id='<?=$id?>' value='' class='' style='outline:none;'>
                                        <img src="<?=_image_url?>bag.png" alt="" style='display: inline-block; width: 50px;'>
                                        &nbsp;&nbsp;Add to bag
                                    </button>

                                     <!--<div style="margin-top: 2em;">
                                        <h4>Body measurement details</h4>
                                        <i>Click to view full image</i>
                                        <a href="<?= _image_url.'sizechart.jpg' ?>" target="_blank">
                                            <img src="<?= _image_url.'sizechart.jpg' ?>" alt="">
                                        </a>
                                    </div> -->
                                </div>
                                <!-- <div class="clear"></div> -->
                                <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3em;">
                                    <h4>Body Guide</h4>
                                    <img src="<?= _image_url.'sizechartguide.jpg' ?>" alt="">
                                </div> -->
                            <!-- </div> -->
                        </form>
                    </div>
                </div>
    		</div>
    	</div>
        
        <!-- <div class="row">            
            <br><br><br>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php 
                    $comments = Query::fetchAll('SELECT 
                        pc.*,CONCAT(up.first_name," ",up.last_name) AS name 
                    FROM product_comments AS pc
                    LEFT JOIN user_profile AS up ON up.user_id = pc.user_id
                    WHERE pc.product_id = ? ORDER BY product_comment_id DESC',[$id]);
                ?>
                <h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Comments (<?=count($comments)?>) </h4><br>
                <?php if($comments): ?>
                    <?php foreach($comments as $comment): ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:1em;">
                            <div> 
                                <b><?= title_case($comment->name) ?> </b>
                                <?php 
                                    $x = 1;
                                    do {   
                                        echo "<i class='glyphicon glyphicon-star'  style='color: #9b59b6;'></i> ";
                                        $x++;
                                    } while ($x <= $comment->stars);
                                ?>
                            </div>
                            <div>
                                <?= paragraph($comment->comment) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:1em;">
                        <div>No comments yet for this product</div>    
                    </div>
                <?php endif; ?>
            </div>
        </div> -->
    
    <?php endif; ?>
    </div>
</div>

<?php 
	include 'includes/footer.php';
 ?>