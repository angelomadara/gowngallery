<form method="POST" action="actions/customer-update-photo.php" enctype="multipart/form-data">
	<div class="input-wrapper">
		<label for="">Select photo</label>
		<input type="file" name="file" id="file-update" class="form-control">
	</div>
	<div class="input-wrapper" id="img-preview">
		<img src="" alt="" id="img-preview">
	</div>
	<div class="clearfix">
		<!-- <input type="submit" value="Upload" class="pull-right btn btn-success"> -->
	</div>
</form>