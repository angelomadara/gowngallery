		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>

<div class="row">    		

    <form class="form" method="POST" action="actions/owner-add-item.php" enctype="multipart/form-data">
		<input type="hidden" name="insert_post" value="test"/>
		<div class="table-responsive">	
			<table class="table">

				<tr>
					<td align="center"><b>Product Title:</b></td>
					<td><input type="text" name="product_title" size="80" class="form-control" required ></td>
				</tr>
				
				<tr>
					<td align="center"><b>Quantity:</b></td>
					<td><input type="text" name="product_quantity" size="20" class="form-control" required ></td>
				</tr>
				
				<tr >
					<td align="center"><b>Size:</b></td>
					<td>
						<select name="product_size" class="form-control" required >
							<option> Select a Size</option>
							<option value="sm"> Small</option>
							<option value="md"> Medium</option>
							<option value="lg"> Large</option>						
						</select>			
					</td>
				</tr>
				
				<tr>
					<td align="center"><b>Category:</b></td>
					<td>
						<select name="product_cat"  class="form-control" required >
							<option value=""> Select a Category </option>
							<?php 
								if($categories){
									foreach ($categories as $key => $category) {
										echo "<option value='".$category['cat_id']."'>".$category['cat_name']."</option>";
									}
								}
							?>
						</select>			
					</td>
				</tr>
				
				<tr>
					<td align="center"><b>Type:</b></td>
					<td><select name="product_type" class="form-control" required >
						<option> Select a Type </option>
						<?php 
							if($types){
								foreach ($types as $key => $type) {
									echo "<option value='".$type['type_id']."'>".$type['type_name']."</option>";
								}
							}
						?>
					</select>
					</td>
				</tr>
				
				<tr>
					<td align="center"><b>Image:</b></td>
					<td><input type="file" name="product_image" class="form-control" required ></td>
				</tr>
				
				<tr>
					<td align="center"><b>Price:</b></td>
					<td><input type="text" name="product_price" class="form-control" required ></td>
				</tr>
				
				<tr >
					<td align="center"><b>Description:</b></td>
					<td><textarea name="product_desc" cols="20" rows="10" ></textarea></td>
				</tr>
				
				<tr >
					<td align="center"><b>Keywords:</b></td>
					<td><input type="text" name="product_keywords" class="form-control" required ></td>
				</tr>
				
				
				<tr>
					<td>
						<input type="submit" class="btn btn-success" value="Add Item" style="float:bottom-right ;">	
					</td>
				</tr>

			</table>
		</div>
	</form>
</div>