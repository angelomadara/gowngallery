<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
]; 
include 'includes/header.php';

if(!Session::isLogin('user') || Session::get('user')['user_type'] != "customer"){
    Redirect::to('index.php');
    return 0;
}
$customer = Session::get('user');

$profile = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$customer['user_id']]);
// Json::print($customer)
?>
    
<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff;">
       
        <div class="col-lg-12">
            <ul class="breadcrumb">
                <li class=""><a href='customer.php'>Home</a></li>
                <li class="active">Profile</li>
            </ul>
        </div>
    </div>
	
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
             <?php if($profile): ?>
            <div class="relative clearfix">
                <?php 
                    $photo = $profile->picture ? _image_url.'profile/'.$profile->picture : _image_url.'profile/'.rand(1,20).'.svg'; 
                ?>
                <img src="<?= $photo ?>" style='width:100%;display: block;margin: auto;' alt="">
                <br>
                <button id="update-photo" class="btn btn-success btn-sm pull-left">Update Picture</button>
            </div>
            <div style="color: #3498db;font-size:1.2em;margin-top:15px;">
            	<p><b><?= strtoupper($profile->first_name." ".$profile->middle_name." ".$profile->last_name) ?></b></p>
            </div>
            <div><?= $profile->contact ?></div>
            <div><?= title_case($profile->address) ?></div>
            <?php endif; ?>
        </div>
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"">	
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="customer.php"><b>Dashboard</b></a></li>
                <li role="presentation"><a href="customer-orders.php"><b>Requests</b></a></li>
                <li role="presentation"><a href="post-gown.php"><b>Post Dress</b></a></li>
                <li role="presentation"><a href="my-customization.php"><b>My Customization</b></a></li>
                <li role="presentation" class="active"><a href="customer-account.php"><b>Account</b></a></li>
            </ul>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<br>
				<form id="form-update-profile" action="ajax/customer-chage-info.php">
					<div class="input-wrapper">
						<label for="">Firstname</label>
						<input type="text" name="firstname" class="form-control" value="<?= $profile?$profile->first_name:null ?>">
					</div>
					<div class="input-wrapper">
						<label for="">Middlename</label>
						<input type="text" name="middlename" class="form-control" value="<?= $profile?$profile->middle_name:null ?>">
					</div>
					<div class="input-wrapper">
						<label for="">Lastname</label>
						<input type="text" name="lastname" class="form-control" value="<?= $profile?$profile->last_name:null ?>">
					</div>
					<div class="input-wrapper">
						<label for="">Contact</label>
						<input type="text" name="contact" class="form-control mask-mobile" value="<?= $profile?$profile->contact:null ?>">
					</div>

					<div class="input-wrapper">
						<label for="">Address</label>
						<textarea name="address" id="" cols="30" rows="10" class="form-control"><?= $profile?$profile->address:null ?></textarea>
					</div>

					<button type="submit" id="update-my-profile" class="btn btn-success">Update Profile</button>
				</form>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<br>
				<form id="update-password" action="ajax/customer-update-password.php">
					<div class="input-wrapper">
						<label for="">Old password</label>
						<input type="password" name="old-password" id="" class="form-control">
					</div>
					<div class="input-wrapper">
						<label for="">New password</label>
						<input type="password" name="password1" id="" class="form-control">
					</div>
					<div class="input-wrapper">
						<label for="">Re-type password</label>
						<input type="password" name="password2" id="" class="form-control">
					</div>
					<button type="submit" id="btn-update-password" class="btn btn-success">Update Password</button>
				</form>	
			</div>		
		</div>
	</div>

</div>


<?php 
	$scripts = ['customer.js'];
	include 'includes/footer.php';
?>