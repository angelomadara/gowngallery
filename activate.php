<?php 
require_once "includes/header.php";

$user_id = Session::get('user');

$cookie = CartCookie::get('_cartCookie');

$code = Request::get('code');

$activation = Query::fetch("SELECT * FROM user WHERE code = ?",[$code]);

?>

<div class="container">
	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="min-height: 300px;">
			<?php if($activation): ?>
				<?php if($activation->status == 0): ?>
					<?php 
						Query::update('user',['status'=>1],'user_id',$activation->user_id);
					?>
					<h3 style="padding:3em 0;text-align: center;">
						Welcome to balangagowngallery.com! 
						Thank you for signing up. 
						<a href='login.php' class="h3">Login</a> your account to browse all products.
					</h3>
				<?php else: ?>
					<h3 style="padding:3em 0;text-align: center;">
						Account already been activated.
					</h3>
				<?php endif; ?>
			<?php else: ?>
				<h3>Activation code not found!!!</h3>
			<?php endif; ?>
		</div>

	</div>
</div>

<?php 
$scripts = ['bag.js'];
require_once "includes/footer.php";
?>