<?php 
// $css = [
//     'index.css'
// ];    
include_once 'includes/header.php';
?>

<div class="article">
    <div class="container"> 
    	<div class="row" style="background: #fff;padding-top:2em;">  
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1 style="text-align: center;font-size: 3em !important; ">
					Page not found, go <a href="javascript:history.go(-1)" style="font-weight: bold;">back</a>.
				</h1>
				<div style="width: 30%;margin: auto">
					<img src="<?=_image_url?>wedding-dress1.svg" alt="" style='display: block;width: 100%'>
				</div>
				<!-- <p style="text-align: center;margin-top:1em">
					Go Back
				</p> -->
			</div>
		</div>
	</div>
</div>
<?php include_once 'includes/footer.php'; ?>