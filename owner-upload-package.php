<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
];
require_once "includes/header.php";
if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
// $parts = Query::fetchAll("SELECT * FROM package");
$store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]);

?>

<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>

    </div>
	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative active">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
                <!-- <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>
            <ul class="breadcrumb">
                <li><a href='owner.php'>Dashboard</a></li>
                <li class="active">Customized Offers</li>
            </ul>
		</div>
	</div>

    <div class="row">       
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <ul id="product-link">
                <li><a href='my-products.php'>+ Dresses / Tuxedos</a> </li>
                <li>
                    <a href='owner-clothings.php'>+ Custom Parts</a> 
                </li>
                <li><a href='my-packages.php' class="active">+ Packages</a> 
                    <ul>
                        <li><a href='owner-upload-package.php'>- add package</a></li>
                    </ul></li>
            </ul>
        </div> 
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 asdf-product-holder">
            <h4>Add Package</h4>            
            <form id="upload-form" action="ajax/upload-package.php" class="clearfix">
                <input type="hidden" name="id" id="id" value="">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <div class="input-wrapper">
                        <label for="">Package Name</label>
                        <input type="text" name="pack-name" id="pack-name" class="form-control">
                    </div>
                    <div class="input-wrapper">
                        <label for="">Package Price</label>
                        <input type="number" name="pack-price" id="pack-price" class="form-control no-spin">
                    </div>
                    <div class="input-wrapper">
                        <label for="">Package Photo</label>
                        <input type="file" name="pack-file" id="pack-file" class="form-control no-spin">
                    </div>
                    <div class="input-wrapper">
                        <label for="">Package note</label>
                        <textarea name="pack-note" id="pack-note" cols="" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="input-wrapper">
                        <button type="submit" class="btn btn-success" id="add-package">Add Package</button>
                    </div>
                </div>
                <div class="col-md-7 col-md-7 col-sm-7 col-xs-7">
                    <img id="preview" src="images/fashion.png" alt="Item image" style="width:100%;">
                </div>
            </form>

        </div>  
    </div>

</div>

<?php 
	$scripts = ['package.js','notification.js'];
	require_once "includes/footer.php";
?>