<?php 

/**
* 
*/
class Database
{
	
	// public static $db;
	// public $_pdo;

	function __construct(){
		global $db_handler;
		// database credentials
		$host = "127.0.0.1";
		$user = "root";
		$pass = "";
		$name = "rentals";

		// set timezone to asia/manila +8
		date_default_timezone_set('Asia/Manila');

		try {
			$db_handler = new PDO('mysql:host=' . $host . ';dbname=' . $name, $user, $pass, array(PDO::ATTR_PERSISTENT => true));
			$db_handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$db_handler->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$db_handler->exec("set names utf8");
			// echo "Connected";			
		// Caught exceptions upon connecting to the database if there's any.
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
	}

	// public static function ready(){
	// 	if(!isset(self::$db)){
	// 		self::$db = new Database();
	// 	}
	// 	return self::$db;
	// }

}

new Database();