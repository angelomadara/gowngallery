<?php 

require_once 'Database.php';

class Categories
{
	public function get_all_categories(){
		global $db_handler;

		$sql_search_products = "SELECT * FROM category";

		$statement = $db_handler->prepare($sql_search_products);
		$statement->execute();
		$found_products = $statement->fetchAll(PDO::FETCH_OBJ);

		return $found_products;
	}

	public static function get_category_by_id($id,$name){
		return $id + $name;
	}
}

$cat = new Categories();