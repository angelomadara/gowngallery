<?php 

require_once 'Database.php';

class Product extends DB
{
	public function get_all_products(){
		global $db_handler;

		$sql_search_products = "SELECT * FROM product";

		$statement = $db_handler->prepare($sql_search_products);
		$statement->execute();
		$found_products = $statement->fetchAll(PDO::FETCH_OBJ);

		return $found_products;
	}

	public function get_product_by_id($id){

		global $db_handler;

		$sql_search_products = "SELECT * FROM product WHERE product_id = ?";

		$statement = $db_handler->prepare($sql_search_products);
		$statement->execute([
			$id
		]);
		$found_products = $statement->fetch(PDO::FETCH_OBJ);

		return $found_products;

	}

	public function remove_product($id){
		global $db_handler;

		$sql_search_products = "DELETE FROM product WHERE  product_id = ?";

		$statement = $db_handler->prepare($sql_search_products);
		$statement->execute([
			$id
		]);
		$found_products = $statement->fetch(PDO::FETCH_OBJ);

		return $found_products;

	}

	public function update_product($id){

		global $db_handler;

		$sql_search_products = "UPDATE `product` SET `product_id`=[value-1],`user_id`=[value-2],`type_id`=[value-3],`cat_id`=[value-4],`product_name`=[value-5],`description`=[value-6],`size`=[value-7],`price`=[value-8],`image`=[value-9] WHERE  product_id = ?";

		$statement = $db_handler->prepare($sql_search_products);
		$statement->execute([
			$id
		]);
		$found_products = $statement->fetch(PDO::FETCH_OBJ);

		return $found_products;

	}
}


$prod = new Product();




// $nahanap_na_prudukto = $prod->get_produt_by_id(2
// if($nahanap_na_prudukto){
// 	echo $nahanap_na_prudukto->product_name;	
// }

// $prod->num1 + $prod->num2;