<?php 
$css = ['index.css',];
include_once 'includes/header.php';
$id = Request::get('id');
$found_store = Query::fetch("SELECT 
								* 
							FROM store AS s
							LEFT JOIN user_profile AS up ON up.user_id = s.user_id
							WHERE s.user_id = ?",[$id]);
$found_products = Query::fetchAll("SELECT
									*
								FROM product AS p
								LEFT JOIN category AS c ON c.cat_id = p.cat_id
								LEFT JOIN type AS t ON t.type_id = p.type_id
								WHERE p.user_id = ?
								",[$id]);
$packages = Query::fetchAll("SELECT 
								*
							FROM package
							WHERE user_id = ? 
							",[$id]);
// Json::pretty($my_orders);
?>

<div class="article">
    <div class="container"> 
    	<div class="row" style="background: #fff">                
            <?php if($found_store): ?>
    		<div class="col-md-12">
                <div id ="content_area">                        
                    <div class="container-fluid display-categories">
                        <h1 class="champagne" style="font-size: 6em;text-align: center;"> <?=$found_store->store_name?> </h1>
						
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
							<div class="clearfix"> <!-- mga damit -->
								<h1 class="champagne">Gowns & Tuxedos</h1>
								<?php foreach($found_products as $key => $value): ?>
									<?php $sizes = Query::fetchAll("SELECT * FROM size_and_quantity WHERE product_id = ? AND isHidden = ? AND qty != ?",[$value->product_id,0,0]); ?>
									<div class='col-lg-3'>
	                                    <div class='product-holder clearfix'> 
	                                        <a href='info.php?prod_id=<?=$value->product_id?>' class='product-link'>
		                                        <img src='<?=_image_url."small/".$value->image ?>' alt='<?=$value->product_name?>'> 
		                                            <div class='product-name'>                                                 
		                                                <?php if($value->product_name != "" || $value->product_name != null ): ?>
		                                                    <p> <?= mb_strimwidth($value->product_name, 0,23, '...') ?> </p>
		                                                <?php else: ?>
		                                                    <p>No description available.</p>";
		                                                <?php endif; ?>
		                                            <p class='store-name'> <?=$found_store->store_name?> </p>
		                                            <p class='product-price'><b>Php  <?=number_format($value->price,2)?> </b></p>
		                                            <?php if($sizes): ?>
		                                                <span> Sizes:
		                                                <?php 
		                                                	foreach($sizes as $x => $size){
		                                                        echo $size->size; $x = $x + 1;
		                                                        if($x < count($sizes)) { echo ", "; }
		                                                        $x++;
		                                                    }
		                                                ?>
		                                                </span>
		                                            <?php else: ?> 
		                                            	<span>Out of stock.</span>
		                                            <?php endif; ?>
		                                            <span class='pull-right glyphicon glyphicon-comment leave-message' data-id='{$value->product_id}'></span>
		                                        </div>
	                                    	</a>
	                                    </div>
	                                </div>
								<?php endforeach; ?>
							</div>
							<div class="clearfix"> <!-- mga package -->
								<h1 class="champagne">Packages Offered</h1>
								<?php foreach($packages as $key => $package): ?>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<a href="store-package.php?id=<?=$package->pack_id?>" class='product-link'>
										<img src="<?=_image_url.'package/'.$package->image?>" alt="" style='width: 100%;'>
										<div><?php echo $package->name; ?></div>
										<div>PHP <?php echo number_format($package->price,2); ?></div>
										</a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

	                        <iframe style="width:100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=<?=$found_store->lat?>,<?=$found_store->lng?>&hl=es;z=14&amp;output=embed"></iframe><br />
							<small>
								<a href="https://maps.google.com/maps?q=<?=$found_store->lat?>,<?=$found_store->lng?>&hl=es;z=14&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank">See map bigger</a>
							</small>							
							<div>
								<img src="<?=_image_url.'permits/'.$found_store->permit?>" alt="">
							</div>
							<div>
								<div><h3>Customize a Gown?</h3></div>
								<div>
									<ol style="padding-left:1em;">
										<li>Pick a perfect Top and Bottom part for your gown and estimate its cost.</li>
										<li>Send your choices to your chosen rental shop.</li>
										<li>Wait a confirmation message if your rented item is granted or not through SMS.</li>
									</ol>
								</div>
								<div>
									<a href='store-customization.php?store=<?=$id?>#female' class="btn btn-success">Customization page</a>
								</div>
							</div>
						</div>
                    </div>
                </div>
    		</div>
            <?php endif; ?>
    	</div>
    </div>
</div>

<?php
$scripts = [];
include_once 'includes/footer.php';
?>