<?php 
$css = [
    'index.css',
    'owner-dashboard.css'
];
require_once "includes/header.php";
if(Session::get('user')['user_type'] != 'owner' || !Session::isLogin('user')){
    Redirect::to('index.php');
}
$owner = Session::get('user');
$store = Query::fetch("SELECT * FROM store WHERE user_id = ?",[$owner['user_id']]);
$orders = Query::fetchAll("SELECT * FROM customized_items WHERE store_id = ?",[$owner['user_id']]);
// Json::pretty($orders);
?>

<div class="container" style="min-height: 400px;">
    <div class="row" style="background: #fff">
        <div class="col-lg-12">
            <h1 class="champagne store-name">
                <?php echo $store ? $store->store_name : null ; ?>
            </h1>
            <!-- <hr> -->
        </div>

    </div>
	
	<div class="row" style="min-height: 300px;">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 5px;"> 
                <li role="presentation" class="relative">
                    <a href="owner.php"><b>Dashboard</b></a>
                </li>
                <li role="presentation" id="feed-tab" class="relative">
                    <a href="owner-feed.php"><b>Feeds</b></a>
                </li>
                <li role="presentation" id="custom-tab" class="relative active">
                    <a href="owner-customization-page.php"><b>Customization</b></a>
                </li>
                <li role="presentation" id='order-tab' class="relative">
                    <a href="my-orders.php"><b>Requests</b></a>
                </li>
                <li role="presentation" class="relative">
                    <a href="my-products.php"><b>Gallery</b></a>
                </li>
                <!-- <li role="presentation" class="relative">
                    <a href="my-packages.php"><b>Packages</b></a>
                </li> -->
                <li role="presentation" class="relative">
                    <a href="my-shop.php"><b>My Shop</b></a>
                </li>
            </ul>

	        <div class="col-lg-12">
	            <ul class="breadcrumb">
	                <li class="active">Customization</li>
	                <!-- <li>Customized Offers</li> -->
	            </ul>
	        </div>
			
			<div class="clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h4>Requests</h4>
					<table class="table table-bordered table-striped datatable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Requests</th>
                                <th style="width:20%">Notes</th>
                                <th style="width:15%;">Dates</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  if($orders): foreach($orders as $order): 
                                    // print_r($order->fitting_date);
                                    if($order->fitting_date!= null && $order->fitting_date <= date('Y-m-d')){
                                        // close order if the fitting date is lagpas na
                                        Query::update('customized_items',[
                                            'status' => 'closed'
                                        ],'item_id',$order->item_id);
                                    }
                                    $customer = Query::fetch("SELECT * FROM user_profile WHERE user_id = ?",[$order->user_id]); 
                            ?>
                                <tr>
                                    <td><?=$order->item_id?></td>
                                    <td>
                                        <?php  $ids = json_decode($order->custom_id,true); $x = 1; $bilang = count($ids); ?>
                                        <?php foreach ($ids as $key => $id) : ?>
                                            <div class="row" style="background: transparent !important;">
                                                <?php $part = Query::fetch("SELECT * FROM customization WHERE custom_id =?",[$id]); ?>                                        
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <img src="<?php echo _image_url.'parts/'.$part->image ?>" alt="<?php echo $part->image ?>" style="width:100%;display:block;">
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="border-bottom:1px solid #ddd;padding-bottom: 1em;">
                                                    <div>Type: <?php echo $part->type; ?> </div>
                                                    <div>Name: <?php echo title_case($part->name); ?> </div>
                                                    <div>Price: PHP <?php echo number_format($part->price,2); ?> </div>
                                                </div>
                                                <?php if($x == $bilang): ?>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <div style="margin-top:2em;">
                                                        <div>
                                                            <p>Status: <?=title_case($order->status)?></p>
                                                            <p>Customer name: <a href='view-customer.php?id=<?=$order->user_id?>'><?=title_case($customer->first_name." ".$customer->last_name)?></a></p>
                                                        </div>
                                                        <div data-number='<?=$customer->contact?>'>
                                                            <?php if($order->status == 'open'): ?>
                                                                <button id="confirm-request" data-id="<?=$order->item_id?>" class="btn btn-success btn-sm">Confirm request</button>
                                                                <button id="cancel-request" data-id="<?=$order->item_id?>" class="btn btn-default btn-sm">Cancel request</button>
                                                            <?php elseif($order->status == 'pending'): ?>
                                                                <button id="close-request" data-id="<?=$order->item_id?>" class="btn btn-success btn-sm">Close deal</button>
                                                                <button id="cancel-request" data-id="<?=$order->item_id?>" class="btn btn-default btn-sm">Cancel request</button>
                                                            <?php elseif($order->status == 'deal'): ?>
                                                                <button id="return-request" data-id="<?=$order->item_id?>" class="btn btn-default btn-sm">Return Clothes</button>
                                                            <?php elseif($order->status == 'cancel'): ?>
                                                                <div>This order has been cancel</div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                                <?php $x++; ?>
                                            </div>
                                        <?php endforeach; ?> 
                                    </td>
                                    <td> 
                                    	<div>Customer Note: <br><?=paragraph($order->note)?></div> 
                                    	<hr>
                                    	<div>My Note: <br> <?=paragraph($order->store_note)?> </div>
                                    </td>
                                    <td> 
                                        <div>
                                            <div>Date Needed :</div> <div><?=pretty_date($order->date_needed)?></div> 
                                        </div>
                                        <hr>
                                        <div>
                                            <div>Fitting Date :</div>
                                            <div>
                                                <?=pretty_date($order->fitting_date)?>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach; endif; ?>
                        </tbody>
                    </table>
				</div>	
			</div>

		</div>
	</div>

</div>

<?php 
	$scripts = ['customized.js','notification.js'];
	require_once "includes/footer.php";
?>